CREATE SCHEMA tor;

CREATE TABLE tor.B (
  id                   integer  NOT NULL  generated always as identity,
  src_date             bigint    ,
  type                 varchar(64)   DEFAULT 'bridge-server-descriptor 1.1' ,
  digest               varchar(40)    ,
  sha256               varchar(43)    ,
  identity_ed25519     char(1)    ,
  master_key_ed25519   varchar(43)    ,
  platform             varchar(256)    ,
  published            bigint    ,
  fingerprint          varchar(40)    ,
  hibernating          char(1)    ,
  uptime               bigint    ,
  onion_key            char(1)    ,
  onion_key_crosscert  char(1)    ,
  ntor_onion_key       char(1)    ,
  ntor_onion_key_crosscert char(1)    ,
  signing_key          char(1)    ,
  ipv6_policy          varchar(256)    ,
  ipv6_portlist        varchar(256)    ,
  contact              varchar(256)    ,
  eventdns             char(1)    ,
  caches_extra_info    char(1)    ,
  extra_info_digest    varchar(40)    ,
  extra_info_digest_sha256 varchar(43)    ,
  allow_single_hop_exits char(1)    ,
  tunneled_dir_server  char(1)    ,
  CONSTRAINT Pk_b PRIMARY KEY ( id ),
  CONSTRAINT Idx_B UNIQUE ( published, fingerprint )
);

CREATE INDEX Idx_B_src_date ON tor.B ( src_date );

CREATE TABLE tor.BS (
  id                   integer  NOT NULL  generated always as identity,
  src_date             bigint    ,
  type                 varchar(64)   DEFAULT 'bridge-network-status 1.0' ,
  published            bigint    ,
  CONSTRAINT Pk_bs PRIMARY KEY ( id ),
  CONSTRAINT Idx_BS UNIQUE ( published )
);

CREATE INDEX Idx_BS_src_date ON tor.BS ( src_date );

CREATE TABLE tor.BS_FlagTresholds (
  id                   integer  NOT NULL  ,
  stable_uptime        bigint    ,
  stable_mtbf          bigint    ,
  enough_mtbf          integer    ,
  fast_speed           bigint    ,
  guard_wfu            double    ,
  guard_tk             bigint    ,
  guard_bw_inc_exits   bigint    ,
  guard_bw_exc_exits   bigint    ,
  ignoring_advertised_bws integer    ,
  CONSTRAINT Pk_bs_FlagTresholds PRIMARY KEY ( id ),
  CONSTRAINT fk_bs_FlagTresholds FOREIGN KEY ( id ) REFERENCES tor.BS( id )
);

CREATE TABLE tor.BS_Status (
  id                   integer  NOT NULL  generated always as identity,
  BS_id                integer  NOT NULL  ,
  v                    varchar(8)    ,
  CONSTRAINT Pk_bs_Status PRIMARY KEY ( id ),
  CONSTRAINT fk_bs_Status FOREIGN KEY ( BS_id ) REFERENCES tor.BS( id )
);

CREATE INDEX idx_bs_Status ON tor.BS_Status ( BS_id );

CREATE TABLE tor.BS_Status_A (
  id                   integer  NOT NULL  generated always as identity,
  BS_Status_id         integer  NOT NULL  ,
  address              varchar(256)    ,
  port                 integer    ,
  CONSTRAINT Pk_bs_Status_A PRIMARY KEY ( id ),
  CONSTRAINT fk_bs_Status_A FOREIGN KEY ( BS_Status_id ) REFERENCES tor.BS_Status( id )
);

CREATE INDEX idx_bs_Status_A ON tor.BS_Status_A ( BS_Status_id );

CREATE TABLE tor.BS_Status_P (
  id                   integer  NOT NULL  ,
  default_policy       varchar(24)    ,
  port_summary         varchar(2048)    ,
  CONSTRAINT Pk_bs_Status_P PRIMARY KEY ( id ),
  CONSTRAINT fk_bs_Status_P FOREIGN KEY ( id ) REFERENCES tor.BS_Status( id )
);

CREATE TABLE tor.BS_Status_R (
  id                   integer  NOT NULL  ,
  nickname             varchar(19)    ,
  "identity"           varchar(40)    ,
  digest               varchar(40)    ,
  published            bigint    ,
  ip                   varchar(15)    ,
  or_port              integer    ,
  dir_port             integer    ,
  CONSTRAINT Pk_bs_Status_R PRIMARY KEY ( id ),
  CONSTRAINT fk_bs_Status_R FOREIGN KEY ( id ) REFERENCES tor.BS_Status( id )
);

CREATE TABLE tor.BS_Status_S (
  id                   integer  NOT NULL  generated always as identity,
  bs_Status_id         integer  NOT NULL  ,
  flag                 varchar(32)    ,
  CONSTRAINT Pk_bs_Status_S PRIMARY KEY ( id ),
  CONSTRAINT fk_bs_Status_S FOREIGN KEY ( bs_Status_id ) REFERENCES tor.BS_Status( id )
);

CREATE INDEX idx_bs_Status_S ON tor.BS_Status_S ( bs_Status_id );

CREATE TABLE tor.BS_Status_W (
  id                   integer  NOT NULL  ,
  bandwidth            bigint    ,
  measured             bigint    ,
  unmeasured           char(1)    ,
  CONSTRAINT Pk_bs_Status_W PRIMARY KEY ( id ),
  CONSTRAINT fk_bs_Status_W FOREIGN KEY ( id ) REFERENCES tor.BS_Status( id )
);

CREATE TABLE tor.BX (
  id                   integer  NOT NULL  generated always as identity,
  src_date             bigint    ,
  type                 varchar(64)   DEFAULT 'bridge-extra-info 1.3' ,
  identity_ed25519     char(1)    ,
  published            bigint    ,
  geoip_db_digest      varchar(40)    ,
  geoip6_db_digest     varchar(40)    ,
  geoip_start_time     bigint    ,
  dirreq_v2_share      double    ,
  dirreq_v3_share      double    ,
  cell_circuits_per_decile integer    ,
  extra_info_digest    varchar(40)    ,
  extra_info_digest_sha256 varchar(43)    ,
  master_key_ed25519   varchar(43)    ,
  CONSTRAINT Pk_be PRIMARY KEY ( id ),
  CONSTRAINT Idx_BX UNIQUE ( published, extra_info_digest )
);

CREATE INDEX Idx_BX_src_date ON tor.BX ( src_date );

CREATE TABLE tor.BX_BridgeIpTransports (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  protocol             varchar(16)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_BridgeIpTransports PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_BridgeIpTransports FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_BridgeIpTransports ON tor.BX_BridgeIpTransports ( BX_id );

CREATE TABLE tor.BX_BridgeIpVersions (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  version              varchar(16)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_BridgeIpVersions PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_BridgeIpVersions FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_BridgeIpVersions ON tor.BX_BridgeIpVersions ( BX_id );

CREATE TABLE tor.BX_BridgeIps (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_BridgeIps PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_BridgeIps FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_BridgeIps ON tor.BX_BridgeIps ( BX_id );

CREATE TABLE tor.BX_BridgeStatsEnd (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_BX_BridgeStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_BridgeStatsEnd FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_BridgeStatsEnd ON tor.BX_BridgeStatsEnd ( id );

CREATE INDEX idx_BX_BridgeStatsEnd_0 ON tor.BX_BridgeStatsEnd ( BX_id );

CREATE TABLE tor.BX_CellProcessedCells (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  mean                 integer    ,
  CONSTRAINT Pk_BX_CellProcessedCells PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_CellProcessedCells FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_CellProcessedCells ON tor.BX_CellProcessedCells ( BX_id );

CREATE TABLE tor.BX_CellQueuedCells (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  mean                 double    ,
  CONSTRAINT Pk_BX_CellQueuedCells PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_CellQueuedCells FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_CellQueuedCells ON tor.BX_CellQueuedCells ( BX_id );

CREATE TABLE tor.BX_CellStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_BX_CellStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_CellStatsEnd FOREIGN KEY ( id ) REFERENCES tor.BX( id )
);

CREATE TABLE tor.BX_CellTimeInQueue (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  mean                 integer    ,
  CONSTRAINT Pk_BX_CellTimeInQueue PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_CellTimeInQueue FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_CellTimeInQueue ON tor.BX_CellTimeInQueue ( BX_id );

CREATE TABLE tor.BX_ConnBiDirect (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  below                integer    ,
  "read"               integer    ,
  "write"              integer    ,
  "both"               integer    ,
  CONSTRAINT Pk_BX_ConnBiDirect PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_ConnBiDirect FOREIGN KEY ( id ) REFERENCES tor.BX( id )
);

CREATE TABLE tor.BX_DirreqReadHistory (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT idx_BX_DirreqReadHistory UNIQUE ( id ),
  CONSTRAINT Pk_BX_DirreqReadHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqReadHistory FOREIGN KEY ( id ) REFERENCES tor.BX( id )
);

CREATE TABLE tor.BX_DirreqReadHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  BX_DirreqRead_id     integer  NOT NULL  ,
  bytes                bigint  NOT NULL  ,
  CONSTRAINT Pk_BX_DirreqReadHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqReadHistory_Bytes FOREIGN KEY ( BX_DirreqRead_id ) REFERENCES tor.BX_DirreqReadHistory( id )
);

CREATE INDEX idx_BX_DirreqReadHistory_Bytes ON tor.BX_DirreqReadHistory_Bytes ( BX_DirreqRead_id );

CREATE TABLE tor.BX_DirreqStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_BX_DirreqStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqStatsEnd FOREIGN KEY ( id ) REFERENCES tor.BX( id )
);

CREATE TABLE tor.BX_DirreqV2DirectDl (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  "key"                varchar(32)    ,
  value                integer    ,
  CONSTRAINT Pk_BX_DirreqV2DirectDl PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV2DirectDl FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_DirreqV2DirectDl ON tor.BX_DirreqV2DirectDl ( BX_id );

CREATE TABLE tor.BX_DirreqV2Ips (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_DirreqV2Ips PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV2Ips FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_DirreqV2Ips ON tor.BX_DirreqV2Ips ( BX_id );

CREATE TABLE tor.BX_DirreqV2Reqs (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_DirreqV2Reqs PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV2Reqs FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_DirreqV2Reqs ON tor.BX_DirreqV2Reqs ( BX_id );

CREATE TABLE tor.BX_DirreqV2Resp (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  response             varchar(256)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_DirreqV2Resp PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV2Resp FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_DirreqV2Resp ON tor.BX_DirreqV2Resp ( BX_id );

CREATE TABLE tor.BX_DirreqV2TunneledDl (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  "key"                varchar(32)    ,
  value                integer    ,
  CONSTRAINT Pk_BX_DirreqV2TunneledDl PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV2TunneledDl FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_DirreqV2TunneledDl ON tor.BX_DirreqV2TunneledDl ( BX_id );

CREATE TABLE tor.BX_DirreqV3DirectDl (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  "key"                varchar(32)    ,
  value                integer    ,
  CONSTRAINT Pk_BX_DirreqV3DirectDl PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV3DirectDl FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_DirreqV3DirectDl_0 ON tor.BX_DirreqV3DirectDl ( BX_id );

CREATE TABLE tor.BX_DirreqV3Ips (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_DirreqV3Ips PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV3Ips FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_DirreqV3Ips ON tor.BX_DirreqV3Ips ( BX_id );

CREATE TABLE tor.BX_DirreqV3Reqs (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_DirreqV3Reqs PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV3Reqs FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_DirreqV3Reqs ON tor.BX_DirreqV3Reqs ( BX_id );

CREATE TABLE tor.BX_DirreqV3Resp (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  response             varchar(256)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_DirreqV3Resp PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV3Resp FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_DirreqV3Resp ON tor.BX_DirreqV3Resp ( BX_id );

CREATE TABLE tor.BX_DirreqV3TunneledDl (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  "key"                varchar(32)    ,
  value                integer    ,
  CONSTRAINT Pk_BX_DirreqV3TunneledDl PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV3TunneledDl FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_DirreqV3TunneledDl_0 ON tor.BX_DirreqV3TunneledDl ( BX_id );

CREATE TABLE tor.BX_DirreqWriteHistory (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_BX_DirreqWriteHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqWriteHistory FOREIGN KEY ( id ) REFERENCES tor.BX( id )
);

CREATE TABLE tor.BX_DirreqWriteHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  BX_DirreqWrite_id    integer  NOT NULL  ,
  bytes                bigint  NOT NULL  ,
  CONSTRAINT Pk_BX_DirreqWriteHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqWriteHistory_Bytes FOREIGN KEY ( BX_DirreqWrite_id ) REFERENCES tor.BX_DirreqWriteHistory( id )
);

CREATE INDEX idx_BX_DirreqWriteHistory_Bytes ON tor.BX_DirreqWriteHistory_Bytes ( BX_DirreqWrite_id );

CREATE TABLE tor.BX_EntryIps (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_EntryIps PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_EntryIps FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_EntryIps ON tor.BX_EntryIps ( BX_id );

CREATE TABLE tor.BX_EntryStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_BX_EntryStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_EntryStatsEnd FOREIGN KEY ( id ) REFERENCES tor.BX( id )
);

CREATE TABLE tor.BX_ExitKibibytesRead (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  port                 varchar(5)    ,
  kib                  bigint    ,
  CONSTRAINT Pk_BX_ExitKibibytesRead PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_ExitKibibytesRead FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_ExitKibibytesRead ON tor.BX_ExitKibibytesRead ( BX_id );

CREATE TABLE tor.BX_ExitKibibytesWritten (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  port                 varchar(5)    ,
  kib                  bigint    ,
  CONSTRAINT Pk_BX_ExitKibibytesWritten PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_ExitKibibytesWritten FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_ExitKibibytesWritten ON tor.BX_ExitKibibytesWritten ( BX_id );

CREATE TABLE tor.BX_ExitStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_BX_ExitStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_ExitStatsEnd FOREIGN KEY ( id ) REFERENCES tor.BX( id )
);

CREATE TABLE tor.BX_ExitStreamsOpened (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  port                 varchar(5)    ,
  count                bigint    ,
  CONSTRAINT Pk_BX_ExitStreamsOpened PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_ExitStreamsOpened FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_ExitStreamsOpened ON tor.BX_ExitStreamsOpened ( BX_id );

CREATE TABLE tor.BX_ExtraInfo (
  id                   integer  NOT NULL  ,
  nickname             varchar(19)    ,
  fingerprint          varchar(40)    ,
  CONSTRAINT Pk_BX_ExtraInfo PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_ExtraInfo FOREIGN KEY ( id ) REFERENCES tor.BX( id )
);

CREATE TABLE tor.BX_GeoIpClientOrigins (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_GeoIpClientOrigins PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_GeoIpClientOrigins FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_GeoIpClientOrigins ON tor.BX_GeoIpClientOrigins ( BX_id );

CREATE TABLE tor.BX_HidservDirOnionsSeen (
  id                   integer  NOT NULL  ,
  onions               double    ,
  CONSTRAINT Pk_Table_0 PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_HidservDirOnionsSeen FOREIGN KEY ( id ) REFERENCES tor.BX( id )
);

CREATE TABLE tor.BX_HidservDirOnionsSeen_Obfuscation (
  id                   integer  NOT NULL  generated always as identity,
  BX_Onion_id          integer  NOT NULL  ,
  obfuscation          varchar(32)    ,
  value                double    ,
  CONSTRAINT Pk_BX_HidservDirOnionsSeen_Obfuscation PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_HidservDirOnionsSeen_Obfuscation FOREIGN KEY ( BX_Onion_id ) REFERENCES tor.BX_HidservDirOnionsSeen( id )
);

CREATE INDEX idx_BX_HidservDirOnionsSeen_Obfuscation ON tor.BX_HidservDirOnionsSeen_Obfuscation ( BX_Onion_id );

CREATE TABLE tor.BX_HidservRendRelayedCells (
  id                   integer  NOT NULL  ,
  cells                double    ,
  CONSTRAINT Pk_BX_HidservRendRelayedCells PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_HidservRendRelayedCells FOREIGN KEY ( id ) REFERENCES tor.BX( id )
);

CREATE TABLE tor.BX_HidservRendRelayedCells_Obfuscation (
  id                   integer  NOT NULL  generated always as identity,
  BX_Relay_id          integer  NOT NULL  ,
  obfuscation          varchar(32)    ,
  value                double    ,
  CONSTRAINT Pk_BX_HidservRendRelayedCells_Obfuscation PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_HidservRendRelayedCells_Obfuscation FOREIGN KEY ( BX_Relay_id ) REFERENCES tor.BX_HidservRendRelayedCells( id )
);

CREATE INDEX idx_BX_HidservRendRelayedCells_Obfuscation ON tor.BX_HidservRendRelayedCells_Obfuscation ( BX_Relay_id );

CREATE TABLE tor.BX_HidservStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_BX_HidservStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_HidservStatsEnd FOREIGN KEY ( id ) REFERENCES tor.BX( id )
);

CREATE TABLE tor.BX_ReadHistory (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_BX_ReadHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_ReadHistory FOREIGN KEY ( id ) REFERENCES tor.BX( id )
);

CREATE TABLE tor.BX_ReadHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  BX_Read_id           integer  NOT NULL  ,
  bytes                bigint    ,
  CONSTRAINT Pk_BX_ReadHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_ReadHistory_Bytes FOREIGN KEY ( BX_Read_id ) REFERENCES tor.BX_ReadHistory( id )
);

CREATE INDEX idx_BX_ReadHistory_Bytes ON tor.BX_ReadHistory_Bytes ( BX_Read_id );

CREATE TABLE tor.BX_Transport (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  transport            varchar(32)    ,
  CONSTRAINT Pk_BX_Transport PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_Transport FOREIGN KEY ( BX_id ) REFERENCES tor.BX( id )
);

CREATE INDEX idx_BX_Transport ON tor.BX_Transport ( BX_id );

CREATE TABLE tor.BX_WriteHistory (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT idx_BX_WriteHistory UNIQUE ( id ),
  CONSTRAINT Pk_BX_WriteHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_WriteHistory FOREIGN KEY ( id ) REFERENCES tor.BX( id )
);

CREATE TABLE tor.BX_WriteHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  BX_Write_id          integer  NOT NULL  ,
  bytes                bigint    ,
  CONSTRAINT Pk_BX_WriteHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_WriteHistory_Bytes FOREIGN KEY ( BX_Write_id ) REFERENCES tor.BX_WriteHistory( id )
);

CREATE INDEX idx_BX_WriteHistory_Bytes ON tor.BX_WriteHistory_Bytes ( BX_Write_id );

CREATE TABLE tor.B_Bandwidth (
  id                   integer  NOT NULL  ,
  "avg"                integer    ,
  burst                integer    ,
  observed             integer    ,
  CONSTRAINT Pk_b_Bandwidth PRIMARY KEY ( id ),
  CONSTRAINT fk_b_Bandwidth FOREIGN KEY ( id ) REFERENCES tor.B( id )
);

CREATE TABLE tor.B_CircuitProtocolVersions (
  id                   integer  NOT NULL  generated always as identity,
  B_id                 integer  NOT NULL  ,
  circuit_protocol_versions smallint  NOT NULL  ,
  CONSTRAINT Pk_b_CircuitProtocolVersions PRIMARY KEY ( id ),
  CONSTRAINT fk_b_CircuitProtocolVersions FOREIGN KEY ( B_id ) REFERENCES tor.B( id )
);

CREATE INDEX idx_b_CircuitProtocolVersions ON tor.B_CircuitProtocolVersions ( B_id );

CREATE TABLE tor.B_ExitPolicy (
  id                   integer  NOT NULL  generated always as identity,
  B_id                 integer  NOT NULL  ,
  exit_policy          varchar(256)  NOT NULL  ,
  CONSTRAINT Pk_b_ExitPolicy PRIMARY KEY ( id ),
  CONSTRAINT fk_b_ExitPolicy FOREIGN KEY ( B_id ) REFERENCES tor.B( id )
);

CREATE INDEX idx_b_ExitPolicy ON tor.B_ExitPolicy ( B_id );

CREATE TABLE tor.B_Family (
  id                   integer  NOT NULL  generated always as identity,
  B_id                 integer  NOT NULL  ,
  family               varchar(256)  NOT NULL  ,
  CONSTRAINT Pk_b_Family PRIMARY KEY ( id ),
  CONSTRAINT fk_b_Family FOREIGN KEY ( B_id ) REFERENCES tor.B( id )
);

CREATE INDEX idx_b_Family ON tor.B_Family ( B_id );

CREATE TABLE tor.B_HiddenserviceDir (
  id                   integer  NOT NULL  generated always as identity,
  B_id                 integer  NOT NULL  ,
  hidden_service_dir   smallint  NOT NULL  ,
  CONSTRAINT Pk_b_HiddenserviceDir PRIMARY KEY ( id ),
  CONSTRAINT fk_b_HiddenserviceDir FOREIGN KEY ( B_id ) REFERENCES tor.B( id )
);

CREATE INDEX idx_b_HiddenserviceDir ON tor.B_HiddenserviceDir ( B_id );

CREATE TABLE tor.B_LinkProtocolVersions (
  id                   integer  NOT NULL  generated always as identity,
  B_id                 integer  NOT NULL  ,
  link_protocol_versions smallint  NOT NULL  ,
  CONSTRAINT Pk_b_LinkProtocolVersions PRIMARY KEY ( id ),
  CONSTRAINT fk_b_LinkProtocolVersions FOREIGN KEY ( B_id ) REFERENCES tor.B( id )
);

CREATE INDEX idx_b_LinkProtocolVersions ON tor.B_LinkProtocolVersions ( B_id );

CREATE TABLE tor.B_OrAdress (
  id                   integer  NOT NULL  generated always as identity,
  B_id                 integer  NOT NULL  ,
  adress               varchar(256)  NOT NULL  ,
  port                 smallint  NOT NULL  ,
  CONSTRAINT Pk_b_OrAdress PRIMARY KEY ( id ),
  CONSTRAINT fk_b_OrAdress FOREIGN KEY ( B_id ) REFERENCES tor.B( id )
);

CREATE INDEX idx_b_OrAdress ON tor.B_OrAdress ( B_id );

CREATE TABLE tor.B_ReadHistory (
  id                   integer  NOT NULL  ,
  date                 bigint  NOT NULL  ,
  interval             bigint  NOT NULL  ,
  CONSTRAINT Pk_b_ReadHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_b_ReadHistory FOREIGN KEY ( id ) REFERENCES tor.B( id )
);

CREATE TABLE tor.B_ReadHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  B_Read_id            integer  NOT NULL  ,
  bytes                bigint  NOT NULL  ,
  CONSTRAINT Pk_b_ReadHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_b_ReadHistory_Bytes FOREIGN KEY ( B_Read_id ) REFERENCES tor.B_ReadHistory( id )
);

CREATE INDEX idx_b_ReadHistory_Bytes ON tor.B_ReadHistory_Bytes ( B_Read_id );

CREATE TABLE tor.B_Router (
  id                   integer  NOT NULL  ,
  nickname             varchar(19)    ,
  address              varchar(256)    ,
  or_port              integer    ,
  socks_port           integer    ,
  dir_port             integer    ,
  CONSTRAINT Pk_b_Router PRIMARY KEY ( id ),
  CONSTRAINT fk_b_Router FOREIGN KEY ( id ) REFERENCES tor.B( id )
);

CREATE TABLE tor.B_WriteHistory (
  id                   integer  NOT NULL  ,
  date                 bigint  NOT NULL  ,
  interval             bigint  NOT NULL  ,
  CONSTRAINT Pk_b_WriteHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_b_WriteHistory FOREIGN KEY ( id ) REFERENCES tor.B( id )
);

CREATE TABLE tor.B_WriteHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  B_Write_id           integer  NOT NULL  ,
  bytes                bigint  NOT NULL  ,
  CONSTRAINT Pk_b_WriteHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_b_WriteHistory_Bytes FOREIGN KEY ( B_Write_id ) REFERENCES tor.B_WriteHistory( id )
);

CREATE INDEX idx_b_WriteHistory_Bytes ON tor.B_WriteHistory_Bytes ( B_Write_id );

CREATE TABLE tor.E (
  id                   integer  NOT NULL  generated always as identity,
  src_date             bigint    ,
  type                 varchar(64)   DEFAULT 'tordnsel 1.0' ,
  downloaded           bigint    ,
  CONSTRAINT Pk_e PRIMARY KEY ( id ),
  CONSTRAINT Idx_E UNIQUE ( downloaded )
);

CREATE INDEX Idx_E_src_date ON tor.E ( src_date );

CREATE TABLE tor.E_ExitNode (
  id                   integer  NOT NULL  generated always as identity,
  E_id                 integer  NOT NULL  ,
  fingerprint          varchar(40)    ,
  published            bigint    ,
  last_status          bigint    ,
  CONSTRAINT Pk_e_ExitNode PRIMARY KEY ( id ),
  CONSTRAINT fk_e_ExitNode FOREIGN KEY ( E_id ) REFERENCES tor.E( id )
);

CREATE INDEX idx_e_ExitNode ON tor.E_ExitNode ( E_id );

CREATE TABLE tor.E_ExitNode_ExitAdress (
  id                   integer  NOT NULL  generated always as identity,
  E_Node_id            integer  NOT NULL  ,
  adress               varchar(20)  NOT NULL  ,
  millis               bigint  NOT NULL  ,
  CONSTRAINT Pk_e_ExitNode_ExitAdress PRIMARY KEY ( id ),
  CONSTRAINT fk_e_ExitNode_ExitAdress FOREIGN KEY ( E_Node_id ) REFERENCES tor.E_ExitNode( id )
);

CREATE INDEX idx_e_ExitNode_ExitAdress ON tor.E_ExitNode_ExitAdress ( E_Node_id );

CREATE TABLE tor.R (
  id                   integer  NOT NULL  generated always as identity,
  src_date             bigint    ,
  type                 varchar(64)   DEFAULT 'server-descriptor 1.0' ,
  digest               varchar(40)    ,
  sha256               varchar(43)    ,
  identity_ed25519     char(1)    ,
  master_key_ed25519   varchar(43)    ,
  platform             varchar(256)    ,
  published            bigint    ,
  fingerprint          varchar(40)    ,
  hibernating          char(1)    ,
  uptime               bigint    ,
  onion_key            char(1)    ,
  onion_key_crosscert  char(1)    ,
  ntor_onion_key       char(1)    ,
  ntor_onion_key_crosscert char(1)    ,
  signing_key          char(1)    ,
  ipv6_policy          varchar(256)    ,
  ipv6_portlist        varchar(256)    ,
  router_sig_ed25519   char(1)    ,
  router_signature     char(1)    ,
  contact              varchar(256)    ,
  eventdns             char(1)    ,
  caches_extra_info    char(1)    ,
  extra_info_digest    varchar(40)    ,
  extra_info_digest_sha256 varchar(43)    ,
  allow_single_hop_exits char(1)    ,
  tunneled_dir_server  char(1)    ,
  CONSTRAINT Pk_r PRIMARY KEY ( id ),
  CONSTRAINT Idx_R UNIQUE ( published, fingerprint )
);

CREATE INDEX Idx_R_src_date ON tor.R ( src_date );

CREATE TABLE tor.RC (
  id                   integer  NOT NULL  generated always as identity,
  src_date             bigint    ,
  type                 varchar(64)   DEFAULT 'network-status-consensus-3 1.0' ,
  network_status_version smallint   DEFAULT 3 ,
  vote_status          varchar(16)   DEFAULT 'consensus' ,
  consensus_method     smallint    ,
  consensus_flavor     varchar(32)    ,
  valid_after          bigint    ,
  fresh_until          bigint    ,
  valid_until          bigint    ,
  contact              varchar(256)    ,
  legacy_dir_key       varchar(64)    ,
  signing_key_digest   varchar(40)    ,
  CONSTRAINT Pk_rc PRIMARY KEY ( id ),
  CONSTRAINT Idx_RC UNIQUE ( valid_after )
);

CREATE INDEX Idx_RC_src_date ON tor.RC ( src_date );

CREATE TABLE tor.RC_Authorities (
  id                   integer  NOT NULL  generated always as identity,
  RC_id                integer  NOT NULL  ,
  authority            varchar(40)    ,
  contact              varchar(256)    ,
  vote_digest          varchar(40)    ,
  CONSTRAINT Pk_RC_Authorities PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_Authorities FOREIGN KEY ( RC_id ) REFERENCES tor.RC( id )
);

CREATE INDEX idx_RC_Authorities ON tor.RC_Authorities ( RC_id );

CREATE TABLE tor.RC_Authority_DirSource (
  id                   integer  NOT NULL  ,
  nickname             varchar(19)    ,
  "identity"           varchar(40)    ,
  hostname             varchar(64)    ,
  address              varchar(15)    ,
  dir_port             integer    ,
  or_port              integer    ,
  is_legacy            char(1)    ,
  CONSTRAINT Pk_RC_Authority_DirSource PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_Authority_DirSource FOREIGN KEY ( id ) REFERENCES tor.RC_Authorities( id )
);

CREATE TABLE tor.RC_ClientVersions (
  id                   integer  NOT NULL  generated always as identity,
  RC_id                integer  NOT NULL  ,
  version              varchar(32)    ,
  CONSTRAINT Pk_RC_ClientVersions PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_ClientVersions FOREIGN KEY ( RC_id ) REFERENCES tor.RC( id )
);

CREATE INDEX idx_RC_ClientVersions ON tor.RC_ClientVersions ( RC_id );

CREATE TABLE tor.RC_DirFooter (
  id                   integer  NOT NULL  ,
  consensus_digest     varchar(40)    ,
  CONSTRAINT Pk_RC_DirFooter PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_DirFooter FOREIGN KEY ( id ) REFERENCES tor.RC( id )
);

CREATE TABLE tor.RC_DirFooter_BandwidthWeights (
  id                   integer  NOT NULL  generated always as identity,
  RC_DirFooter_Id      integer  NOT NULL  ,
  weight               varchar(32)    ,
  value                integer    ,
  CONSTRAINT Pk_RC_DirFooter_BandwidthWeights PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_DirFooter_BandwidthWeights FOREIGN KEY ( RC_DirFooter_Id ) REFERENCES tor.RC_DirFooter( id )
);

CREATE INDEX idx_RC_DirFooter_BandwidthWeights ON tor.RC_DirFooter_BandwidthWeights ( RC_DirFooter_Id );

CREATE TABLE tor.RC_DirFooter_DirSig (
  id                   integer  NOT NULL  generated always as identity,
  RC_DirFooter_Id      integer  NOT NULL  ,
  algorithm            varchar(16)    ,
  "identity"           varchar(40)    ,
  signing_key_digest   varchar(40)    ,
  signature            char(1)    ,
  CONSTRAINT Pk_RC_DirFooter_DirSig PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_DirFooter_DirSig FOREIGN KEY ( RC_DirFooter_Id ) REFERENCES tor.RC_DirFooter( id )
);

CREATE INDEX idx_RC_DirFooter_DirSig ON tor.RC_DirFooter_DirSig ( RC_DirFooter_Id );

CREATE TABLE tor.RC_KnownFlags (
  id                   integer  NOT NULL  generated always as identity,
  RC_id                integer  NOT NULL  ,
  flag                 varchar(32)    ,
  CONSTRAINT Pk_RC_KnownFlags PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_KnownFlags FOREIGN KEY ( RC_id ) REFERENCES tor.RC( id )
);

CREATE INDEX idx_RC_KnownFlags ON tor.RC_KnownFlags ( RC_id );

CREATE TABLE tor.RC_Params (
  id                   integer  NOT NULL  generated always as identity,
  RC_id                integer  NOT NULL  ,
  param                varchar(32)    ,
  value                smallint    ,
  CONSTRAINT Pk_RC_Params PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_Params FOREIGN KEY ( RC_id ) REFERENCES tor.RC( id )
);

CREATE INDEX idx_RC_Params ON tor.RC_Params ( RC_id );

CREATE TABLE tor.RC_ServerVersions (
  id                   integer  NOT NULL  generated always as identity,
  RC_id                integer  NOT NULL  ,
  version              varchar(32)    ,
  CONSTRAINT Pk_RC_ServerVersions PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_ServerVersions FOREIGN KEY ( RC_id ) REFERENCES tor.RC( id )
);

CREATE INDEX idx_RC_ServerVersions ON tor.RC_ServerVersions ( RC_id );

CREATE TABLE tor.RC_Status (
  id                   integer  NOT NULL  generated always as identity,
  RC_id                integer  NOT NULL  ,
  server               varchar(40)    ,
  v                    varchar(32)    ,
  CONSTRAINT Pk_RC_Status PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_Status FOREIGN KEY ( RC_id ) REFERENCES tor.RC( id )
);

CREATE INDEX idx_RC_Status ON tor.RC_Status ( RC_id );

CREATE TABLE tor.RC_Status_A (
  id                   integer  NOT NULL  generated always as identity,
  RC_Status_Id         integer  NOT NULL  ,
  address              varchar(256)    ,
  port                 integer    ,
  CONSTRAINT Pk_RC_Status_A PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_Status_A FOREIGN KEY ( RC_Status_Id ) REFERENCES tor.RC_Status( id )
);

CREATE INDEX idx_RC_Status_A ON tor.RC_Status_A ( RC_Status_Id );

CREATE TABLE tor.RC_Status_P (
  id                   integer  NOT NULL  ,
  default_policy       varchar(24)    ,
  port_summary         varchar(4096)    ,
  CONSTRAINT Pk_RC_Status_P PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_Status_P FOREIGN KEY ( id ) REFERENCES tor.RC_Status( id )
);

CREATE TABLE tor.RC_Status_R (
  id                   integer  NOT NULL  ,
  nickname             varchar(19)    ,
  "identity"           varchar(40)    ,
  digest               varchar(40)    ,
  publication          bigint    ,
  ip                   varchar(15)    ,
  or_port              integer    ,
  dir_port             integer    ,
  CONSTRAINT Pk_RC_Status_R PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_Status_R FOREIGN KEY ( id ) REFERENCES tor.RC_Status( id )
);

CREATE TABLE tor.RC_Status_S (
  id                   integer  NOT NULL  generated always as identity,
  RC_Status_id         integer  NOT NULL  ,
  flag                 varchar(32)    ,
  CONSTRAINT Pk_RC_Status_S PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_Status_S FOREIGN KEY ( RC_Status_id ) REFERENCES tor.RC_Status( id )
);

CREATE INDEX idx_RC_Status_S ON tor.RC_Status_S ( RC_Status_id );

CREATE TABLE tor.RC_Status_W (
  id                   integer  NOT NULL  ,
  bandwidth            smallint    ,
  measured             smallint    ,
  unmeasured           char(1)    ,
  CONSTRAINT Pk_RC_Status_W PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_Status_W FOREIGN KEY ( id ) REFERENCES tor.RC_Status( id )
);

CREATE TABLE tor.RC_VotingDelay (
  id                   integer  NOT NULL  ,
  vote_seconds         bigint    ,
  dist_seconds         bigint    ,
  CONSTRAINT Pk_RC_VotingDelay PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_VotingDelay FOREIGN KEY ( id ) REFERENCES tor.RC( id )
);

CREATE TABLE tor.RC_package (
  id                   integer  NOT NULL  generated always as identity,
  RC_id                integer  NOT NULL  ,
  version              varchar(32)    ,
  CONSTRAINT Pk_RC_package PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_package FOREIGN KEY ( RC_id ) REFERENCES tor.RC( id )
);

CREATE INDEX idx_RC_package ON tor.RC_package ( RC_id );

CREATE TABLE tor.RV (
  id                   integer  NOT NULL  generated always as identity,
  src_date             bigint    ,
  type                 varchar(64)   DEFAULT 'network-status-vote-3 1.0' ,
  network_status_version smallint   DEFAULT 3 ,
  vote_status          varchar(16)   DEFAULT 'vote' ,
  published            bigint    ,
  valid_after          bigint    ,
  fresh_until          bigint    ,
  valid_until          bigint    ,
  contact              varchar(256)    ,
  legacy_dir_key       varchar(64)    ,
  signing_key_digest   varchar(40)    ,
  DirSource_identity   varchar(40)    ,
  CONSTRAINT Pk_rv PRIMARY KEY ( id ),
  CONSTRAINT Idx_RV UNIQUE ( valid_after, DirSource_identity )
);

CREATE INDEX Idx_RV_src_date ON tor.RV ( src_date );

CREATE TABLE tor.RV_ClientVersions (
  id                   integer  NOT NULL  generated always as identity,
  RV_id                integer  NOT NULL  ,
  version              varchar(32)    ,
  CONSTRAINT Pk_RV_ClientVersions PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_ClientVersions FOREIGN KEY ( RV_id ) REFERENCES tor.RV( id )
);

CREATE INDEX idx_RV_ClientVersions ON tor.RV_ClientVersions ( RV_id );

CREATE TABLE tor.RV_ConsensusMethod (
  id                   integer  NOT NULL  generated always as identity,
  RV_id                integer  NOT NULL  ,
  value                smallint    ,
  CONSTRAINT Pk_RV_ConsensusMethod PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_ConsensusMethod FOREIGN KEY ( RV_id ) REFERENCES tor.RV( id )
);

CREATE INDEX idx_RV_ConsensusMethod ON tor.RV_ConsensusMethod ( RV_id );

CREATE TABLE tor.RV_DirFooter_DirSig (
  id                   integer  NOT NULL  ,
  RV_id                integer  NOT NULL  ,
  algorithm            varchar(16)    ,
  "identity"           varchar(40)    ,
  signing_key_digest   varchar(40)    ,
  signature            char(1)    ,
  CONSTRAINT Pk_RV_DirFooter PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_DirFooter_DirSig FOREIGN KEY ( RV_id ) REFERENCES tor.RV( id )
);

CREATE INDEX idx_RV_DirFooter_DirSig ON tor.RV_DirFooter_DirSig ( RV_id );

CREATE TABLE tor.RV_DirKey (
  id                   integer  NOT NULL  ,
  version              smallint    ,
  dir_key_published    bigint    ,
  dir_key_expires      bigint    ,
  CONSTRAINT Pk_RV_DirKey PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_DirKey FOREIGN KEY ( id ) REFERENCES tor.RV( id )
);

CREATE TABLE tor.RV_DirSource (
  id                   integer  NOT NULL  ,
  nickname             varchar(19)    ,
  "identity"           varchar(40)    ,
  hostname             varchar(64)    ,
  address              varchar(15)    ,
  dir_port             integer    ,
  or_port              integer    ,
  CONSTRAINT Pk_RV_Status_Router PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_Status_Router FOREIGN KEY ( id ) REFERENCES tor.RV( id )
);

CREATE TABLE tor.RV_FlagTresholds (
  id                   integer  NOT NULL  ,
  stable_uptime        bigint    ,
  stable_mtbf          bigint    ,
  enough_mtbf          integer    ,
  fast_speed           integer    ,
  guard_wfu            double    ,
  guard_tk             bigint    ,
  guard_bw_inc_exits   bigint    ,
  guard_bw_exc_exits   bigint    ,
  ignoring_advertised_bws integer    ,
  CONSTRAINT Pk_RV_FlagTresholds PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_FlagTresholds FOREIGN KEY ( id ) REFERENCES tor.RV( id )
);

CREATE TABLE tor.RV_KnownFlags (
  id                   integer  NOT NULL  generated always as identity,
  RV_id                integer  NOT NULL  ,
  flag                 varchar(32)    ,
  CONSTRAINT Pk_RV_KnownFlags PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_KnownFlags FOREIGN KEY ( RV_id ) REFERENCES tor.RV( id )
);

CREATE INDEX idx_RV_KnownFlags ON tor.RV_KnownFlags ( RV_id );

CREATE TABLE tor.RV_Params (
  id                   integer  NOT NULL  generated always as identity,
  RV_id                integer  NOT NULL  ,
  param                varchar(32)    ,
  value                smallint    ,
  CONSTRAINT Pk_RV_Status_Params PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_Status_Params FOREIGN KEY ( RV_id ) REFERENCES tor.RV( id )
);

CREATE INDEX idx_RV_Status_Params ON tor.RV_Params ( RV_id );

CREATE TABLE tor.RV_ServerVersions (
  id                   integer  NOT NULL  generated always as identity,
  RV_id                integer  NOT NULL  ,
  version              varchar(32)    ,
  CONSTRAINT Pk_RV_Status_ServerVersions PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_Status_ServerVersions FOREIGN KEY ( RV_id ) REFERENCES tor.RV( id )
);

CREATE INDEX idx_RV_Status_ServerVersions ON tor.RV_ServerVersions ( RV_id );

CREATE TABLE tor.RV_Status (
  id                   integer  NOT NULL  generated always as identity,
  RV_id                integer  NOT NULL  ,
  v                    varchar(32)    ,
  ed25519              char(1)    ,
  CONSTRAINT Pk_RV_Status PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_Status FOREIGN KEY ( RV_id ) REFERENCES tor.RV( id )
);

CREATE INDEX idx_RV_Status ON tor.RV_Status ( RV_id );

CREATE TABLE tor.RV_Status_A (
  id                   integer  NOT NULL  generated always as identity,
  RV_Status_id         integer  NOT NULL  ,
  address              varchar(256)    ,
  port                 integer    ,
  CONSTRAINT Pk_RV_Status_A PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_Status_A FOREIGN KEY ( RV_Status_id ) REFERENCES tor.RV_Status( id )
);

CREATE INDEX idx_RV_Status_A ON tor.RV_Status_A ( RV_Status_id );

CREATE TABLE tor.RV_Status_P (
  id                   integer  NOT NULL  ,
  default_policy       varchar(24)    ,
  port_summary         varchar(4096)    ,
  CONSTRAINT Pk_RV_Status_P PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_Status_P FOREIGN KEY ( id ) REFERENCES tor.RV_Status( id )
);

CREATE TABLE tor.RV_Status_R (
  id                   integer  NOT NULL  ,
  nickname             varchar(19)    ,
  "identity"           varchar(40)    ,
  digest               varchar(40)    ,
  publication          bigint    ,
  ip                   varchar(15)    ,
  or_port              integer    ,
  dir_port             integer    ,
  CONSTRAINT Pk_RV_Status_R PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_Status_R FOREIGN KEY ( id ) REFERENCES tor.RV_Status( id )
);

CREATE TABLE tor.RV_Status_S (
  id                   integer  NOT NULL  generated always as identity,
  RV_Status_id         integer  NOT NULL  ,
  flag                 varchar(32)    ,
  CONSTRAINT Pk_RV_Status_S PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_Status_S FOREIGN KEY ( RV_Status_id ) REFERENCES tor.RV_Status( id )
);

CREATE INDEX idx_RV_Status_S ON tor.RV_Status_S ( RV_Status_id );

CREATE TABLE tor.RV_Status_W (
  id                   integer  NOT NULL  ,
  bandwidth            smallint    ,
  measured             smallint    ,
  unmeasured           char(1)    ,
  CONSTRAINT Pk_RV_Status_W PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_Status_W FOREIGN KEY ( id ) REFERENCES tor.RV_Status( id )
);

CREATE TABLE tor.RV_VotingDelay (
  id                   integer  NOT NULL  ,
  vote_seconds         bigint    ,
  dist_seconds         bigint    ,
  CONSTRAINT Pk_RV_VotingDelay PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_VotingDelay FOREIGN KEY ( id ) REFERENCES tor.RV( id )
);

CREATE TABLE tor.RV_package (
  id                   integer  NOT NULL  generated always as identity,
  RV_id                integer  NOT NULL  ,
  package              varchar(512)    ,
  CONSTRAINT Pk_RV_package PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_package FOREIGN KEY ( RV_id ) REFERENCES tor.RV( id )
);

CREATE INDEX idx_RV_package ON tor.RV_package ( RV_id );

CREATE TABLE tor.RX (
  id                   integer  NOT NULL  generated always as identity,
  src_date             bigint    ,
  type                 varchar(64)   DEFAULT 'extra-info 1.0' ,
  identity_ed25519     char(1)    ,
  published            bigint    ,
  geoip_db_digest      varchar(40)    ,
  geoip6_db_digest     varchar(40)    ,
  geoip_start_time     bigint    ,
  dirreq_v2_share      double    ,
  dirreq_v3_share      double    ,
  cell_circuits_per_decile integer    ,
  router_sig_ed25519   char(1)    ,
  router_signature     char(1)    ,
  extra_info_digest    varchar(40)    ,
  extra_info_digest_sha256 varchar(43)    ,
  master_key_ed25519   varchar(43)    ,
  CONSTRAINT Pk_be_0 PRIMARY KEY ( id ),
  CONSTRAINT Idx_RX UNIQUE ( published, extra_info_digest )
);

CREATE INDEX Idx_RX_src_date ON tor.RX ( src_date );

CREATE TABLE tor.RX_CellProcessedCells (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  mean                 integer    ,
  CONSTRAINT Pk_RX_CellProcessedCells PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_CellProcessedCells FOREIGN KEY ( RX_id ) REFERENCES tor.RX( id )
);

CREATE INDEX idx_RX_CellProcessedCells ON tor.RX_CellProcessedCells ( RX_id );

CREATE TABLE tor.RX_CellQueuedCells (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  mean                 double    ,
  CONSTRAINT Pk_RX_CellQueuedCells PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_CellQueuedCells FOREIGN KEY ( RX_id ) REFERENCES tor.RX( id )
);

CREATE INDEX idx_RX_CellQueuedCells ON tor.RX_CellQueuedCells ( RX_id );

CREATE TABLE tor.RX_CellStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_RX_CellStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_CellStatsEnd FOREIGN KEY ( id ) REFERENCES tor.RX( id )
);

CREATE TABLE tor.RX_CellTimeInQueue (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  mean                 integer    ,
  CONSTRAINT Pk_RX_CellTimeInQueue PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_CellTimeInQueue FOREIGN KEY ( RX_id ) REFERENCES tor.RX( id )
);

CREATE INDEX idx_RX_CellTimeInQueue ON tor.RX_CellTimeInQueue ( RX_id );

CREATE TABLE tor.RX_ConnBiDirect (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  below                integer    ,
  "read"               integer    ,
  "write"              integer    ,
  "both"               integer    ,
  CONSTRAINT Pk_RX_ConnBiDirect PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_ConnBiDirect FOREIGN KEY ( id ) REFERENCES tor.RX( id )
);

CREATE TABLE tor.RX_DirreqReadHistory (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT idx_BX_DirreqWriteHistory UNIQUE ( id ),
  CONSTRAINT Pk_RX_DirreqReadHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqReadHistory FOREIGN KEY ( id ) REFERENCES tor.RX( id )
);

CREATE TABLE tor.RX_DirreqReadHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  RX_DirreqRead_id     integer  NOT NULL  ,
  bytes                bigint  NOT NULL  ,
  CONSTRAINT Pk_RX_DirreqReadHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqReadHistory_Bytes FOREIGN KEY ( RX_DirreqRead_id ) REFERENCES tor.RX_DirreqReadHistory( id )
);

CREATE INDEX idx_RX_DirreqReadHistory_Bytes ON tor.RX_DirreqReadHistory_Bytes ( RX_DirreqRead_id );

CREATE TABLE tor.RX_DirreqStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_RX_DirreqStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqStatsEnd FOREIGN KEY ( id ) REFERENCES tor.RX( id )
);

CREATE TABLE tor.RX_DirreqV2DirectDl (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  "key"                varchar(32)    ,
  value                integer    ,
  CONSTRAINT Pk_RX_DirreqV2DirectDl PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV2DirectDl FOREIGN KEY ( RX_id ) REFERENCES tor.RX( id )
);

CREATE INDEX idx_BX_DirreqV3DirectDl ON tor.RX_DirreqV2DirectDl ( RX_id );

CREATE TABLE tor.RX_DirreqV2Ips (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_RX_DirreqV2Ips PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV2Ips FOREIGN KEY ( RX_id ) REFERENCES tor.RX( id )
);

CREATE INDEX idx_RX_DirreqV2Ips ON tor.RX_DirreqV2Ips ( RX_id );

CREATE TABLE tor.RX_DirreqV2Reqs (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_RX_DirreqV2Reqs PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV2Reqs FOREIGN KEY ( RX_id ) REFERENCES tor.RX( id )
);

CREATE INDEX idx_RX_DirreqV2Reqs ON tor.RX_DirreqV2Reqs ( RX_id );

CREATE TABLE tor.RX_DirreqV2Resp (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  response             varchar(256)    ,
  count                integer    ,
  CONSTRAINT Pk_RX_DirreqV2Resp PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV2Resp FOREIGN KEY ( RX_id ) REFERENCES tor.RX( id )
);

CREATE INDEX idx_RX_DirreqV2Resp ON tor.RX_DirreqV2Resp ( RX_id );

CREATE TABLE tor.RX_DirreqV2TunneledDl (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  "key"                varchar(32)    ,
  value                integer    ,
  CONSTRAINT Pk_RX_DirreqV2TunneledDl PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV2TunneledDl FOREIGN KEY ( RX_id ) REFERENCES tor.RX( id )
);

CREATE INDEX idx_BX_DirreqV3TunneledDl ON tor.RX_DirreqV2TunneledDl ( RX_id );

CREATE TABLE tor.RX_DirreqV3DirectDl (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  "key"                varchar(32)    ,
  value                integer    ,
  CONSTRAINT Pk_RX_DirreqV3DirectDl PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV3DirectDl FOREIGN KEY ( RX_id ) REFERENCES tor.RX( id )
);

CREATE INDEX idx_RX_DirreqV3DirectDl ON tor.RX_DirreqV3DirectDl ( RX_id );

CREATE TABLE tor.RX_DirreqV3Ips (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_RX_DirreqV3Ips PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV3Ips FOREIGN KEY ( RX_id ) REFERENCES tor.RX( id )
);

CREATE INDEX idx_RX_DirreqV3Ips ON tor.RX_DirreqV3Ips ( RX_id );

CREATE TABLE tor.RX_DirreqV3Reqs (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_RX_DirreqV3Reqs PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV3Reqs FOREIGN KEY ( RX_id ) REFERENCES tor.RX( id )
);

CREATE INDEX idx_RX_DirreqV3Reqs ON tor.RX_DirreqV3Reqs ( RX_id );

CREATE TABLE tor.RX_DirreqV3Resp (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  response             varchar(256)    ,
  count                integer    ,
  CONSTRAINT Pk_RX_DirreqV3Resp PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV3Resp FOREIGN KEY ( RX_id ) REFERENCES tor.RX( id )
);

CREATE INDEX idx_RX_DirreqV3Resp ON tor.RX_DirreqV3Resp ( RX_id );

CREATE TABLE tor.RX_DirreqV3TunneledDl (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  "key"                varchar(32)    ,
  value                integer    ,
  CONSTRAINT Pk_RX_DirreqV3TunneledDl PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV3TunneledDl FOREIGN KEY ( RX_id ) REFERENCES tor.RX( id )
);

CREATE INDEX idx_RX_DirreqV3TunneledDl ON tor.RX_DirreqV3TunneledDl ( RX_id );

CREATE TABLE tor.RX_DirreqWriteHistory (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_RX_DirreqWriteHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqWriteHistory FOREIGN KEY ( id ) REFERENCES tor.RX( id )
);

CREATE TABLE tor.RX_DirreqWriteHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  RX_DirreqWrite_id    integer  NOT NULL  ,
  bytes                bigint  NOT NULL  ,
  CONSTRAINT Pk_RX_DirreqWriteHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqWriteHistory_Bytes FOREIGN KEY ( RX_DirreqWrite_id ) REFERENCES tor.RX_DirreqWriteHistory( id )
);

CREATE INDEX idx_RX_DirreqWriteHistory_Bytes ON tor.RX_DirreqWriteHistory_Bytes ( RX_DirreqWrite_id );

CREATE TABLE tor.RX_EntryIps (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_RX_EntryIps PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_EntryIps FOREIGN KEY ( RX_id ) REFERENCES tor.RX( id )
);

CREATE INDEX idx_RX_EntryIps ON tor.RX_EntryIps ( RX_id );

CREATE TABLE tor.RX_EntryStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_RX_EntryStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_EntryStatsEnd FOREIGN KEY ( id ) REFERENCES tor.RX( id )
);

CREATE TABLE tor.RX_ExitKibibytesRead (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  port                 varchar(5)    ,
  kib                  bigint    ,
  CONSTRAINT Pk_RX_ExitKibibytesRead PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_ExitKibibytesRead FOREIGN KEY ( RX_id ) REFERENCES tor.RX( id )
);

CREATE INDEX idx_RX_ExitKibibytesRead ON tor.RX_ExitKibibytesRead ( RX_id );

CREATE TABLE tor.RX_ExitKibibytesWritten (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  port                 varchar(5)    ,
  kib                  bigint    ,
  CONSTRAINT Pk_RX_ExitKibibytesWritten PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_ExitKibibytesWritten FOREIGN KEY ( RX_id ) REFERENCES tor.RX( id )
);

CREATE INDEX idx_RX_ExitKibibytesWritten ON tor.RX_ExitKibibytesWritten ( RX_id );

CREATE TABLE tor.RX_ExitStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_RX_ExitStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_ExitStatsEnd FOREIGN KEY ( id ) REFERENCES tor.RX( id )
);

CREATE TABLE tor.RX_ExitStreamsOpened (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  port                 varchar(5)    ,
  count                bigint    ,
  CONSTRAINT Pk_RX_ExitStreamsOpened PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_ExitStreamsOpened FOREIGN KEY ( RX_id ) REFERENCES tor.RX( id )
);

CREATE INDEX idx_RX_ExitStreamsOpened ON tor.RX_ExitStreamsOpened ( RX_id );

CREATE TABLE tor.RX_ExtraInfo (
  id                   integer  NOT NULL  ,
  nickname             varchar(19)    ,
  fingerprint          varchar(40)    ,
  CONSTRAINT Pk_RX_ExtraInfo PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_ExtraInfo FOREIGN KEY ( id ) REFERENCES tor.RX( id )
);

CREATE TABLE tor.RX_HidservDirOnionsSeen (
  id                   integer  NOT NULL  ,
  onions               double    ,
  CONSTRAINT Pk_Table_1 PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_HidservDirOnionsSeen FOREIGN KEY ( id ) REFERENCES tor.RX( id )
);

CREATE TABLE tor.RX_HidservDirOnionsSeen_Obfuscation (
  id                   integer  NOT NULL  generated always as identity,
  RX_Onion_id          integer  NOT NULL  ,
  obfuscation          varchar(32)    ,
  value                double    ,
  CONSTRAINT Pk_RX_HidservDirOnionsSeen_Obfuscation PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_HidservDirOnionsSeen_Obfuscation FOREIGN KEY ( RX_Onion_id ) REFERENCES tor.RX_HidservDirOnionsSeen( id )
);

CREATE INDEX idx_RX_HidservDirOnionsSeen_Obfuscation ON tor.RX_HidservDirOnionsSeen_Obfuscation ( RX_Onion_id );

CREATE TABLE tor.RX_HidservRendRelayedCells (
  id                   integer  NOT NULL  ,
  cells                double    ,
  CONSTRAINT Pk_RX_HidservRendRelayedCells PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_HidservRendRelayedCells FOREIGN KEY ( id ) REFERENCES tor.RX( id )
);

CREATE TABLE tor.RX_HidservRendRelayedCells_Obfuscation (
  id                   integer  NOT NULL  generated always as identity,
  RX_Relay_id          integer  NOT NULL  ,
  obfuscation          varchar(32)    ,
  value                double    ,
  CONSTRAINT Pk_RX_HidservRendRelayedCells_Obfuscation PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_HidservRendRelayedCells_Obfuscation FOREIGN KEY ( RX_Relay_id ) REFERENCES tor.RX_HidservRendRelayedCells( id )
);

CREATE INDEX idx_RX_HidservRendRelayedCells_Obfuscation ON tor.RX_HidservRendRelayedCells_Obfuscation ( RX_Relay_id );

CREATE TABLE tor.RX_HidservStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_RX_HidservStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_HidservStatsEnd FOREIGN KEY ( id ) REFERENCES tor.RX( id )
);

CREATE TABLE tor.RX_ReadHistory (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_RX_ReadHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_ReadHistory FOREIGN KEY ( id ) REFERENCES tor.RX( id )
);

CREATE TABLE tor.RX_ReadHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  RX_Read_id           integer  NOT NULL  ,
  bytes                bigint    ,
  CONSTRAINT Pk_RX_ReadHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_ReadHistory_Bytes FOREIGN KEY ( RX_Read_id ) REFERENCES tor.RX_ReadHistory( id )
);

CREATE INDEX idx_RX_ReadHistory_Bytes ON tor.RX_ReadHistory_Bytes ( RX_Read_id );

CREATE TABLE tor.RX_Transport (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  transport            varchar(32)    ,
  CONSTRAINT Pk_RX_Transport PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_Transport FOREIGN KEY ( RX_id ) REFERENCES tor.RX( id )
);

CREATE INDEX idx_RX_Transport ON tor.RX_Transport ( RX_id );

CREATE TABLE tor.RX_WriteHistory (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT idx_RX_WriteHistory UNIQUE ( id ),
  CONSTRAINT Pk_RX_WriteHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_WriteHistory FOREIGN KEY ( id ) REFERENCES tor.RX( id )
);

CREATE TABLE tor.RX_WriteHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  RX_Write_id          integer  NOT NULL  ,
  bytes                bigint    ,
  CONSTRAINT Pk_RX_WriteHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_WriteHistory_Bytes FOREIGN KEY ( RX_Write_id ) REFERENCES tor.RX_WriteHistory( id )
);

CREATE INDEX idx_RX_WriteHistory_Bytes ON tor.RX_WriteHistory_Bytes ( RX_Write_id );

CREATE TABLE tor.R_Bandwidth (
  id                   integer  NOT NULL  ,
  "avg"                integer    ,
  burst                integer    ,
  observed             integer    ,
  CONSTRAINT Pk_r_Bandwidth PRIMARY KEY ( id ),
  CONSTRAINT fk_r_Bandwidth FOREIGN KEY ( id ) REFERENCES tor.R( id )
);

CREATE TABLE tor.R_CircuitProtocolVersions (
  id                   integer  NOT NULL  generated always as identity,
  R_id                 integer  NOT NULL  ,
  circuit_protocol_versions smallint  NOT NULL  ,
  CONSTRAINT Pk_r_CircuitProtocolVersions PRIMARY KEY ( id ),
  CONSTRAINT fk_r_CircuitProtocolVersions FOREIGN KEY ( R_id ) REFERENCES tor.R( id )
);

CREATE INDEX idx_r_CircuitProtocolVersions ON tor.R_CircuitProtocolVersions ( R_id );

CREATE TABLE tor.R_ExitPolicy (
  id                   integer  NOT NULL  generated always as identity,
  R_id                 integer  NOT NULL  ,
  exit_policy          varchar(256)  NOT NULL  ,
  CONSTRAINT Pk_r_ExitPolicy PRIMARY KEY ( id ),
  CONSTRAINT fk_r_ExitPolicy FOREIGN KEY ( R_id ) REFERENCES tor.R( id )
);

CREATE INDEX idx_r_ExitPolicy ON tor.R_ExitPolicy ( R_id );

CREATE TABLE tor.R_Family (
  id                   integer  NOT NULL  generated always as identity,
  R_id                 integer  NOT NULL  ,
  family               varchar(256)  NOT NULL  ,
  CONSTRAINT Pk_r_Family PRIMARY KEY ( id ),
  CONSTRAINT fk_r_Family FOREIGN KEY ( R_id ) REFERENCES tor.R( id )
);

CREATE INDEX idx_r_Family ON tor.R_Family ( R_id );

CREATE TABLE tor.R_HiddenserviceDir (
  id                   integer  NOT NULL  generated always as identity,
  R_id                 integer  NOT NULL  ,
  hidden_service_dir   smallint  NOT NULL  ,
  CONSTRAINT Pk_r_HiddenserviceDir PRIMARY KEY ( id ),
  CONSTRAINT fk_r_HiddenserviceDir FOREIGN KEY ( R_id ) REFERENCES tor.R( id )
);

CREATE INDEX idx_r_HiddenserviceDir ON tor.R_HiddenserviceDir ( R_id );

CREATE TABLE tor.R_LinkProtocolVersions (
  id                   integer  NOT NULL  generated always as identity,
  R_id                 integer  NOT NULL  ,
  link_protocol_versions smallint  NOT NULL  ,
  CONSTRAINT Pk_r_LinkProtocolVersions PRIMARY KEY ( id ),
  CONSTRAINT fk_r_LinkProtocolVersions FOREIGN KEY ( R_id ) REFERENCES tor.R( id )
);

CREATE INDEX idx_r_LinkProtocolVersions ON tor.R_LinkProtocolVersions ( R_id );

CREATE TABLE tor.R_OrAdress (
  id                   integer  NOT NULL  generated always as identity,
  R_id                 integer  NOT NULL  ,
  adress               varchar(256)  NOT NULL  ,
  port                 smallint  NOT NULL  ,
  CONSTRAINT Pk_r_OrAdress PRIMARY KEY ( id ),
  CONSTRAINT fk_r_OrAdress FOREIGN KEY ( R_id ) REFERENCES tor.R( id )
);

CREATE INDEX idx_r_OrAdress ON tor.R_OrAdress ( R_id );

CREATE TABLE tor.R_ReadHistory (
  id                   integer  NOT NULL  ,
  date                 bigint  NOT NULL  ,
  interval             bigint  NOT NULL  ,
  CONSTRAINT Pk_r_ReadHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_r_ReadHistory FOREIGN KEY ( id ) REFERENCES tor.R( id )
);

CREATE TABLE tor.R_ReadHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  R_Read_id            integer  NOT NULL  ,
  bytes                bigint  NOT NULL  ,
  CONSTRAINT Pk_r_ReadHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_r_ReadHistory_Bytes FOREIGN KEY ( R_Read_id ) REFERENCES tor.R_ReadHistory( id )
);

CREATE INDEX idx_r_ReadHistory_Bytes ON tor.R_ReadHistory_Bytes ( R_Read_id );

CREATE TABLE tor.R_Router (
  id                   integer  NOT NULL  ,
  nickname             varchar(19)    ,
  address              varchar(256)    ,
  or_port              integer    ,
  socks_port           integer    ,
  dir_port             integer    ,
  CONSTRAINT Pk_r_Router PRIMARY KEY ( id ),
  CONSTRAINT fk_r_Router FOREIGN KEY ( id ) REFERENCES tor.R( id )
);

CREATE TABLE tor.R_WriteHistory (
  id                   integer  NOT NULL  ,
  date                 bigint  NOT NULL  ,
  interval             bigint  NOT NULL  ,
  CONSTRAINT Pk_r_WriteHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_r_WriteHistory FOREIGN KEY ( id ) REFERENCES tor.R( id )
);

CREATE TABLE tor.R_WriteHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  R_Write_id           integer  NOT NULL  ,
  bytes                bigint  NOT NULL  ,
  CONSTRAINT Pk_r_WriteHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_r_WriteHistory_Bytes FOREIGN KEY ( R_Write_id ) REFERENCES tor.R_WriteHistory( id )
);

CREATE INDEX idx_r_WriteHistory_Bytes ON tor.R_WriteHistory_Bytes ( R_Write_id );

CREATE TABLE tor.T (
  id                   integer  NOT NULL  generated always as identity,
  src_date             bigint    ,
  type                 varchar(64)   DEFAULT 'torperf 1.0' ,
  source               varchar(256)    ,
  filesize             integer    ,
  start                bigint    ,
  socket               bigint    ,
  "connect"            bigint    ,
  negotiate            bigint    ,
  request              bigint    ,
  response             bigint    ,
  datarequest          bigint    ,
  dataresponse         bigint    ,
  datacomplete         bigint    ,
  writebytes           integer    ,
  readbytes            integer    ,
  didtimeout           char(1)    ,
  dataperc10           bigint    ,
  dataperc20           bigint    ,
  dataperc30           bigint    ,
  dataperc40           bigint    ,
  dataperc50           bigint    ,
  dataperc60           bigint    ,
  dataperc70           bigint    ,
  dataperc80           bigint    ,
  dataperc90           bigint    ,
  launch               bigint    ,
  used_at              bigint    ,
  timeout              bigint    ,
  quantile             double    ,
  circ_id              integer    ,
  used_by              integer    ,
  CONSTRAINT Pk_t PRIMARY KEY ( id ),
  CONSTRAINT Idx_T UNIQUE ( start, source, filesize )
);

CREATE INDEX Idx_T_src_date ON tor.T ( src_date );

CREATE TABLE tor.T_Buildtimes (
  id                   integer  NOT NULL  generated always as identity,
  T_id                 integer  NOT NULL  ,
  value                bigint    ,
  CONSTRAINT Pk_t_Buildtimes PRIMARY KEY ( id ),
  CONSTRAINT fk_t_Buildtimes FOREIGN KEY ( T_id ) REFERENCES tor.T( id )
);

CREATE INDEX idx_t_Buildtimes ON tor.T_Buildtimes ( T_id );

CREATE TABLE tor.T_Path (
  id                   integer  NOT NULL  generated always as identity,
  T_id                 integer  NOT NULL  ,
  fingerprint          varchar(40)    ,
  CONSTRAINT Pk_t_Path PRIMARY KEY ( id ),
  CONSTRAINT fk_t_Path FOREIGN KEY ( T_id ) REFERENCES tor.T( id )
);

CREATE INDEX idx_t_Path ON tor.T_Path ( T_id );
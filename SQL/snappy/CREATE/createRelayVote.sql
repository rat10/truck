ALTER TABLE APP.RV DROP CONSTRAINT IDX_RV;
ALTER TABLE APP.RV DROP CONSTRAINT PK_RV;
ALTER TABLE APP.RV_CLIENTVERSIONS DROP CONSTRAINT FK_RV_CLIENTVERSIONS;
ALTER TABLE APP.RV_CLIENTVERSIONS DROP CONSTRAINT PK_RV_CLIENTVERSIONS;
ALTER TABLE APP.RV_CONSENSUSMETHOD DROP CONSTRAINT FK_RV_CONSENSUSMETHOD;
ALTER TABLE APP.RV_CONSENSUSMETHOD DROP CONSTRAINT PK_RV_CONSENSUSMETHOD;
ALTER TABLE APP.RV_DIRFOOTER_DIRSIG DROP CONSTRAINT FK_RV_DIRFOOTER_DIRSIG;
ALTER TABLE APP.RV_DIRFOOTER_DIRSIG DROP CONSTRAINT PK_RV_DIRFOOTER;
ALTER TABLE APP.RV_DIRKEY DROP CONSTRAINT FK_RV_DIRKEY;
ALTER TABLE APP.RV_DIRKEY DROP CONSTRAINT PK_RV_DIRKEY;
ALTER TABLE APP.RV_DIRSOURCE DROP CONSTRAINT FK_RV_STATUS_ROUTER;
ALTER TABLE APP.RV_DIRSOURCE DROP CONSTRAINT PK_RV_STATUS_ROUTER;
ALTER TABLE APP.RV_FLAGTRESHOLDS DROP CONSTRAINT FK_RV_FLAGTRESHOLDS;
ALTER TABLE APP.RV_FLAGTRESHOLDS DROP CONSTRAINT PK_RV_FLAGTRESHOLDS;
ALTER TABLE APP.RV_KNOWNFLAGS DROP CONSTRAINT FK_RV_KNOWNFLAGS;
ALTER TABLE APP.RV_KNOWNFLAGS DROP CONSTRAINT PK_RV_KNOWNFLAGS;
ALTER TABLE APP.RV_PACKAGE DROP CONSTRAINT FK_RV_PACKAGE;
ALTER TABLE APP.RV_PACKAGE DROP CONSTRAINT PK_RV_PACKAGE;
ALTER TABLE APP.RV_PARAMS DROP CONSTRAINT FK_RV_STATUS_PARAMS;
ALTER TABLE APP.RV_PARAMS DROP CONSTRAINT PK_RV_STATUS_PARAMS;
ALTER TABLE APP.RV_SERVERVERSIONS DROP CONSTRAINT FK_RV_STATUS_SERVERVERSIONS;
ALTER TABLE APP.RV_SERVERVERSIONS DROP CONSTRAINT PK_RV_STATUS_SERVERVERSIONS;
ALTER TABLE APP.RV_STATUS DROP CONSTRAINT FK_RV_STATUS;
ALTER TABLE APP.RV_STATUS DROP CONSTRAINT PK_RV_STATUS;
ALTER TABLE APP.RV_STATUS_A DROP CONSTRAINT FK_RV_STATUS_A;
ALTER TABLE APP.RV_STATUS_A DROP CONSTRAINT PK_RV_STATUS_A;
ALTER TABLE APP.RV_STATUS_P DROP CONSTRAINT FK_RV_STATUS_P;
ALTER TABLE APP.RV_STATUS_P DROP CONSTRAINT PK_RV_STATUS_P;
ALTER TABLE APP.RV_STATUS_R DROP CONSTRAINT FK_RV_STATUS_R;
ALTER TABLE APP.RV_STATUS_R DROP CONSTRAINT PK_RV_STATUS_R;
ALTER TABLE APP.RV_STATUS_S DROP CONSTRAINT FK_RV_STATUS_S;
ALTER TABLE APP.RV_STATUS_S DROP CONSTRAINT PK_RV_STATUS_S;
ALTER TABLE APP.RV_STATUS_W DROP CONSTRAINT FK_RV_STATUS_W;
ALTER TABLE APP.RV_STATUS_W DROP CONSTRAINT PK_RV_STATUS_W;
ALTER TABLE APP.RV_VOTINGDELAY DROP CONSTRAINT FK_RV_VOTINGDELAY;
ALTER TABLE APP.RV_VOTINGDELAY DROP CONSTRAINT PK_RV_VOTINGDELAY;
DROP TABLE APP.RV;
DROP TABLE APP.RV_CLIENTVERSIONS;
DROP TABLE APP.RV_CONSENSUSMETHOD;
DROP TABLE APP.RV_DIRFOOTER_DIRSIG;
DROP TABLE APP.RV_DIRKEY;
DROP TABLE APP.RV_DIRSOURCE;
DROP TABLE APP.RV_FLAGTRESHOLDS;
DROP TABLE APP.RV_KNOWNFLAGS;
DROP TABLE APP.RV_PACKAGE;
DROP TABLE APP.RV_PARAMS;
DROP TABLE APP.RV_SERVERVERSIONS;
DROP TABLE APP.RV_STATUS;
DROP TABLE APP.RV_STATUS_A;
DROP TABLE APP.RV_STATUS_P;
DROP TABLE APP.RV_STATUS_R;
DROP TABLE APP.RV_STATUS_S;
DROP TABLE APP.RV_STATUS_W;
DROP TABLE APP.RV_VOTINGDELAY;



CREATE TABLE RV (
  id                   integer  NOT NULL  generated always as identity,
  src_date             bigint    ,
  type                 varchar(64)   DEFAULT 'network-status-vote-3 1.0' ,
  network_status_version smallint   DEFAULT 3 ,
  vote_status          varchar(16)   DEFAULT 'vote' ,
  published            bigint    ,
  valid_after          bigint    ,
  fresh_until          bigint    ,
  valid_until          bigint    ,
  contact              varchar(256)    ,
  legacy_dir_key       varchar(64)    ,
  signing_key_digest   varchar(40)    ,
  DirSource_identity   varchar(40)    ,
  CONSTRAINT Pk_rv PRIMARY KEY ( id ),
  CONSTRAINT Idx_RV UNIQUE ( valid_after, DirSource_identity )
);
CREATE INDEX Idx_RV_src_date ON RV ( src_date );


CREATE TABLE RV_ClientVersions (
  id                   integer  NOT NULL  generated always as identity,
  RV_id                integer  NOT NULL  ,
  version              varchar(32)    ,
  CONSTRAINT Pk_RV_ClientVersions PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_ClientVersions FOREIGN KEY ( RV_id ) REFERENCES RV( id )
);


CREATE TABLE RV_ConsensusMethod (
  id                   integer  NOT NULL  generated always as identity,
  RV_id                integer  NOT NULL  ,
  value                smallint    ,
  CONSTRAINT Pk_RV_ConsensusMethod PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_ConsensusMethod FOREIGN KEY ( RV_id ) REFERENCES RV( id )
);


CREATE TABLE RV_DirFooter_DirSig (
  id                   integer  NOT NULL  ,
  RV_id                integer  NOT NULL  ,
  algorithm            varchar(16)    ,
  "identity"           varchar(40)    ,
  signing_key_digest   varchar(40)    ,
  signature            char(7)    ,
  CONSTRAINT Pk_RV_DirFooter PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_DirFooter_DirSig FOREIGN KEY ( RV_id ) REFERENCES RV( id )
);


CREATE TABLE RV_DirKey (
  id                   integer  NOT NULL  ,
  version              smallint    ,
  dir_key_published    bigint    ,
  dir_key_expires      bigint    ,
  CONSTRAINT Pk_RV_DirKey PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_DirKey FOREIGN KEY ( id ) REFERENCES RV( id )
);


CREATE TABLE RV_DirSource (
  id                   integer  NOT NULL  ,
  nickname             varchar(19)    ,
  "identity"           varchar(40)    ,
  hostname             varchar(64)    ,
  address              varchar(15)    ,
  dir_port             integer    ,
  or_port              integer    ,
  CONSTRAINT Pk_RV_Status_Router PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_Status_Router FOREIGN KEY ( id ) REFERENCES RV( id )
);


CREATE TABLE RV_FlagTresholds (
  id                   integer  NOT NULL  ,
  stable_uptime        bigint    ,
  stable_mtbf          bigint    ,
  enough_mtbf          integer    ,
  fast_speed           integer    ,
  guard_wfu            double    ,
  guard_tk             bigint    ,
  guard_bw_inc_exits   bigint    ,
  guard_bw_exc_exits   bigint    ,
  ignoring_advertised_bws integer    ,
  CONSTRAINT Pk_RV_FlagTresholds PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_FlagTresholds FOREIGN KEY ( id ) REFERENCES RV( id )
);


CREATE TABLE RV_KnownFlags (
  id                   integer  NOT NULL  generated always as identity,
  RV_id                integer  NOT NULL  ,
  flag                 varchar(32)    ,
  CONSTRAINT Pk_RV_KnownFlags PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_KnownFlags FOREIGN KEY ( RV_id ) REFERENCES RV( id )
);


CREATE TABLE RV_Params (
  id                   integer  NOT NULL  generated always as identity,
  RV_id                integer  NOT NULL  ,
  param                varchar(32)    ,
  value                smallint    ,
  CONSTRAINT Pk_RV_Status_Params PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_Status_Params FOREIGN KEY ( RV_id ) REFERENCES RV( id )
);


CREATE TABLE RV_ServerVersions (
  id                   integer  NOT NULL  generated always as identity,
  RV_id                integer  NOT NULL  ,
  version              varchar(32)    ,
  CONSTRAINT Pk_RV_Status_ServerVersions PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_Status_ServerVersions FOREIGN KEY ( RV_id ) REFERENCES RV( id )
);


CREATE TABLE RV_Status (
  id                   integer  NOT NULL  generated always as identity,
  RV_id                integer  NOT NULL  ,
  v                    varchar(32)    ,
  ed25519              char(7)    ,
  CONSTRAINT Pk_RV_Status PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_Status FOREIGN KEY ( RV_id ) REFERENCES RV( id )
);


CREATE TABLE RV_Status_A (
  id                   integer  NOT NULL  generated always as identity,
  RV_Status_id         integer  NOT NULL  ,
  address              varchar(256)    ,
  port                 integer    ,
  CONSTRAINT Pk_RV_Status_A PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_Status_A FOREIGN KEY ( RV_Status_id ) REFERENCES RV_Status( id )
);


CREATE TABLE RV_Status_P (
  id                   integer  NOT NULL  ,
  default_policy       varchar(24)    ,
  port_summary         varchar(4096)    ,
  CONSTRAINT Pk_RV_Status_P PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_Status_P FOREIGN KEY ( id ) REFERENCES RV_Status( id )
);


CREATE TABLE RV_Status_R (
  id                   integer  NOT NULL  ,
  nickname             varchar(19)    ,
  "identity"           varchar(40)    ,
  digest               varchar(40)    ,
  publication          bigint    ,
  ip                   varchar(15)    ,
  or_port              integer    ,
  dir_port             integer    ,
  CONSTRAINT Pk_RV_Status_R PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_Status_R FOREIGN KEY ( id ) REFERENCES RV_Status( id )
);


CREATE TABLE RV_Status_S (
  id                   integer  NOT NULL  generated always as identity,
  RV_Status_id         integer  NOT NULL  ,
  flag                 varchar(32)    ,
  CONSTRAINT Pk_RV_Status_S PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_Status_S FOREIGN KEY ( RV_Status_id ) REFERENCES RV_Status( id )
);


CREATE TABLE RV_Status_W (
  id                   integer  NOT NULL  ,
  bandwidth            smallint    ,
  measured             smallint    ,
  unmeasured           char(7)    ,
  CONSTRAINT Pk_RV_Status_W PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_Status_W FOREIGN KEY ( id ) REFERENCES RV_Status( id )
);


CREATE TABLE RV_VotingDelay (
  id                   integer  NOT NULL  ,
  vote_seconds         bigint    ,
  dist_seconds         bigint    ,
  CONSTRAINT Pk_RV_VotingDelay PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_VotingDelay FOREIGN KEY ( id ) REFERENCES RV( id )
);


CREATE TABLE RV_package (
  id                   integer  NOT NULL  generated always as identity,
  RV_id                integer  NOT NULL  ,
  package              varchar(512)    ,
  CONSTRAINT Pk_RV_package PRIMARY KEY ( id ),
  CONSTRAINT fk_RV_package FOREIGN KEY ( RV_id ) REFERENCES RV( id )
);

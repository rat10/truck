ALTER TABLE APP.BX DROP CONSTRAINT IDX_BX;
ALTER TABLE APP.BX DROP CONSTRAINT PK_BE;
ALTER TABLE APP.BX_BRIDGEIPS DROP CONSTRAINT FK_BX_BRIDGEIPS;
ALTER TABLE APP.BX_BRIDGEIPS DROP CONSTRAINT PK_BX_BRIDGEIPS;
ALTER TABLE APP.BX_BRIDGEIPTRANSPORTS DROP CONSTRAINT FK_BX_BRIDGEIPTRANSPORTS;
ALTER TABLE APP.BX_BRIDGEIPTRANSPORTS DROP CONSTRAINT PK_BX_BRIDGEIPTRANSPORTS;
ALTER TABLE APP.BX_BRIDGEIPVERSIONS DROP CONSTRAINT FK_BX_BRIDGEIPVERSIONS;
ALTER TABLE APP.BX_BRIDGEIPVERSIONS DROP CONSTRAINT PK_BX_BRIDGEIPVERSIONS;
ALTER TABLE APP.BX_BRIDGESTATSEND DROP CONSTRAINT FK_BX_BRIDGESTATSEND;
ALTER TABLE APP.BX_BRIDGESTATSEND DROP CONSTRAINT PK_BX_BRIDGESTATSEND;
ALTER TABLE APP.BX_CELLPROCESSEDCELLS DROP CONSTRAINT FK_BX_CELLPROCESSEDCELLS;
ALTER TABLE APP.BX_CELLPROCESSEDCELLS DROP CONSTRAINT PK_BX_CELLPROCESSEDCELLS;
ALTER TABLE APP.BX_CELLQUEUEDCELLS DROP CONSTRAINT FK_BX_CELLQUEUEDCELLS;
ALTER TABLE APP.BX_CELLQUEUEDCELLS DROP CONSTRAINT PK_BX_CELLQUEUEDCELLS;
ALTER TABLE APP.BX_CELLSTATSEND DROP CONSTRAINT FK_BX_CELLSTATSEND;
ALTER TABLE APP.BX_CELLSTATSEND DROP CONSTRAINT PK_BX_CELLSTATSEND;
ALTER TABLE APP.BX_CELLTIMEINQUEUE DROP CONSTRAINT FK_BX_CELLTIMEINQUEUE;
ALTER TABLE APP.BX_CELLTIMEINQUEUE DROP CONSTRAINT PK_BX_CELLTIMEINQUEUE;
ALTER TABLE APP.BX_CONNBIDIRECT DROP CONSTRAINT FK_BX_CONNBIDIRECT;
ALTER TABLE APP.BX_CONNBIDIRECT DROP CONSTRAINT PK_BX_CONNBIDIRECT;
ALTER TABLE APP.BX_DIRREQREADHISTORY DROP CONSTRAINT FK_BX_DIRREQREADHISTORY;
ALTER TABLE APP.BX_DIRREQREADHISTORY DROP CONSTRAINT PK_BX_DIRREQREADHISTORY;
ALTER TABLE APP.BX_DIRREQREADHISTORY_BYTES DROP CONSTRAINT FK_BX_DIRREQREADHISTORY_BYTES;
ALTER TABLE APP.BX_DIRREQREADHISTORY_BYTES DROP CONSTRAINT PK_BX_DIRREQREADHISTORY_BYTES;
ALTER TABLE APP.BX_DIRREQSTATSEND DROP CONSTRAINT FK_BX_DIRREQSTATSEND;
ALTER TABLE APP.BX_DIRREQSTATSEND DROP CONSTRAINT PK_BX_DIRREQSTATSEND;
ALTER TABLE APP.BX_DIRREQV2DIRECTDL DROP CONSTRAINT FK_BX_DIRREQV2DIRECTDL;
ALTER TABLE APP.BX_DIRREQV2DIRECTDL DROP CONSTRAINT PK_BX_DIRREQV2DIRECTDL;
ALTER TABLE APP.BX_DIRREQV2IPS DROP CONSTRAINT FK_BX_DIRREQV2IPS;
ALTER TABLE APP.BX_DIRREQV2IPS DROP CONSTRAINT PK_BX_DIRREQV2IPS;
ALTER TABLE APP.BX_DIRREQV2REQS DROP CONSTRAINT FK_BX_DIRREQV2REQS;
ALTER TABLE APP.BX_DIRREQV2REQS DROP CONSTRAINT PK_BX_DIRREQV2REQS;
ALTER TABLE APP.BX_DIRREQV2RESP DROP CONSTRAINT FK_BX_DIRREQV2RESP;
ALTER TABLE APP.BX_DIRREQV2RESP DROP CONSTRAINT PK_BX_DIRREQV2RESP;
ALTER TABLE APP.BX_DIRREQV2TUNNELEDDL DROP CONSTRAINT FK_BX_DIRREQV2TUNNELEDDL;
ALTER TABLE APP.BX_DIRREQV2TUNNELEDDL DROP CONSTRAINT PK_BX_DIRREQV2TUNNELEDDL;
ALTER TABLE APP.BX_DIRREQV3DIRECTDL DROP CONSTRAINT FK_BX_DIRREQV3DIRECTDL;
ALTER TABLE APP.BX_DIRREQV3DIRECTDL DROP CONSTRAINT PK_BX_DIRREQV3DIRECTDL;
ALTER TABLE APP.BX_DIRREQV3IPS DROP CONSTRAINT FK_BX_DIRREQV3IPS;
ALTER TABLE APP.BX_DIRREQV3IPS DROP CONSTRAINT PK_BX_DIRREQV3IPS;
ALTER TABLE APP.BX_DIRREQV3REQS DROP CONSTRAINT FK_BX_DIRREQV3REQS;
ALTER TABLE APP.BX_DIRREQV3REQS DROP CONSTRAINT PK_BX_DIRREQV3REQS;
ALTER TABLE APP.BX_DIRREQV3RESP DROP CONSTRAINT FK_BX_DIRREQV3RESP;
ALTER TABLE APP.BX_DIRREQV3RESP DROP CONSTRAINT PK_BX_DIRREQV3RESP;
ALTER TABLE APP.BX_DIRREQV3TUNNELEDDL DROP CONSTRAINT FK_BX_DIRREQV3TUNNELEDDL;
ALTER TABLE APP.BX_DIRREQV3TUNNELEDDL DROP CONSTRAINT PK_BX_DIRREQV3TUNNELEDDL;
ALTER TABLE APP.BX_DIRREQWRITEHISTORY DROP CONSTRAINT FK_BX_DIRREQWRITEHISTORY;
ALTER TABLE APP.BX_DIRREQWRITEHISTORY DROP CONSTRAINT PK_BX_DIRREQWRITEHISTORY;
ALTER TABLE APP.BX_DIRREQWRITEHISTORY_BYTES DROP CONSTRAINT FK_BX_DIRREQWRITEHISTORY_BYTES;
ALTER TABLE APP.BX_DIRREQWRITEHISTORY_BYTES DROP CONSTRAINT PK_BX_DIRREQWRITEHISTORY_BYTES;
ALTER TABLE APP.BX_ENTRYIPS DROP CONSTRAINT FK_BX_ENTRYIPS;
ALTER TABLE APP.BX_ENTRYIPS DROP CONSTRAINT PK_BX_ENTRYIPS;
ALTER TABLE APP.BX_ENTRYSTATSEND DROP CONSTRAINT FK_BX_ENTRYSTATSEND;
ALTER TABLE APP.BX_ENTRYSTATSEND DROP CONSTRAINT PK_BX_ENTRYSTATSEND;
ALTER TABLE APP.BX_EXITKIBIBYTESREAD DROP CONSTRAINT FK_BX_EXITKIBIBYTESREAD;
ALTER TABLE APP.BX_EXITKIBIBYTESREAD DROP CONSTRAINT PK_BX_EXITKIBIBYTESREAD;
ALTER TABLE APP.BX_EXITKIBIBYTESWRITTEN DROP CONSTRAINT FK_BX_EXITKIBIBYTESWRITTEN;
ALTER TABLE APP.BX_EXITKIBIBYTESWRITTEN DROP CONSTRAINT PK_BX_EXITKIBIBYTESWRITTEN;
ALTER TABLE APP.BX_EXITSTATSEND DROP CONSTRAINT FK_BX_EXITSTATSEND;
ALTER TABLE APP.BX_EXITSTATSEND DROP CONSTRAINT PK_BX_EXITSTATSEND;
ALTER TABLE APP.BX_EXITSTREAMSOPENED DROP CONSTRAINT FK_BX_EXITSTREAMSOPENED;
ALTER TABLE APP.BX_EXITSTREAMSOPENED DROP CONSTRAINT PK_BX_EXITSTREAMSOPENED;
ALTER TABLE APP.BX_EXTRAINFO DROP CONSTRAINT FK_BX_EXTRAINFO;
ALTER TABLE APP.BX_EXTRAINFO DROP CONSTRAINT PK_BX_EXTRAINFO;
ALTER TABLE APP.BX_GEOIPCLIENTORIGINS DROP CONSTRAINT FK_BX_GEOIPCLIENTORIGINS;
ALTER TABLE APP.BX_GEOIPCLIENTORIGINS DROP CONSTRAINT PK_BX_GEOIPCLIENTORIGINS;
ALTER TABLE APP.BX_HIDSERVDIRONIONSSEEN DROP CONSTRAINT FK_BX_HIDSERVDIRONIONSSEEN;
ALTER TABLE APP.BX_HIDSERVDIRONIONSSEEN DROP CONSTRAINT PK_TABLE_0;
ALTER TABLE APP.BX_HIDSERVDIRONIONSSEEN_OBFUSCATION DROP CONSTRAINT FK_BX_HIDSERVDIRONIONSSEEN_OBFUSCATION;
ALTER TABLE APP.BX_HIDSERVDIRONIONSSEEN_OBFUSCATION DROP CONSTRAINT PK_BX_HIDSERVDIRONIONSSEEN_OBFUSCATION;
ALTER TABLE APP.BX_HIDSERVRENDRELAYEDCELLS DROP CONSTRAINT FK_BX_HIDSERVRENDRELAYEDCELLS;
ALTER TABLE APP.BX_HIDSERVRENDRELAYEDCELLS DROP CONSTRAINT PK_BX_HIDSERVRENDRELAYEDCELLS;
ALTER TABLE APP.BX_HIDSERVRENDRELAYEDCELLS_OBFUSCATION DROP CONSTRAINT FK_BX_HIDSERVRENDRELAYEDCELLS_OBFUSCATION;
ALTER TABLE APP.BX_HIDSERVRENDRELAYEDCELLS_OBFUSCATION DROP CONSTRAINT PK_BX_HIDSERVRENDRELAYEDCELLS_OBFUSCATION;
ALTER TABLE APP.BX_HIDSERVSTATSEND DROP CONSTRAINT FK_BX_HIDSERVSTATSEND;
ALTER TABLE APP.BX_HIDSERVSTATSEND DROP CONSTRAINT PK_BX_HIDSERVSTATSEND;
ALTER TABLE APP.BX_READHISTORY DROP CONSTRAINT FK_BX_READHISTORY;
ALTER TABLE APP.BX_READHISTORY DROP CONSTRAINT PK_BX_READHISTORY;
ALTER TABLE APP.BX_READHISTORY_BYTES DROP CONSTRAINT FK_BX_READHISTORY_BYTES;
ALTER TABLE APP.BX_READHISTORY_BYTES DROP CONSTRAINT PK_BX_READHISTORY_BYTES;
ALTER TABLE APP.BX_TRANSPORT DROP CONSTRAINT FK_BX_TRANSPORT;
ALTER TABLE APP.BX_TRANSPORT DROP CONSTRAINT PK_BX_TRANSPORT;
ALTER TABLE APP.BX_WRITEHISTORY DROP CONSTRAINT FK_BX_WRITEHISTORY;
ALTER TABLE APP.BX_WRITEHISTORY DROP CONSTRAINT PK_BX_WRITEHISTORY;
ALTER TABLE APP.BX_WRITEHISTORY_BYTES DROP CONSTRAINT FK_BX_WRITEHISTORY_BYTES;
ALTER TABLE APP.BX_WRITEHISTORY_BYTES DROP CONSTRAINT PK_BX_WRITEHISTORY_BYTES;
DROP TABLE APP.BX;
DROP TABLE APP.BX_BRIDGEIPS;
DROP TABLE APP.BX_BRIDGEIPTRANSPORTS;
DROP TABLE APP.BX_BRIDGEIPVERSIONS;
DROP TABLE APP.BX_BRIDGESTATSEND;
DROP TABLE APP.BX_CELLPROCESSEDCELLS;
DROP TABLE APP.BX_CELLQUEUEDCELLS;
DROP TABLE APP.BX_CELLSTATSEND;
DROP TABLE APP.BX_CELLTIMEINQUEUE;
DROP TABLE APP.BX_CONNBIDIRECT;
DROP TABLE APP.BX_DIRREQREADHISTORY;
DROP TABLE APP.BX_DIRREQREADHISTORY_BYTES;
DROP TABLE APP.BX_DIRREQSTATSEND;
DROP TABLE APP.BX_DIRREQV2DIRECTDL;
DROP TABLE APP.BX_DIRREQV2IPS;
DROP TABLE APP.BX_DIRREQV2REQS;
DROP TABLE APP.BX_DIRREQV2RESP;
DROP TABLE APP.BX_DIRREQV2TUNNELEDDL;
DROP TABLE APP.BX_DIRREQV3DIRECTDL;
DROP TABLE APP.BX_DIRREQV3IPS;
DROP TABLE APP.BX_DIRREQV3REQS;
DROP TABLE APP.BX_DIRREQV3RESP;
DROP TABLE APP.BX_DIRREQV3TUNNELEDDL;
DROP TABLE APP.BX_DIRREQWRITEHISTORY;
DROP TABLE APP.BX_DIRREQWRITEHISTORY_BYTES;
DROP TABLE APP.BX_ENTRYIPS;
DROP TABLE APP.BX_ENTRYSTATSEND;
DROP TABLE APP.BX_EXITKIBIBYTESREAD;
DROP TABLE APP.BX_EXITKIBIBYTESWRITTEN;
DROP TABLE APP.BX_EXITSTATSEND;
DROP TABLE APP.BX_EXITSTREAMSOPENED;
DROP TABLE APP.BX_EXTRAINFO;
DROP TABLE APP.BX_GEOIPCLIENTORIGINS;
DROP TABLE APP.BX_HIDSERVDIRONIONSSEEN;
DROP TABLE APP.BX_HIDSERVDIRONIONSSEEN_OBFUSCATION;
DROP TABLE APP.BX_HIDSERVRENDRELAYEDCELLS;
DROP TABLE APP.BX_HIDSERVRENDRELAYEDCELLS_OBFUSCATION;
DROP TABLE APP.BX_HIDSERVSTATSEND;
DROP TABLE APP.BX_READHISTORY;
DROP TABLE APP.BX_READHISTORY_BYTES;
DROP TABLE APP.BX_TRANSPORT;
DROP TABLE APP.BX_WRITEHISTORY;
DROP TABLE APP.BX_WRITEHISTORY_BYTES;





CREATE TABLE BX (
  id                   integer  NOT NULL  generated always as identity,
  src_date             bigint    ,
  type                 varchar(64)   DEFAULT 'bridge-extra-info 1.3' ,
  identity_ed25519     char(7)    ,
  published            bigint    ,
  geoip_db_digest      varchar(40)    ,
  geoip6_db_digest     varchar(40)    ,
  geoip_start_time     bigint    ,
  dirreq_v2_share      double    ,
  dirreq_v3_share      double    ,
  cell_circuits_per_decile integer    ,
  extra_info_digest    varchar(40)    ,
  extra_info_digest_sha256 varchar(43)    ,
  master_key_ed25519   varchar(43)    ,
  CONSTRAINT Pk_be PRIMARY KEY ( id ),
  CONSTRAINT Idx_BX UNIQUE ( published, extra_info_digest )
);
CREATE INDEX Idx_BX_src_date ON BX ( src_date );


CREATE TABLE BX_BridgeIpTransports (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  protocol             varchar(16)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_BridgeIpTransports PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_BridgeIpTransports FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_BridgeIpVersions (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  version              varchar(16)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_BridgeIpVersions PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_BridgeIpVersions FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_BridgeIps (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_BridgeIps PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_BridgeIps FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_BridgeStatsEnd (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_BX_BridgeStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_BridgeStatsEnd FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_CellProcessedCells (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  mean                 integer    ,
  CONSTRAINT Pk_BX_CellProcessedCells PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_CellProcessedCells FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_CellQueuedCells (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  mean                 double    ,
  CONSTRAINT Pk_BX_CellQueuedCells PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_CellQueuedCells FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_CellStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_BX_CellStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_CellStatsEnd FOREIGN KEY ( id ) REFERENCES BX( id )
);


CREATE TABLE BX_CellTimeInQueue (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  mean                 integer    ,
  CONSTRAINT Pk_BX_CellTimeInQueue PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_CellTimeInQueue FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_ConnBiDirect (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  below                integer    ,
  "read"               integer    ,
  "write"              integer    ,
  "both"               integer    ,
  CONSTRAINT Pk_BX_ConnBiDirect PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_ConnBiDirect FOREIGN KEY ( id ) REFERENCES BX( id )
);


CREATE TABLE BX_DirreqReadHistory (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_BX_DirreqReadHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqReadHistory FOREIGN KEY ( id ) REFERENCES BX( id )
);


CREATE TABLE BX_DirreqReadHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  BX_DirreqRead_id     integer  NOT NULL  ,
  bytes                bigint  NOT NULL  ,
  CONSTRAINT Pk_BX_DirreqReadHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqReadHistory_Bytes FOREIGN KEY ( BX_DirreqRead_id ) REFERENCES BX_DirreqReadHistory( id )
);


CREATE TABLE BX_DirreqStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_BX_DirreqStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqStatsEnd FOREIGN KEY ( id ) REFERENCES BX( id )
);


CREATE TABLE BX_DirreqV2DirectDl (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  "key"                varchar(32)    ,
  value                integer    ,
  CONSTRAINT Pk_BX_DirreqV2DirectDl PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV2DirectDl FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_DirreqV2Ips (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_DirreqV2Ips PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV2Ips FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_DirreqV2Reqs (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_DirreqV2Reqs PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV2Reqs FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_DirreqV2Resp (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  response             varchar(256)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_DirreqV2Resp PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV2Resp FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_DirreqV2TunneledDl (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  "key"                varchar(32)    ,
  value                integer    ,
  CONSTRAINT Pk_BX_DirreqV2TunneledDl PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV2TunneledDl FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_DirreqV3DirectDl (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  "key"                varchar(32)    ,
  value                integer    ,
  CONSTRAINT Pk_BX_DirreqV3DirectDl PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV3DirectDl FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_DirreqV3Ips (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_DirreqV3Ips PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV3Ips FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_DirreqV3Reqs (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_DirreqV3Reqs PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV3Reqs FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_DirreqV3Resp (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  response             varchar(256)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_DirreqV3Resp PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV3Resp FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_DirreqV3TunneledDl (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  "key"                varchar(32)    ,
  value                integer    ,
  CONSTRAINT Pk_BX_DirreqV3TunneledDl PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqV3TunneledDl FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_DirreqWriteHistory (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_BX_DirreqWriteHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqWriteHistory FOREIGN KEY ( id ) REFERENCES BX( id )
);


CREATE TABLE BX_DirreqWriteHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  BX_DirreqWrite_id    integer  NOT NULL  ,
  bytes                bigint  NOT NULL  ,
  CONSTRAINT Pk_BX_DirreqWriteHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_DirreqWriteHistory_Bytes FOREIGN KEY ( BX_DirreqWrite_id ) REFERENCES BX_DirreqWriteHistory( id )
);


CREATE TABLE BX_EntryIps (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_EntryIps PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_EntryIps FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_EntryStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_BX_EntryStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_EntryStatsEnd FOREIGN KEY ( id ) REFERENCES BX( id )
);


CREATE TABLE BX_ExitKibibytesRead (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  port                 varchar(5)    ,
  kib                  bigint    ,
  CONSTRAINT Pk_BX_ExitKibibytesRead PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_ExitKibibytesRead FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_ExitKibibytesWritten (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  port                 varchar(5)    ,
  kib                  bigint    ,
  CONSTRAINT Pk_BX_ExitKibibytesWritten PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_ExitKibibytesWritten FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_ExitStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_BX_ExitStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_ExitStatsEnd FOREIGN KEY ( id ) REFERENCES BX( id )
);


CREATE TABLE BX_ExitStreamsOpened (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  port                 varchar(5)    ,
  count                bigint    ,
  CONSTRAINT Pk_BX_ExitStreamsOpened PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_ExitStreamsOpened FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_ExtraInfo (
  id                   integer  NOT NULL  ,
  nickname             varchar(19)    ,
  fingerprint          varchar(40)    ,
  CONSTRAINT Pk_BX_ExtraInfo PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_ExtraInfo FOREIGN KEY ( id ) REFERENCES BX( id )
);


CREATE TABLE BX_GeoIpClientOrigins (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_BX_GeoIpClientOrigins PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_GeoIpClientOrigins FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_HidservDirOnionsSeen (
  id                   integer  NOT NULL  ,
  onions               double    ,
  CONSTRAINT Pk_Table_0 PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_HidservDirOnionsSeen FOREIGN KEY ( id ) REFERENCES BX( id )
);


CREATE TABLE BX_HidservDirOnionsSeen_Obfuscation (
  id                   integer  NOT NULL  generated always as identity,
  BX_Onion_id          integer  NOT NULL  ,
  obfuscation          varchar(32)    ,
  value                double    ,
  CONSTRAINT Pk_BX_HidservDirOnionsSeen_Obfuscation PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_HidservDirOnionsSeen_Obfuscation FOREIGN KEY ( BX_Onion_id ) REFERENCES BX_HidservDirOnionsSeen( id )
);


CREATE TABLE BX_HidservRendRelayedCells (
  id                   integer  NOT NULL  ,
  cells                double    ,
  CONSTRAINT Pk_BX_HidservRendRelayedCells PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_HidservRendRelayedCells FOREIGN KEY ( id ) REFERENCES BX( id )
);


CREATE TABLE BX_HidservRendRelayedCells_Obfuscation (
  id                   integer  NOT NULL  generated always as identity,
  BX_Relay_id          integer  NOT NULL  ,
  obfuscation          varchar(32)    ,
  value                double    ,
  CONSTRAINT Pk_BX_HidservRendRelayedCells_Obfuscation PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_HidservRendRelayedCells_Obfuscation FOREIGN KEY ( BX_Relay_id ) REFERENCES BX_HidservRendRelayedCells( id )
);


CREATE TABLE BX_HidservStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_BX_HidservStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_HidservStatsEnd FOREIGN KEY ( id ) REFERENCES BX( id )
);


CREATE TABLE BX_ReadHistory (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_BX_ReadHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_ReadHistory FOREIGN KEY ( id ) REFERENCES BX( id )
);


CREATE TABLE BX_ReadHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  BX_Read_id           integer  NOT NULL  ,
  bytes                bigint    ,
  CONSTRAINT Pk_BX_ReadHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_ReadHistory_Bytes FOREIGN KEY ( BX_Read_id ) REFERENCES BX_ReadHistory( id )
);


CREATE TABLE BX_Transport (
  id                   integer  NOT NULL  generated always as identity,
  BX_id                integer  NOT NULL  ,
  transport            varchar(32)    ,
  CONSTRAINT Pk_BX_Transport PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_Transport FOREIGN KEY ( BX_id ) REFERENCES BX( id )
);


CREATE TABLE BX_WriteHistory (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_BX_WriteHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_WriteHistory FOREIGN KEY ( id ) REFERENCES BX( id )
);


CREATE TABLE BX_WriteHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  BX_Write_id          integer  NOT NULL  ,
  bytes                bigint    ,
  CONSTRAINT Pk_BX_WriteHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_BX_WriteHistory_Bytes FOREIGN KEY ( BX_Write_id ) REFERENCES BX_WriteHistory( id )
);

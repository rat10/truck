ALTER TABLE APP.RC DROP CONSTRAINT IDX_RC;
ALTER TABLE APP.RC DROP CONSTRAINT PK_RC;
ALTER TABLE APP.RC_AUTHORITIES DROP CONSTRAINT FK_RC_AUTHORITIES;
ALTER TABLE APP.RC_AUTHORITIES DROP CONSTRAINT PK_RC_AUTHORITIES;
ALTER TABLE APP.RC_AUTHORITY_DIRSOURCE DROP CONSTRAINT FK_RC_AUTHORITY_DIRSOURCE;
ALTER TABLE APP.RC_AUTHORITY_DIRSOURCE DROP CONSTRAINT PK_RC_AUTHORITY_DIRSOURCE;
ALTER TABLE APP.RC_CLIENTVERSIONS DROP CONSTRAINT FK_RC_CLIENTVERSIONS;
ALTER TABLE APP.RC_CLIENTVERSIONS DROP CONSTRAINT PK_RC_CLIENTVERSIONS;
ALTER TABLE APP.RC_DIRFOOTER DROP CONSTRAINT FK_RC_DIRFOOTER;
ALTER TABLE APP.RC_DIRFOOTER DROP CONSTRAINT PK_RC_DIRFOOTER;
ALTER TABLE APP.RC_DIRFOOTER_BANDWIDTHWEIGHTS DROP CONSTRAINT FK_RC_DIRFOOTER_BANDWIDTHWEIGHTS;
ALTER TABLE APP.RC_DIRFOOTER_BANDWIDTHWEIGHTS DROP CONSTRAINT PK_RC_DIRFOOTER_BANDWIDTHWEIGHTS;
ALTER TABLE APP.RC_DIRFOOTER_DIRSIG DROP CONSTRAINT FK_RC_DIRFOOTER_DIRSIG;
ALTER TABLE APP.RC_DIRFOOTER_DIRSIG DROP CONSTRAINT PK_RC_DIRFOOTER_DIRSIG;
ALTER TABLE APP.RC_KNOWNFLAGS DROP CONSTRAINT FK_RC_KNOWNFLAGS;
ALTER TABLE APP.RC_KNOWNFLAGS DROP CONSTRAINT PK_RC_KNOWNFLAGS;
ALTER TABLE APP.RC_PACKAGE DROP CONSTRAINT FK_RC_PACKAGE;
ALTER TABLE APP.RC_PACKAGE DROP CONSTRAINT PK_RC_PACKAGE;
ALTER TABLE APP.RC_PARAMS DROP CONSTRAINT FK_RC_PARAMS;
ALTER TABLE APP.RC_PARAMS DROP CONSTRAINT PK_RC_PARAMS;
ALTER TABLE APP.RC_SERVERVERSIONS DROP CONSTRAINT FK_RC_SERVERVERSIONS;
ALTER TABLE APP.RC_SERVERVERSIONS DROP CONSTRAINT PK_RC_SERVERVERSIONS;
ALTER TABLE APP.RC_STATUS DROP CONSTRAINT FK_RC_STATUS;
ALTER TABLE APP.RC_STATUS DROP CONSTRAINT PK_RC_STATUS;
ALTER TABLE APP.RC_STATUS_A DROP CONSTRAINT FK_RC_STATUS_A;
ALTER TABLE APP.RC_STATUS_A DROP CONSTRAINT PK_RC_STATUS_A;
ALTER TABLE APP.RC_STATUS_P DROP CONSTRAINT FK_RC_STATUS_P;
ALTER TABLE APP.RC_STATUS_P DROP CONSTRAINT PK_RC_STATUS_P;
ALTER TABLE APP.RC_STATUS_R DROP CONSTRAINT FK_RC_STATUS_R;
ALTER TABLE APP.RC_STATUS_R DROP CONSTRAINT PK_RC_STATUS_R;
ALTER TABLE APP.RC_STATUS_S DROP CONSTRAINT FK_RC_STATUS_S;
ALTER TABLE APP.RC_STATUS_S DROP CONSTRAINT PK_RC_STATUS_S;
ALTER TABLE APP.RC_STATUS_W DROP CONSTRAINT FK_RC_STATUS_W;
ALTER TABLE APP.RC_STATUS_W DROP CONSTRAINT PK_RC_STATUS_W;
ALTER TABLE APP.RC_VOTINGDELAY DROP CONSTRAINT FK_RC_VOTINGDELAY;
ALTER TABLE APP.RC_VOTINGDELAY DROP CONSTRAINT PK_RC_VOTINGDELAY;
DROP TABLE APP.RC;
DROP TABLE APP.RC_AUTHORITIES;
DROP TABLE APP.RC_AUTHORITY_DIRSOURCE;
DROP TABLE APP.RC_CLIENTVERSIONS;
DROP TABLE APP.RC_DIRFOOTER;
DROP TABLE APP.RC_DIRFOOTER_BANDWIDTHWEIGHTS;
DROP TABLE APP.RC_DIRFOOTER_DIRSIG;
DROP TABLE APP.RC_KNOWNFLAGS;
DROP TABLE APP.RC_PACKAGE;
DROP TABLE APP.RC_PARAMS;
DROP TABLE APP.RC_SERVERVERSIONS;
DROP TABLE APP.RC_STATUS;
DROP TABLE APP.RC_STATUS_A;
DROP TABLE APP.RC_STATUS_P;
DROP TABLE APP.RC_STATUS_R;
DROP TABLE APP.RC_STATUS_S;
DROP TABLE APP.RC_STATUS_W;
DROP TABLE APP.RC_VOTINGDELAY;


CREATE TABLE RC (
  id                   integer  NOT NULL  generated always as identity,
  src_date             bigint    ,
  type                 varchar(64)   DEFAULT 'network-status-consensus-3 1.0' ,
  network_status_version smallint   DEFAULT 3 ,
  vote_status          varchar(16)   DEFAULT 'consensus' ,
  consensus_method     smallint    ,
  consensus_flavor     varchar(32)    ,
  valid_after          bigint    ,
  fresh_until          bigint    ,
  valid_until          bigint    ,
  contact              varchar(256)    ,
  legacy_dir_key       varchar(64)    ,
  signing_key_digest   varchar(40)    ,
  CONSTRAINT Pk_rc PRIMARY KEY ( id ),
  CONSTRAINT Idx_RC UNIQUE ( valid_after )
);
CREATE INDEX Idx_RC_src_date ON RC ( src_date );


CREATE TABLE RC_Authorities (
  id                   integer  NOT NULL  generated always as identity,
  RC_id                integer  NOT NULL  ,
  authority            varchar(40)    ,
  contact              varchar(256)    ,
  vote_digest          varchar(40)    ,
  CONSTRAINT Pk_RC_Authorities PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_Authorities FOREIGN KEY ( RC_id ) REFERENCES RC( id )
);


CREATE TABLE RC_Authority_DirSource (
  id                   integer  NOT NULL  ,
  nickname             varchar(19)    ,
  "identity"           varchar(40)    ,
  hostname             varchar(64)    ,
  address              varchar(15)    ,
  dir_port             integer    ,
  or_port              integer    ,
  is_legacy            char(7)    ,
  CONSTRAINT Pk_RC_Authority_DirSource PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_Authority_DirSource FOREIGN KEY ( id ) REFERENCES RC_Authorities( id )
);


CREATE TABLE RC_ClientVersions (
  id                   integer  NOT NULL  generated always as identity,
  RC_id                integer  NOT NULL  ,
  version              varchar(32)    ,
  CONSTRAINT Pk_RC_ClientVersions PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_ClientVersions FOREIGN KEY ( RC_id ) REFERENCES RC( id )
);


CREATE TABLE RC_DirFooter (
  id                   integer  NOT NULL  ,
  consensus_digest     varchar(40)    ,
  CONSTRAINT Pk_RC_DirFooter PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_DirFooter FOREIGN KEY ( id ) REFERENCES RC( id )
);


CREATE TABLE RC_DirFooter_BandwidthWeights (
  id                   integer  NOT NULL  generated always as identity,
  RC_DirFooter_Id      integer  NOT NULL  ,
  weight               varchar(32)    ,
  value                integer    ,
  CONSTRAINT Pk_RC_DirFooter_BandwidthWeights PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_DirFooter_BandwidthWeights FOREIGN KEY ( RC_DirFooter_Id ) REFERENCES RC_DirFooter( id )
);


CREATE TABLE RC_DirFooter_DirSig (
  id                   integer  NOT NULL  generated always as identity,
  RC_DirFooter_Id      integer  NOT NULL  ,
  algorithm            varchar(16)    ,
  "identity"           varchar(40)    ,
  signing_key_digest   varchar(40)    ,
  signature            char(7)    ,
  CONSTRAINT Pk_RC_DirFooter_DirSig PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_DirFooter_DirSig FOREIGN KEY ( RC_DirFooter_Id ) REFERENCES RC_DirFooter( id )
);


CREATE TABLE RC_KnownFlags (
  id                   integer  NOT NULL  generated always as identity,
  RC_id                integer  NOT NULL  ,
  flag                 varchar(32)    ,
  CONSTRAINT Pk_RC_KnownFlags PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_KnownFlags FOREIGN KEY ( RC_id ) REFERENCES RC( id )
);


CREATE TABLE RC_Params (
  id                   integer  NOT NULL  generated always as identity,
  RC_id                integer  NOT NULL  ,
  param                varchar(32)    ,
  value                smallint    ,
  CONSTRAINT Pk_RC_Params PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_Params FOREIGN KEY ( RC_id ) REFERENCES RC( id )
);


CREATE TABLE RC_ServerVersions (
  id                   integer  NOT NULL  generated always as identity,
  RC_id                integer  NOT NULL  ,
  version              varchar(32)    ,
  CONSTRAINT Pk_RC_ServerVersions PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_ServerVersions FOREIGN KEY ( RC_id ) REFERENCES RC( id )
);


CREATE TABLE RC_Status (
  id                   integer  NOT NULL  generated always as identity,
  RC_id                integer  NOT NULL  ,
  server               varchar(40)    ,
  v                    varchar(32)    ,
  CONSTRAINT Pk_RC_Status PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_Status FOREIGN KEY ( RC_id ) REFERENCES RC( id )
);


CREATE TABLE RC_Status_A (
  id                   integer  NOT NULL  generated always as identity,
  RC_Status_Id         integer  NOT NULL  ,
  address              varchar(256)    ,
  port                 integer    ,
  CONSTRAINT Pk_RC_Status_A PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_Status_A FOREIGN KEY ( RC_Status_Id ) REFERENCES RC_Status( id )
);


CREATE TABLE RC_Status_P (
  id                   integer  NOT NULL  ,
  default_policy       varchar(24)    ,
  port_summary         varchar(4096)    ,
  CONSTRAINT Pk_RC_Status_P PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_Status_P FOREIGN KEY ( id ) REFERENCES RC_Status( id )
);


CREATE TABLE RC_Status_R (
  id                   integer  NOT NULL  ,
  nickname             varchar(19)    ,
  "identity"           varchar(40)    ,
  digest               varchar(40)    ,
  publication          bigint    ,
  ip                   varchar(15)    ,
  or_port              integer    ,
  dir_port             integer    ,
  CONSTRAINT Pk_RC_Status_R PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_Status_R FOREIGN KEY ( id ) REFERENCES RC_Status( id )
);


CREATE TABLE RC_Status_S (
  id                   integer  NOT NULL  generated always as identity,
  RC_Status_id         integer  NOT NULL  ,
  flag                 varchar(32)    ,
  CONSTRAINT Pk_RC_Status_S PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_Status_S FOREIGN KEY ( RC_Status_id ) REFERENCES RC_Status( id )
);


CREATE TABLE RC_Status_W (
  id                   integer  NOT NULL  ,
  bandwidth            smallint    ,
  measured             smallint    ,
  unmeasured           char(7)    ,
  CONSTRAINT Pk_RC_Status_W PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_Status_W FOREIGN KEY ( id ) REFERENCES RC_Status( id )
);


CREATE TABLE RC_VotingDelay (
  id                   integer  NOT NULL  ,
  vote_seconds         bigint    ,
  dist_seconds         bigint    ,
  CONSTRAINT Pk_RC_VotingDelay PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_VotingDelay FOREIGN KEY ( id ) REFERENCES RC( id )
);


CREATE TABLE RC_package (
  id                   integer  NOT NULL  generated always as identity,
  RC_id                integer  NOT NULL  ,
  version              varchar(32)    ,
  CONSTRAINT Pk_RC_package PRIMARY KEY ( id ),
  CONSTRAINT fk_RC_package FOREIGN KEY ( RC_id ) REFERENCES RC( id )
);

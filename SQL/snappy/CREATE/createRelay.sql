ALTER TABLE APP.R DROP CONSTRAINT IDX_R;
ALTER TABLE APP.R DROP CONSTRAINT PK_R;
ALTER TABLE APP.R_BANDWIDTH DROP CONSTRAINT FK_R_BANDWIDTH;
ALTER TABLE APP.R_BANDWIDTH DROP CONSTRAINT PK_R_BANDWIDTH;
ALTER TABLE APP.R_CIRCUITPROTOCOLVERSIONS DROP CONSTRAINT FK_R_CIRCUITPROTOCOLVERSIONS;
ALTER TABLE APP.R_CIRCUITPROTOCOLVERSIONS DROP CONSTRAINT PK_R_CIRCUITPROTOCOLVERSIONS;
ALTER TABLE APP.R_EXITPOLICY DROP CONSTRAINT FK_R_EXITPOLICY;
ALTER TABLE APP.R_EXITPOLICY DROP CONSTRAINT PK_R_EXITPOLICY;
ALTER TABLE APP.R_FAMILY DROP CONSTRAINT FK_R_FAMILY;
ALTER TABLE APP.R_FAMILY DROP CONSTRAINT PK_R_FAMILY;
ALTER TABLE APP.R_HIDDENSERVICEDIR DROP CONSTRAINT FK_R_HIDDENSERVICEDIR;
ALTER TABLE APP.R_HIDDENSERVICEDIR DROP CONSTRAINT PK_R_HIDDENSERVICEDIR;
ALTER TABLE APP.R_LINKPROTOCOLVERSIONS DROP CONSTRAINT FK_R_LINKPROTOCOLVERSIONS;
ALTER TABLE APP.R_LINKPROTOCOLVERSIONS DROP CONSTRAINT PK_R_LINKPROTOCOLVERSIONS;
ALTER TABLE APP.R_ORADRESS DROP CONSTRAINT FK_R_ORADRESS;
ALTER TABLE APP.R_ORADRESS DROP CONSTRAINT PK_R_ORADRESS;
ALTER TABLE APP.R_READHISTORY DROP CONSTRAINT FK_R_READHISTORY;
ALTER TABLE APP.R_READHISTORY DROP CONSTRAINT PK_R_READHISTORY;
ALTER TABLE APP.R_READHISTORY_BYTES DROP CONSTRAINT FK_R_READHISTORY_BYTES;
ALTER TABLE APP.R_READHISTORY_BYTES DROP CONSTRAINT PK_R_READHISTORY_BYTES;
ALTER TABLE APP.R_ROUTER DROP CONSTRAINT FK_R_ROUTER;
ALTER TABLE APP.R_ROUTER DROP CONSTRAINT PK_R_ROUTER;
ALTER TABLE APP.R_WRITEHISTORY DROP CONSTRAINT FK_R_WRITEHISTORY;
ALTER TABLE APP.R_WRITEHISTORY DROP CONSTRAINT PK_R_WRITEHISTORY;
ALTER TABLE APP.R_WRITEHISTORY_BYTES DROP CONSTRAINT FK_R_WRITEHISTORY_BYTES;
ALTER TABLE APP.R_WRITEHISTORY_BYTES DROP CONSTRAINT PK_R_WRITEHISTORY_BYTES;
DROP TABLE APP.R;
DROP TABLE APP.R_BANDWIDTH;
DROP TABLE APP.R_CIRCUITPROTOCOLVERSIONS;
DROP TABLE APP.R_EXITPOLICY;
DROP TABLE APP.R_FAMILY;
DROP TABLE APP.R_HIDDENSERVICEDIR;
DROP TABLE APP.R_LINKPROTOCOLVERSIONS;
DROP TABLE APP.R_ORADRESS;
DROP TABLE APP.R_READHISTORY;
DROP TABLE APP.R_READHISTORY_BYTES;
DROP TABLE APP.R_ROUTER;
DROP TABLE APP.R_WRITEHISTORY;
DROP TABLE APP.R_WRITEHISTORY_BYTES;




CREATE TABLE R (
  id                   integer  NOT NULL  generated always as identity,
  src_date             bigint    ,
  type                 varchar(64)   DEFAULT 'server-descriptor 1.0' ,
  digest               varchar(40)    ,
  sha256               varchar(43)    ,
  identity_ed25519     char(7)    ,
  master_key_ed25519   varchar(43)    ,
  platform             varchar(256)    ,
  published            bigint    ,
  fingerprint          varchar(40)    ,
  hibernating          char(7)    ,
  uptime               bigint    ,
  onion_key            char(7)    ,
  onion_key_crosscert  char(7)    ,
  ntor_onion_key       char(7)    ,
  ntor_onion_key_crosscert char(7)    ,
  signing_key          char(7)    ,
  ipv6_policy          varchar(256)    ,
  ipv6_portlist        varchar(256)    ,
  router_sig_ed25519   char(7)    ,
  router_signature     char(7)    ,
  contact              varchar(256)    ,
  eventdns             char(7)    ,
  caches_extra_info    char(7)    ,
  extra_info_digest    varchar(40)    ,
  extra_info_digest_sha256 varchar(43)    ,
  allow_single_hop_exits char(7)    ,
  tunneled_dir_server  char(7)    ,
  CONSTRAINT Pk_r PRIMARY KEY ( id ),
  CONSTRAINT Idx_R UNIQUE ( published, fingerprint )
);
CREATE INDEX Idx_R_src_date ON R ( src_date );


CREATE TABLE R_Bandwidth (
  id                   integer  NOT NULL  ,
  "avg"                integer    ,
  burst                integer    ,
  observed             integer    ,
  CONSTRAINT Pk_r_Bandwidth PRIMARY KEY ( id ),
  CONSTRAINT fk_r_Bandwidth FOREIGN KEY ( id ) REFERENCES R( id )
);


CREATE TABLE R_CircuitProtocolVersions (
  id                   integer  NOT NULL  generated always as identity,
  R_id                 integer  NOT NULL  ,
  circuit_protocol_versions smallint  NOT NULL  ,
  CONSTRAINT Pk_r_CircuitProtocolVersions PRIMARY KEY ( id ),
  CONSTRAINT fk_r_CircuitProtocolVersions FOREIGN KEY ( R_id ) REFERENCES R( id )
);


CREATE TABLE R_ExitPolicy (
  id                   integer  NOT NULL  generated always as identity,
  R_id                 integer  NOT NULL  ,
  exit_policy          varchar(256)  NOT NULL  ,
  CONSTRAINT Pk_r_ExitPolicy PRIMARY KEY ( id ),
  CONSTRAINT fk_r_ExitPolicy FOREIGN KEY ( R_id ) REFERENCES R( id )
);


CREATE TABLE R_Family (
  id                   integer  NOT NULL  generated always as identity,
  R_id                 integer  NOT NULL  ,
  family               varchar(256)  NOT NULL  ,
  CONSTRAINT Pk_r_Family PRIMARY KEY ( id ),
  CONSTRAINT fk_r_Family FOREIGN KEY ( R_id ) REFERENCES R( id )
);


CREATE TABLE R_HiddenserviceDir (
  id                   integer  NOT NULL  generated always as identity,
  R_id                 integer  NOT NULL  ,
  hidden_service_dir   smallint  NOT NULL  ,
  CONSTRAINT Pk_r_HiddenserviceDir PRIMARY KEY ( id ),
  CONSTRAINT fk_r_HiddenserviceDir FOREIGN KEY ( R_id ) REFERENCES R( id )
);


CREATE TABLE R_LinkProtocolVersions (
  id                   integer  NOT NULL  generated always as identity,
  R_id                 integer  NOT NULL  ,
  link_protocol_versions smallint  NOT NULL  ,
  CONSTRAINT Pk_r_LinkProtocolVersions PRIMARY KEY ( id ),
  CONSTRAINT fk_r_LinkProtocolVersions FOREIGN KEY ( R_id ) REFERENCES R( id )
);


CREATE TABLE R_OrAdress (
  id                   integer  NOT NULL  generated always as identity,
  R_id                 integer  NOT NULL  ,
  adress               varchar(256)  NOT NULL  ,
  port                 smallint  NOT NULL  ,
  CONSTRAINT Pk_r_OrAdress PRIMARY KEY ( id ),
  CONSTRAINT fk_r_OrAdress FOREIGN KEY ( R_id ) REFERENCES R( id )
);


CREATE TABLE R_ReadHistory (
  id                   integer  NOT NULL  ,
  date                 bigint  NOT NULL  ,
  interval             bigint  NOT NULL  ,
  CONSTRAINT Pk_r_ReadHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_r_ReadHistory FOREIGN KEY ( id ) REFERENCES R( id )
);


CREATE TABLE R_ReadHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  R_Read_id            integer  NOT NULL  ,
  bytes                bigint  NOT NULL  ,
  CONSTRAINT Pk_r_ReadHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_r_ReadHistory_Bytes FOREIGN KEY ( R_Read_id ) REFERENCES R_ReadHistory( id )
);


CREATE TABLE R_Router (
  id                   integer  NOT NULL  ,
  nickname             varchar(19)    ,
  address              varchar(256)    ,
  or_port              integer    ,
  socks_port           integer    ,
  dir_port             integer    ,
  CONSTRAINT Pk_r_Router PRIMARY KEY ( id ),
  CONSTRAINT fk_r_Router FOREIGN KEY ( id ) REFERENCES R( id )
);


CREATE TABLE R_WriteHistory (
  id                   integer  NOT NULL  ,
  date                 bigint  NOT NULL  ,
  interval             bigint  NOT NULL  ,
  CONSTRAINT Pk_r_WriteHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_r_WriteHistory FOREIGN KEY ( id ) REFERENCES R( id )
);


CREATE TABLE R_WriteHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  R_Write_id           integer  NOT NULL  ,
  bytes                bigint  NOT NULL  ,
  CONSTRAINT Pk_r_WriteHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_r_WriteHistory_Bytes FOREIGN KEY ( R_Write_id ) REFERENCES R_WriteHistory( id )
);

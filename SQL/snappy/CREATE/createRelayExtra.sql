ALTER TABLE APP.RX DROP CONSTRAINT IDX_RX;
ALTER TABLE APP.RX DROP CONSTRAINT PK_BE_0;
ALTER TABLE APP.RX_CELLPROCESSEDCELLS DROP CONSTRAINT FK_RX_CELLPROCESSEDCELLS;
ALTER TABLE APP.RX_CELLPROCESSEDCELLS DROP CONSTRAINT PK_RX_CELLPROCESSEDCELLS;
ALTER TABLE APP.RX_CELLQUEUEDCELLS DROP CONSTRAINT FK_RX_CELLQUEUEDCELLS;
ALTER TABLE APP.RX_CELLQUEUEDCELLS DROP CONSTRAINT PK_RX_CELLQUEUEDCELLS;
ALTER TABLE APP.RX_CELLSTATSEND DROP CONSTRAINT FK_RX_CELLSTATSEND;
ALTER TABLE APP.RX_CELLSTATSEND DROP CONSTRAINT PK_RX_CELLSTATSEND;
ALTER TABLE APP.RX_CELLTIMEINQUEUE DROP CONSTRAINT FK_RX_CELLTIMEINQUEUE;
ALTER TABLE APP.RX_CELLTIMEINQUEUE DROP CONSTRAINT PK_RX_CELLTIMEINQUEUE;
ALTER TABLE APP.RX_CONNBIDIRECT DROP CONSTRAINT FK_RX_CONNBIDIRECT;
ALTER TABLE APP.RX_CONNBIDIRECT DROP CONSTRAINT PK_RX_CONNBIDIRECT;
ALTER TABLE APP.RX_DIRREQREADHISTORY DROP CONSTRAINT FK_RX_DIRREQREADHISTORY;
ALTER TABLE APP.RX_DIRREQREADHISTORY DROP CONSTRAINT PK_RX_DIRREQREADHISTORY;
ALTER TABLE APP.RX_DIRREQREADHISTORY_BYTES DROP CONSTRAINT FK_RX_DIRREQREADHISTORY_BYTES;
ALTER TABLE APP.RX_DIRREQREADHISTORY_BYTES DROP CONSTRAINT PK_RX_DIRREQREADHISTORY_BYTES;
ALTER TABLE APP.RX_DIRREQSTATSEND DROP CONSTRAINT FK_RX_DIRREQSTATSEND;
ALTER TABLE APP.RX_DIRREQSTATSEND DROP CONSTRAINT PK_RX_DIRREQSTATSEND;
ALTER TABLE APP.RX_DIRREQV2DIRECTDL DROP CONSTRAINT FK_RX_DIRREQV2DIRECTDL;
ALTER TABLE APP.RX_DIRREQV2DIRECTDL DROP CONSTRAINT PK_RX_DIRREQV2DIRECTDL;
ALTER TABLE APP.RX_DIRREQV2IPS DROP CONSTRAINT FK_RX_DIRREQV2IPS;
ALTER TABLE APP.RX_DIRREQV2IPS DROP CONSTRAINT PK_RX_DIRREQV2IPS;
ALTER TABLE APP.RX_DIRREQV2REQS DROP CONSTRAINT FK_RX_DIRREQV2REQS;
ALTER TABLE APP.RX_DIRREQV2REQS DROP CONSTRAINT PK_RX_DIRREQV2REQS;
ALTER TABLE APP.RX_DIRREQV2RESP DROP CONSTRAINT FK_RX_DIRREQV2RESP;
ALTER TABLE APP.RX_DIRREQV2RESP DROP CONSTRAINT PK_RX_DIRREQV2RESP;
ALTER TABLE APP.RX_DIRREQV2TUNNELEDDL DROP CONSTRAINT FK_RX_DIRREQV2TUNNELEDDL;
ALTER TABLE APP.RX_DIRREQV2TUNNELEDDL DROP CONSTRAINT PK_RX_DIRREQV2TUNNELEDDL;
ALTER TABLE APP.RX_DIRREQV3DIRECTDL DROP CONSTRAINT FK_RX_DIRREQV3DIRECTDL;
ALTER TABLE APP.RX_DIRREQV3DIRECTDL DROP CONSTRAINT PK_RX_DIRREQV3DIRECTDL;
ALTER TABLE APP.RX_DIRREQV3IPS DROP CONSTRAINT FK_RX_DIRREQV3IPS;
ALTER TABLE APP.RX_DIRREQV3IPS DROP CONSTRAINT PK_RX_DIRREQV3IPS;
ALTER TABLE APP.RX_DIRREQV3REQS DROP CONSTRAINT FK_RX_DIRREQV3REQS;
ALTER TABLE APP.RX_DIRREQV3REQS DROP CONSTRAINT PK_RX_DIRREQV3REQS;
ALTER TABLE APP.RX_DIRREQV3RESP DROP CONSTRAINT FK_RX_DIRREQV3RESP;
ALTER TABLE APP.RX_DIRREQV3RESP DROP CONSTRAINT PK_RX_DIRREQV3RESP;
ALTER TABLE APP.RX_DIRREQV3TUNNELEDDL DROP CONSTRAINT FK_RX_DIRREQV3TUNNELEDDL;
ALTER TABLE APP.RX_DIRREQV3TUNNELEDDL DROP CONSTRAINT PK_RX_DIRREQV3TUNNELEDDL;
ALTER TABLE APP.RX_DIRREQWRITEHISTORY DROP CONSTRAINT FK_RX_DIRREQWRITEHISTORY;
ALTER TABLE APP.RX_DIRREQWRITEHISTORY DROP CONSTRAINT PK_RX_DIRREQWRITEHISTORY;
ALTER TABLE APP.RX_DIRREQWRITEHISTORY_BYTES DROP CONSTRAINT FK_RX_DIRREQWRITEHISTORY_BYTES;
ALTER TABLE APP.RX_DIRREQWRITEHISTORY_BYTES DROP CONSTRAINT PK_RX_DIRREQWRITEHISTORY_BYTES;
ALTER TABLE APP.RX_ENTRYIPS DROP CONSTRAINT FK_RX_ENTRYIPS;
ALTER TABLE APP.RX_ENTRYIPS DROP CONSTRAINT PK_RX_ENTRYIPS;
ALTER TABLE APP.RX_ENTRYSTATSEND DROP CONSTRAINT FK_RX_ENTRYSTATSEND;
ALTER TABLE APP.RX_ENTRYSTATSEND DROP CONSTRAINT PK_RX_ENTRYSTATSEND;
ALTER TABLE APP.RX_EXITKIBIBYTESREAD DROP CONSTRAINT FK_RX_EXITKIBIBYTESREAD;
ALTER TABLE APP.RX_EXITKIBIBYTESREAD DROP CONSTRAINT PK_RX_EXITKIBIBYTESREAD;
ALTER TABLE APP.RX_EXITKIBIBYTESWRITTEN DROP CONSTRAINT FK_RX_EXITKIBIBYTESWRITTEN;
ALTER TABLE APP.RX_EXITKIBIBYTESWRITTEN DROP CONSTRAINT PK_RX_EXITKIBIBYTESWRITTEN;
ALTER TABLE APP.RX_EXITSTATSEND DROP CONSTRAINT FK_RX_EXITSTATSEND;
ALTER TABLE APP.RX_EXITSTATSEND DROP CONSTRAINT PK_RX_EXITSTATSEND;
ALTER TABLE APP.RX_EXITSTREAMSOPENED DROP CONSTRAINT FK_RX_EXITSTREAMSOPENED;
ALTER TABLE APP.RX_EXITSTREAMSOPENED DROP CONSTRAINT PK_RX_EXITSTREAMSOPENED;
ALTER TABLE APP.RX_EXTRAINFO DROP CONSTRAINT FK_RX_EXTRAINFO;
ALTER TABLE APP.RX_EXTRAINFO DROP CONSTRAINT PK_RX_EXTRAINFO;
ALTER TABLE APP.RX_HIDSERVDIRONIONSSEEN DROP CONSTRAINT FK_RX_HIDSERVDIRONIONSSEEN;
ALTER TABLE APP.RX_HIDSERVDIRONIONSSEEN DROP CONSTRAINT PK_TABLE_1;
ALTER TABLE APP.RX_HIDSERVDIRONIONSSEEN_OBFUSCATION DROP CONSTRAINT FK_RX_HIDSERVDIRONIONSSEEN_OBFUSCATION;
ALTER TABLE APP.RX_HIDSERVDIRONIONSSEEN_OBFUSCATION DROP CONSTRAINT PK_RX_HIDSERVDIRONIONSSEEN_OBFUSCATION;
ALTER TABLE APP.RX_HIDSERVRENDRELAYEDCELLS DROP CONSTRAINT FK_RX_HIDSERVRENDRELAYEDCELLS;
ALTER TABLE APP.RX_HIDSERVRENDRELAYEDCELLS DROP CONSTRAINT PK_RX_HIDSERVRENDRELAYEDCELLS;
ALTER TABLE APP.RX_HIDSERVRENDRELAYEDCELLS_OBFUSCATION DROP CONSTRAINT FK_RX_HIDSERVRENDRELAYEDCELLS_OBFUSCATION;
ALTER TABLE APP.RX_HIDSERVRENDRELAYEDCELLS_OBFUSCATION DROP CONSTRAINT PK_RX_HIDSERVRENDRELAYEDCELLS_OBFUSCATION;
ALTER TABLE APP.RX_HIDSERVSTATSEND DROP CONSTRAINT FK_RX_HIDSERVSTATSEND;
ALTER TABLE APP.RX_HIDSERVSTATSEND DROP CONSTRAINT PK_RX_HIDSERVSTATSEND;
ALTER TABLE APP.RX_READHISTORY DROP CONSTRAINT FK_RX_READHISTORY;
ALTER TABLE APP.RX_READHISTORY DROP CONSTRAINT PK_RX_READHISTORY;
ALTER TABLE APP.RX_READHISTORY_BYTES DROP CONSTRAINT FK_RX_READHISTORY_BYTES;
ALTER TABLE APP.RX_READHISTORY_BYTES DROP CONSTRAINT PK_RX_READHISTORY_BYTES;
ALTER TABLE APP.RX_TRANSPORT DROP CONSTRAINT FK_RX_TRANSPORT;
ALTER TABLE APP.RX_TRANSPORT DROP CONSTRAINT PK_RX_TRANSPORT;
ALTER TABLE APP.RX_WRITEHISTORY DROP CONSTRAINT FK_RX_WRITEHISTORY;
ALTER TABLE APP.RX_WRITEHISTORY DROP CONSTRAINT PK_RX_WRITEHISTORY;
ALTER TABLE APP.RX_WRITEHISTORY_BYTES DROP CONSTRAINT FK_RX_WRITEHISTORY_BYTES;
ALTER TABLE APP.RX_WRITEHISTORY_BYTES DROP CONSTRAINT PK_RX_WRITEHISTORY_BYTES;
DROP TABLE APP.RX;
DROP TABLE APP.RX_CELLPROCESSEDCELLS;
DROP TABLE APP.RX_CELLQUEUEDCELLS;
DROP TABLE APP.RX_CELLSTATSEND;
DROP TABLE APP.RX_CELLTIMEINQUEUE;
DROP TABLE APP.RX_CONNBIDIRECT;
DROP TABLE APP.RX_DIRREQREADHISTORY;
DROP TABLE APP.RX_DIRREQREADHISTORY_BYTES;
DROP TABLE APP.RX_DIRREQSTATSEND;
DROP TABLE APP.RX_DIRREQV2DIRECTDL;
DROP TABLE APP.RX_DIRREQV2IPS;
DROP TABLE APP.RX_DIRREQV2REQS;
DROP TABLE APP.RX_DIRREQV2RESP;
DROP TABLE APP.RX_DIRREQV2TUNNELEDDL;
DROP TABLE APP.RX_DIRREQV3DIRECTDL;
DROP TABLE APP.RX_DIRREQV3IPS;
DROP TABLE APP.RX_DIRREQV3REQS;
DROP TABLE APP.RX_DIRREQV3RESP;
DROP TABLE APP.RX_DIRREQV3TUNNELEDDL;
DROP TABLE APP.RX_DIRREQWRITEHISTORY;
DROP TABLE APP.RX_DIRREQWRITEHISTORY_BYTES;
DROP TABLE APP.RX_ENTRYIPS;
DROP TABLE APP.RX_ENTRYSTATSEND;
DROP TABLE APP.RX_EXITKIBIBYTESREAD;
DROP TABLE APP.RX_EXITKIBIBYTESWRITTEN;
DROP TABLE APP.RX_EXITSTATSEND;
DROP TABLE APP.RX_EXITSTREAMSOPENED;
DROP TABLE APP.RX_EXTRAINFO;
DROP TABLE APP.RX_HIDSERVDIRONIONSSEEN;
DROP TABLE APP.RX_HIDSERVDIRONIONSSEEN_OBFUSCATION;
DROP TABLE APP.RX_HIDSERVRENDRELAYEDCELLS;
DROP TABLE APP.RX_HIDSERVRENDRELAYEDCELLS_OBFUSCATION;
DROP TABLE APP.RX_HIDSERVSTATSEND;
DROP TABLE APP.RX_READHISTORY;
DROP TABLE APP.RX_READHISTORY_BYTES;
DROP TABLE APP.RX_TRANSPORT;
DROP TABLE APP.RX_WRITEHISTORY;
DROP TABLE APP.RX_WRITEHISTORY_BYTES;




CREATE TABLE RX (
  id                   integer  NOT NULL  generated always as identity,
  src_date             bigint    ,
  type                 varchar(64)   DEFAULT 'extra-info 1.0' ,
  identity_ed25519     char(7)    ,
  published            bigint    ,
  geoip_db_digest      varchar(40)    ,
  geoip6_db_digest     varchar(40)    ,
  geoip_start_time     bigint    ,
  dirreq_v2_share      double    ,
  dirreq_v3_share      double    ,
  cell_circuits_per_decile integer    ,
  router_sig_ed25519   char(7)    ,
  router_signature     char(7)    ,
  extra_info_digest    varchar(40)    ,
  extra_info_digest_sha256 varchar(43)    ,
  master_key_ed25519   varchar(43)    ,
  CONSTRAINT Pk_be_0 PRIMARY KEY ( id ),
  CONSTRAINT Idx_RX UNIQUE ( published, extra_info_digest )
);
CREATE INDEX Idx_RX_src_date ON RX ( src_date );


CREATE TABLE RX_CellProcessedCells (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  mean                 integer    ,
  CONSTRAINT Pk_RX_CellProcessedCells PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_CellProcessedCells FOREIGN KEY ( RX_id ) REFERENCES RX( id )
);


CREATE TABLE RX_CellQueuedCells (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  mean                 double    ,
  CONSTRAINT Pk_RX_CellQueuedCells PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_CellQueuedCells FOREIGN KEY ( RX_id ) REFERENCES RX( id )
);


CREATE TABLE RX_CellStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_RX_CellStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_CellStatsEnd FOREIGN KEY ( id ) REFERENCES RX( id )
);


CREATE TABLE RX_CellTimeInQueue (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  mean                 integer    ,
  CONSTRAINT Pk_RX_CellTimeInQueue PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_CellTimeInQueue FOREIGN KEY ( RX_id ) REFERENCES RX( id )
);


CREATE TABLE RX_ConnBiDirect (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  below                integer    ,
  "read"               integer    ,
  "write"              integer    ,
  "both"               integer    ,
  CONSTRAINT Pk_RX_ConnBiDirect PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_ConnBiDirect FOREIGN KEY ( id ) REFERENCES RX( id )
);


CREATE TABLE RX_DirreqReadHistory (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_RX_DirreqReadHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqReadHistory FOREIGN KEY ( id ) REFERENCES RX( id )
);


CREATE TABLE RX_DirreqReadHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  RX_DirreqRead_id     integer  NOT NULL  ,
  bytes                bigint  NOT NULL  ,
  CONSTRAINT Pk_RX_DirreqReadHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqReadHistory_Bytes FOREIGN KEY ( RX_DirreqRead_id ) REFERENCES RX_DirreqReadHistory( id )
);


CREATE TABLE RX_DirreqStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_RX_DirreqStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqStatsEnd FOREIGN KEY ( id ) REFERENCES RX( id )
);


CREATE TABLE RX_DirreqV2DirectDl (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  "key"                varchar(32)    ,
  value                integer    ,
  CONSTRAINT Pk_RX_DirreqV2DirectDl PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV2DirectDl FOREIGN KEY ( RX_id ) REFERENCES RX( id )
);


CREATE TABLE RX_DirreqV2Ips (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_RX_DirreqV2Ips PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV2Ips FOREIGN KEY ( RX_id ) REFERENCES RX( id )
);


CREATE TABLE RX_DirreqV2Reqs (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_RX_DirreqV2Reqs PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV2Reqs FOREIGN KEY ( RX_id ) REFERENCES RX( id )
);


CREATE TABLE RX_DirreqV2Resp (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  response             varchar(256)    ,
  count                integer    ,
  CONSTRAINT Pk_RX_DirreqV2Resp PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV2Resp FOREIGN KEY ( RX_id ) REFERENCES RX( id )
);


CREATE TABLE RX_DirreqV2TunneledDl (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  "key"                varchar(32)    ,
  value                integer    ,
  CONSTRAINT Pk_RX_DirreqV2TunneledDl PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV2TunneledDl FOREIGN KEY ( RX_id ) REFERENCES RX( id )
);


CREATE TABLE RX_DirreqV3DirectDl (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  "key"                varchar(32)    ,
  value                integer    ,
  CONSTRAINT Pk_RX_DirreqV3DirectDl PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV3DirectDl FOREIGN KEY ( RX_id ) REFERENCES RX( id )
);


CREATE TABLE RX_DirreqV3Ips (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_RX_DirreqV3Ips PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV3Ips FOREIGN KEY ( RX_id ) REFERENCES RX( id )
);


CREATE TABLE RX_DirreqV3Reqs (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_RX_DirreqV3Reqs PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV3Reqs FOREIGN KEY ( RX_id ) REFERENCES RX( id )
);


CREATE TABLE RX_DirreqV3Resp (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  response             varchar(256)    ,
  count                integer    ,
  CONSTRAINT Pk_RX_DirreqV3Resp PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV3Resp FOREIGN KEY ( RX_id ) REFERENCES RX( id )
);


CREATE TABLE RX_DirreqV3TunneledDl (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  "key"                varchar(32)    ,
  value                integer    ,
  CONSTRAINT Pk_RX_DirreqV3TunneledDl PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqV3TunneledDl FOREIGN KEY ( RX_id ) REFERENCES RX( id )
);


CREATE TABLE RX_DirreqWriteHistory (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_RX_DirreqWriteHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqWriteHistory FOREIGN KEY ( id ) REFERENCES RX( id )
);


CREATE TABLE RX_DirreqWriteHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  RX_DirreqWrite_id    integer  NOT NULL  ,
  bytes                bigint  NOT NULL  ,
  CONSTRAINT Pk_RX_DirreqWriteHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_DirreqWriteHistory_Bytes FOREIGN KEY ( RX_DirreqWrite_id ) REFERENCES RX_DirreqWriteHistory( id )
);


CREATE TABLE RX_EntryIps (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  country              char(2)    ,
  count                integer    ,
  CONSTRAINT Pk_RX_EntryIps PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_EntryIps FOREIGN KEY ( RX_id ) REFERENCES RX( id )
);

CREATE TABLE RX_EntryStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_RX_EntryStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_EntryStatsEnd FOREIGN KEY ( id ) REFERENCES RX( id )
);


CREATE TABLE RX_ExitKibibytesRead (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  port                 varchar(5)    ,
  kib                  bigint    ,
  CONSTRAINT Pk_RX_ExitKibibytesRead PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_ExitKibibytesRead FOREIGN KEY ( RX_id ) REFERENCES RX( id )
);


CREATE TABLE RX_ExitKibibytesWritten (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  port                 varchar(5)    ,
  kib                  bigint    ,
  CONSTRAINT Pk_RX_ExitKibibytesWritten PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_ExitKibibytesWritten FOREIGN KEY ( RX_id ) REFERENCES RX( id )
);


CREATE TABLE RX_ExitStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_RX_ExitStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_ExitStatsEnd FOREIGN KEY ( id ) REFERENCES RX( id )
);


CREATE TABLE RX_ExitStreamsOpened (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  port                 varchar(5)    ,
  count                bigint    ,
  CONSTRAINT Pk_RX_ExitStreamsOpened PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_ExitStreamsOpened FOREIGN KEY ( RX_id ) REFERENCES RX( id )
);


CREATE TABLE RX_ExtraInfo (
  id                   integer  NOT NULL  ,
  nickname             varchar(19)    ,
  fingerprint          varchar(40)    ,
  CONSTRAINT Pk_RX_ExtraInfo PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_ExtraInfo FOREIGN KEY ( id ) REFERENCES RX( id )
);


CREATE TABLE RX_HidservDirOnionsSeen (
  id                   integer  NOT NULL  ,
  onions               double    ,
  CONSTRAINT Pk_Table_1 PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_HidservDirOnionsSeen FOREIGN KEY ( id ) REFERENCES RX( id )
);


CREATE TABLE RX_HidservDirOnionsSeen_Obfuscation (
  id                   integer  NOT NULL  generated always as identity,
  RX_Onion_id          integer  NOT NULL  ,
  obfuscation          varchar(32)    ,
  value                double    ,
  CONSTRAINT Pk_RX_HidservDirOnionsSeen_Obfuscation PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_HidservDirOnionsSeen_Obfuscation FOREIGN KEY ( RX_Onion_id ) REFERENCES RX_HidservDirOnionsSeen( id )
);


CREATE TABLE RX_HidservRendRelayedCells (
  id                   integer  NOT NULL  ,
  cells                double    ,
  CONSTRAINT Pk_RX_HidservRendRelayedCells PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_HidservRendRelayedCells FOREIGN KEY ( id ) REFERENCES RX( id )
);


CREATE TABLE RX_HidservRendRelayedCells_Obfuscation (
  id                   integer  NOT NULL  generated always as identity,
  RX_Relay_id          integer  NOT NULL  ,
  obfuscation          varchar(32)    ,
  value                double    ,
  CONSTRAINT Pk_RX_HidservRendRelayedCells_Obfuscation PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_HidservRendRelayedCells_Obfuscation FOREIGN KEY ( RX_Relay_id ) REFERENCES RX_HidservRendRelayedCells( id )
);


CREATE TABLE RX_HidservStatsEnd (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_RX_HidservStatsEnd PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_HidservStatsEnd FOREIGN KEY ( id ) REFERENCES RX( id )
);


CREATE TABLE RX_ReadHistory (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_RX_ReadHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_ReadHistory FOREIGN KEY ( id ) REFERENCES RX( id )
);


CREATE TABLE RX_ReadHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  RX_Read_id           integer  NOT NULL  ,
  bytes                bigint    ,
  CONSTRAINT Pk_RX_ReadHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_ReadHistory_Bytes FOREIGN KEY ( RX_Read_id ) REFERENCES RX_ReadHistory( id )
);


CREATE TABLE RX_Transport (
  id                   integer  NOT NULL  generated always as identity,
  RX_id                integer  NOT NULL  ,
  transport            varchar(32)    ,
  CONSTRAINT Pk_RX_Transport PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_Transport FOREIGN KEY ( RX_id ) REFERENCES RX( id )
);


CREATE TABLE RX_WriteHistory (
  id                   integer  NOT NULL  ,
  date                 bigint    ,
  interval             bigint    ,
  CONSTRAINT Pk_RX_WriteHistory PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_WriteHistory FOREIGN KEY ( id ) REFERENCES RX( id )
);


CREATE TABLE RX_WriteHistory_Bytes (
  id                   integer  NOT NULL  generated always as identity,
  RX_Write_id          integer  NOT NULL  ,
  bytes                bigint    ,
  CONSTRAINT Pk_RX_WriteHistory_Bytes PRIMARY KEY ( id ),
  CONSTRAINT fk_RX_WriteHistory_Bytes FOREIGN KEY ( RX_Write_id ) REFERENCES RX_WriteHistory( id )
);

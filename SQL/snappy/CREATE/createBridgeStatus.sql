ALTER TABLE APP.BS DROP CONSTRAINT IDX_BS;
ALTER TABLE APP.BS DROP CONSTRAINT PK_BS;
ALTER TABLE APP.BS_FLAGTRESHOLDS DROP CONSTRAINT FK_BS_FLAGTRESHOLDS;
ALTER TABLE APP.BS_FLAGTRESHOLDS DROP CONSTRAINT PK_BS_FLAGTRESHOLDS;
ALTER TABLE APP.BS_STATUS DROP CONSTRAINT FK_BS_STATUS;
ALTER TABLE APP.BS_STATUS DROP CONSTRAINT PK_BS_STATUS;
ALTER TABLE APP.BS_STATUS_A DROP CONSTRAINT FK_BS_STATUS_A;
ALTER TABLE APP.BS_STATUS_A DROP CONSTRAINT PK_BS_STATUS_A;
ALTER TABLE APP.BS_STATUS_P DROP CONSTRAINT FK_BS_STATUS_P;
ALTER TABLE APP.BS_STATUS_P DROP CONSTRAINT PK_BS_STATUS_P;
ALTER TABLE APP.BS_STATUS_R DROP CONSTRAINT FK_BS_STATUS_R;
ALTER TABLE APP.BS_STATUS_R DROP CONSTRAINT PK_BS_STATUS_R;
ALTER TABLE APP.BS_STATUS_S DROP CONSTRAINT FK_BS_STATUS_S;
ALTER TABLE APP.BS_STATUS_S DROP CONSTRAINT PK_BS_STATUS_S;
ALTER TABLE APP.BS_STATUS_W DROP CONSTRAINT FK_BS_STATUS_W;
ALTER TABLE APP.BS_STATUS_W DROP CONSTRAINT PK_BS_STATUS_W;
DROP TABLE APP.BS;
DROP TABLE APP.BS_FLAGTRESHOLDS;
DROP TABLE APP.BS_STATUS;
DROP TABLE APP.BS_STATUS_A;
DROP TABLE APP.BS_STATUS_P;
DROP TABLE APP.BS_STATUS_R;
DROP TABLE APP.BS_STATUS_S;
DROP TABLE APP.BS_STATUS_W;





CREATE TABLE BS (
  id                   integer  NOT NULL  generated always as identity,
  src_date             bigint    ,
  type                 varchar(64)   DEFAULT 'bridge-network-status 1.0' ,
  published            bigint    ,
  CONSTRAINT Pk_bs PRIMARY KEY ( id ),
  CONSTRAINT Idx_BS UNIQUE ( published )
);
CREATE INDEX Idx_BS_src_date ON BS ( src_date );


CREATE TABLE BS_FlagTresholds (
  id                   integer  NOT NULL  ,
  stable_uptime        bigint    ,
  stable_mtbf          bigint    ,
  enough_mtbf          integer    ,
  fast_speed           bigint    ,
  guard_wfu            double    ,
  guard_tk             bigint    ,
  guard_bw_inc_exits   bigint    ,
  guard_bw_exc_exits   bigint    ,
  ignoring_advertised_bws integer    ,
  CONSTRAINT Pk_bs_FlagTresholds PRIMARY KEY ( id ),
  CONSTRAINT fk_bs_FlagTresholds FOREIGN KEY ( id ) REFERENCES BS( id )
);


CREATE TABLE BS_Status (
  id                   integer  NOT NULL  generated always as identity,
  BS_id                integer  NOT NULL  ,
  v                    varchar(8)    ,
  CONSTRAINT Pk_bs_Status PRIMARY KEY ( id ),
  CONSTRAINT fk_bs_Status FOREIGN KEY ( BS_id ) REFERENCES BS( id )
);


CREATE TABLE BS_Status_A (
  id                   integer  NOT NULL  generated always as identity,
  BS_Status_id         integer  NOT NULL  ,
  address              varchar(256)    ,
  port                 integer    ,
  CONSTRAINT Pk_bs_Status_A PRIMARY KEY ( id ),
  CONSTRAINT fk_bs_Status_A FOREIGN KEY ( BS_Status_id ) REFERENCES BS_Status( id )
);


CREATE TABLE BS_Status_P (
  id                   integer  NOT NULL  ,
  default_policy       varchar(24)    ,
  port_summary         varchar(2048)    ,
  CONSTRAINT Pk_bs_Status_P PRIMARY KEY ( id ),
  CONSTRAINT fk_bs_Status_P FOREIGN KEY ( id ) REFERENCES BS_Status( id )
);


CREATE TABLE BS_Status_R (
  id                   integer  NOT NULL  ,
  nickname             varchar(19)    ,
  "identity"           varchar(40)    ,
  digest               varchar(40)    ,
  published            bigint    ,
  ip                   varchar(15)    ,
  or_port              integer    ,
  dir_port             integer    ,
  CONSTRAINT Pk_bs_Status_R PRIMARY KEY ( id ),
  CONSTRAINT fk_bs_Status_R FOREIGN KEY ( id ) REFERENCES BS_Status( id )
);


CREATE TABLE BS_Status_S (
  id                   integer  NOT NULL  generated always as identity,
  bs_Status_id         integer  NOT NULL  ,
  flag                 varchar(32)    ,
  CONSTRAINT Pk_bs_Status_S PRIMARY KEY ( id ),
  CONSTRAINT fk_bs_Status_S FOREIGN KEY ( bs_Status_id ) REFERENCES BS_Status( id )
);


CREATE TABLE BS_Status_W (
  id                   integer  NOT NULL  ,
  bandwidth            bigint    ,
  measured             bigint    ,
  unmeasured           char(7)    ,
  CONSTRAINT Pk_bs_Status_W PRIMARY KEY ( id ),
  CONSTRAINT fk_bs_Status_W FOREIGN KEY ( id ) REFERENCES BS_Status( id )
);

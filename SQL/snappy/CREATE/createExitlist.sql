ALTER TABLE APP.E DROP CONSTRAINT IDX_E;
ALTER TABLE APP.E DROP CONSTRAINT PK_E;
ALTER TABLE APP.E_EXITNODE DROP CONSTRAINT FK_E_EXITNODE;
ALTER TABLE APP.E_EXITNODE DROP CONSTRAINT PK_E_EXITNODE;
ALTER TABLE APP.E_EXITNODE_EXITADRESS DROP CONSTRAINT FK_E_EXITNODE_EXITADRESS;
ALTER TABLE APP.E_EXITNODE_EXITADRESS DROP CONSTRAINT PK_E_EXITNODE_EXITADRESS;
DROP TABLE APP.E;
DROP TABLE APP.E_EXITNODE;
DROP TABLE APP.E_EXITNODE_EXITADRESS;


CREATE TABLE E (
  id                   integer  NOT NULL  generated always as identity,
  src_date             bigint    ,
  type                 varchar(64)   DEFAULT 'tordnsel 1.0' ,
  downloaded           bigint    ,
  CONSTRAINT Pk_e PRIMARY KEY ( id ),
  CONSTRAINT Idx_E UNIQUE ( downloaded )
);
CREATE INDEX Idx_E_src_date ON E ( src_date );


CREATE TABLE E_ExitNode (
  id                   integer  NOT NULL  generated always as identity,
  E_id                 integer  NOT NULL  ,
  fingerprint          varchar(40)    ,
  published            bigint    ,
  last_status          bigint    ,
  CONSTRAINT Pk_e_ExitNode PRIMARY KEY ( id ),
  CONSTRAINT fk_e_ExitNode FOREIGN KEY ( E_id ) REFERENCES E( id )
);


CREATE TABLE E_ExitNode_ExitAdress (
  id                   integer  NOT NULL  generated always as identity,
  E_Node_id            integer  NOT NULL  ,
  adress               varchar(20)  NOT NULL  ,
  millis               bigint  NOT NULL  ,
  CONSTRAINT Pk_e_ExitNode_ExitAdress PRIMARY KEY ( id ),
  CONSTRAINT fk_e_ExitNode_ExitAdress FOREIGN KEY ( E_Node_id ) REFERENCES E_ExitNode( id )
);

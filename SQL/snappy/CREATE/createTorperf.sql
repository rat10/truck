ALTER TABLE APP.T DROP CONSTRAINT IDX_T;
ALTER TABLE APP.T DROP CONSTRAINT PK_T;
ALTER TABLE APP.T_BUILDTIMES DROP CONSTRAINT FK_T_BUILDTIMES;
ALTER TABLE APP.T_BUILDTIMES DROP CONSTRAINT PK_T_BUILDTIMES;
ALTER TABLE APP.T_PATH DROP CONSTRAINT FK_T_PATH;
ALTER TABLE APP.T_PATH DROP CONSTRAINT PK_T_PATH;
DROP TABLE APP.T;
DROP TABLE APP.T_BUILDTIMES;
DROP TABLE APP.T_PATH;



CREATE TABLE T (
  id                   integer  NOT NULL  generated always as identity,
  src_date             bigint    ,
  type                 varchar(64)   DEFAULT 'torperf 1.0' ,
  source               varchar(256)    ,
  filesize             integer    ,
  start                bigint    ,
  socket               bigint    ,
  "connect"            bigint    ,
  negotiate            bigint    ,
  request              bigint    ,
  response             bigint    ,
  datarequest          bigint    ,
  dataresponse         bigint    ,
  datacomplete         bigint    ,
  writebytes           integer    ,
  readbytes            integer    ,
  didtimeout           char(7)    ,
  dataperc10           bigint    ,
  dataperc20           bigint    ,
  dataperc30           bigint    ,
  dataperc40           bigint    ,
  dataperc50           bigint    ,
  dataperc60           bigint    ,
  dataperc70           bigint    ,
  dataperc80           bigint    ,
  dataperc90           bigint    ,
  launch               bigint    ,
  used_at              bigint    ,
  timeout              bigint    ,
  quantile             double    ,
  circ_id              integer    ,
  used_by              integer    ,
  CONSTRAINT Pk_t PRIMARY KEY ( id ),
  CONSTRAINT Idx_T UNIQUE ( start, source, filesize )
);
CREATE INDEX Idx_T_src_date ON T ( src_date );


CREATE TABLE T_Buildtimes (
  id                   integer  NOT NULL  generated always as identity,
  T_id                 integer  NOT NULL  ,
  value                bigint    ,
  CONSTRAINT Pk_t_Buildtimes PRIMARY KEY ( id ),
  CONSTRAINT fk_t_Buildtimes FOREIGN KEY ( T_id ) REFERENCES T( id )
);


CREATE TABLE T_Path (
  id                   integer  NOT NULL  generated always as identity,
  T_id                 integer  NOT NULL  ,
  fingerprint          varchar(40)    ,
  CONSTRAINT Pk_t_Path PRIMARY KEY ( id ),
  CONSTRAINT fk_t_Path FOREIGN KEY ( T_id ) REFERENCES T( id )
);
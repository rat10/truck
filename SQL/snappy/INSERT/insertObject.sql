-- snappy: You can execute transactions using commands autocommit off and commit.
--         http://rowstore.docs.snappydata.io/docs/reference/store_commands/autocommit.html

TODO  boolean

TODO  generate schemata from parquet
      make sure SQL tables/columns have the same names (so that select * works)

TODO ich muss ggfs nicht nur inserts sondern auch deletes definieren
     die delete statements werden nicht weniger umfangreich

/* ask something on stackoverflow
I need to insert nested records from Parquet files into a SQL database (SnappyData, which is based on Apache Derby).
The nested records contain arrays (and arrays of arrays, but let's not get into that). I'm looking for a way to:
 - insert the non-nested fields into a "main" table
 - get the primary key of that record
 - inserted the nested fields into "extra" tables
 - with the primary key of the main table as one field

It's lots of records (700 GB in Parquet) so some efficiency wouldn't hurt.
Also I might have to switch to another databse in the future so I'd rather not
rely on some very Derby/SnappyData specific solution.
 */

/*
shell script: get the directory your currently in
myvar=$PWD
cd $myvar
fileVar="$PWD/filename.txt"
*/
/*  how to make this run for all files in a directory?
https://stackoverflow.com/questions/28437282/how-to-run-an-sql-update-string-from-bash-script-with-parameter

*/

-- STUB on inserting a complex object into multiple tables at once
-- http://www.codinghelmet.com/?path=howto/bulk-insert

/*
BEGIN TRANSACTION

INSERT INTO [users].[SiteUser] ([Username], [FirstName], [LastName], [YearOfBirth])
VALUES ('someuser' /* @0 */, 'First' /* @1 */, 'Last' /* @2 */, 1982 /* @3 */)

SELECT [SiteUserID]
FROM [users].[SiteUser]
WHERE @@ROWCOUNT > 0 AND [SiteUserID] = SCOPE_IDENTITY()

INSERT INTO [users].[EmailAddress] ([SiteUserID], [Address], [MailsSentCount], [MailsReceivedCount])
VALUES (14936 /* @0 */, 'someone@somewhere.org' /* @1 */, 3096 /* @2 */, 1 /* @3 */)

SELECT [EmailAddressID]
FROM [users].[EmailAddress]
WHERE @@ROWCOUNT > 0 AND [EmailAddressID] = SCOPE_IDENTITY()

INSERT INTO [users].[EmailAddress] ([SiteUserID], [Address], [MailsSentCount], [MailsReceivedCount])
VALUES (14936 /* @0 */, 'alternate@somewhere.org' /* @1 */, 19367 /* @2 */, 2 /* @3 */)

SELECT [EmailAddressID]
FROM [users].[EmailAddress]
WHERE @@ROWCOUNT > 0 AND [EmailAddressID] = SCOPE_IDENTITY ()

COMMIT TRANSACTION

*/

-- drop staging table
DROP TABLE IF EXISTS exits;

-- create temporary staging table to load parquet formatted data
CREATE EXTERNAL TABLE exits
  USING PARQUET
  OPTIONS (
    path '/Users/t/test/exits.parquet'     -- TODO  how to call files per parameter
  )
;

AUTOCOMMIT off; -- begin transaction

  -- insert the main table
  INSERT INTO T (SELECT * FROM exits);
      -- hier gibts schon ein problem: die "id" spalte
      -- der insert * spart die _nicht_ automatisch aus
      -- also müssen doch alle attribute händisch eingepflegt werden
      -- was für ein spass!!!
      -- http://rowstore.docs.snappydata.io/docs/reference/language_ref/ref-create-table-clauses.html#topic_78D59C07CE254CFFA1FADD8EB7D399D8

  -- insert sub tables
  INSERT INTO T2 (SELECT * FROM exits WHERE identifier IS T.identifier);
  INSERT INTO T3 (SELECT * FROM exits WHERE identifier IS T.identifier);
  -- set 'T_id' columns for foreign keys
  INSERT INTO T2.T_id SELECT T.id;
       -- auch das geht u.u. so nicht: row store besteht auf dem einfügen von foreign keys
       -- http://rowstore.docs.snappydata.io/docs/reference/language_ref/ref-create-table-clauses.html#topic_78D59C07CE254CFFA1FADD8EB7D399D8

  INSERT INTO T3.T_id SELECT T.id;
  -- do this recursively for attributes nested in attributes
  -- ... this get's more complicated :-( ...

COMMIT; -- end transaction
AUTOCOMMIT on; -- necessary?




AUTOCOMMIT off; -- begin transaction


-- the main record
INSERT INTO E
  ( type, src_date, downloaded )
SELECT
  ( descriptor_type, src_date, downloaded )
FROM exits
;

-- the exit-nodes sub array
INSERT INTO E_ExitNode
  ( E_id, fingerprint, published, last_status )
SELECT
  ( E.id,
    LATERAL VIEW EXPLODE(exits.exit_nodes.fingerprint) ex_fingerprint
      as fingerprint,
    LATERAL VIEW EXPLODE(exits.exit_nodes.published) ex_published
      as published,
    LATERAL VIEW EXPLODE(exits.exit_nodes.last_status) ex_last_status
      as last_status
  )
FROM E
  LEFT JOIN exits            -- left join since exit_nodes may be null
  ON (
    E.type = exits.descriptor_type,
    E.src_date = exits.src_date,
    E.downloaded = exits.downloaded
  )
;

-- the map within an exit-node entry in the exit_adresses-nodes sub array
INSERT INTO E_ExitNode_ExitAdress
  ( E_Node_id, adress, millis )
SELECT
  ( E_ExitNode.id,
    LATERAL VIEW EXPLODE(exit_nodes.exit_adresses.key) ex_key as key,
    LATERAL VIEW EXPLODE(exit_nodes.exit_adresses.value) ex_value as value
  )
FROM E_ExitNode
  LEFT JOIN exits.exit_nodes -- left join since exit_adresses may be null
  ON (                       -- problem: this is not guaranteed to be unique
       E_ExitNode.fingerprint = exits.exit_nodes.fingerprint,
       E_ExitNode.published = exits.exit_nodes.published,
       E_ExitNode.last_status = exits.exit_nodes.last_status
)

;
COMMIT; -- end transaction
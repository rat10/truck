// RUNNING REAL SQL 


{"name":"Michael", "cities":["palo alto", "menlo park"], "schools":[ {"sname":"stanford", "years":[2010, 2011, 2013]}, {"sname":"berkeley", "years":[2012]} ] }
{"name":"Andy", "cities":["santa cruz"], "schools":[{"sname":"ucsb", "years":[2011, 2012]}]}
{"name":"Justin", "cities":["portland"], "schools":[ {"sname":"berkeley", "years":[2014, 2015], "grades": { "math": [2], "bio": [1,3], "lang": [1]}} ] }

scala> df.show()
+--------------------+-------+--------------------+
|              cities|   name|             schools|
+--------------------+-------+--------------------+
|[palo alto, menlo...|Michael|[[null,stanford,W...|                             ]]
|        [santa cruz]|   Andy|[[null,ucsb,Wrapp...|                             ]]
|          [portland]| Justin|[[[WrappedArray(1...|                             ]]
+--------------------+-------+--------------------+

CREATE EXTERNAL TABLE students USING json OPTIONS (path '/Users/t/people.json');

scala> df.printSchema()
root
 |-- cities: array (nullable = true)
 |    |-- element: string (containsNull = true)
 |-- name: string (nullable = true)
 |-- schools: array (nullable = true)
 |    |-- element: struct (containsNull = true)
 |    |    |-- grades: struct (nullable = true)
 |    |    |    |-- bio: array (nullable = true)
 |    |    |    |    |-- element: long (containsNull = true)
 |    |    |    |-- lang: array (nullable = true)
 |    |    |    |    |-- element: long (containsNull = true)
 |    |    |    |-- math: array (nullable = true)
 |    |    |    |    |-- element: long (containsNull = true)
 |    |    |-- sname: string (nullable = true)
 |    |    |-- years: array (nullable = true)
 |    |    |    |-- element: long (containsNull = true)



// Register the DataSet or DataFrame as a SQL temporary view
scala> df.createOrReplaceTempView("students")
scala> studentsDS.createOrReplaceTempView("studentDS")

scala> spark.sql("SELECT * FROM students").show()
+--------------------+-------+--------------------+
|              cities|   name|             schools|
+--------------------+-------+--------------------+
|[palo alto, menlo...|Michael|[[null,stanford,W...|                             ]]
|        [santa cruz]|   Andy|[[null,ucsb,Wrapp...|                             ]]
|          [portland]| Justin|[[[WrappedArray(1...|                             ]]
+--------------------+-------+--------------------+

// da geht einiges
scala> spark.sql("select name, schools[0].grades from studentDS").show()
+-------+--------------------+
|   name|   schools[0].grades|
+-------+--------------------+
|Michael|                null|
|   Andy|                null|
| Justin|[WrappedArray(1, ...|
+-------+--------------------+

scala> spark.sql("select name, schools.years from studentDS").show()
+-------+--------------------+
|   name|               years|
+-------+--------------------+
|Michael|[WrappedArray(201...|
|   Andy|[WrappedArray(201...|
| Justin|[WrappedArray(201...|
+-------+--------------------+

scala> spark.sql("select name, schools.years[0] from studentDS").show()
+-------+---------------------------+
|   name|schools.years AS `years`[0]|
+-------+---------------------------+
|Michael|         [2010, 2011, 2013]|
|   Andy|               [2011, 2012]|
| Justin|               [2014, 2015]|
+-------+---------------------------+

scala> spark.sql("select name, schools.years[1] from studentDS").show()
+-------+---------------------------+
|   name|schools.years AS `years`[1]|
+-------+---------------------------+
|Michael|                     [2012]|
|   Andy|                       null|
| Justin|                       null|
+-------+---------------------------+

// on DataSet
scala> spark.sql("select name, schools.years[0][2] from studentDS").show()
+-------+------------------------------+
|   name|schools.years AS `years`[0][2]|
+-------+------------------------------+
|Michael|                          2013|
|   Andy|                          null|
| Justin|                          null|
+-------+------------------------------+

// on DataFrame
scala> spark.sql("select name, schools.years[0][2] from students").show()
+-------+------------------------------+
|   name|schools.years AS `years`[0][2]|
+-------+------------------------------+
|Michael|                          2013|
|   Andy|                          null|
| Justin|                          null|
+-------+------------------------------+

scala> spark.sql("select name, schools from students").show()
+-------+--------------------+
|   name|             schools|
+-------+--------------------+
|Michael|[[null,stanford,W...|                                                  ]]
|   Andy|[[null,ucsb,Wrapp...|                                                  ]]
| Justin|[[[WrappedArray(1...|                                                  ]]
+-------+--------------------+                             

scala> spark.sql("select name, cities from students").show()
+-------+--------------------+
|   name|              cities|
+-------+--------------------+
|Michael|[palo alto, menlo...|
|   Andy|        [santa cruz]|
| Justin|          [portland]|
+-------+--------------------+

// lateral view explode
// https://stackoverflow.com/questions/36876959/sparksql-can-i-explode-two-different-variables-in-the-same-query
scala> spark.sql("select name, city from students lateral view explode(cities) ex_cities as city").show()
+-------+----------+
|   name|      city|
+-------+----------+
|Michael| palo alto|
|Michael|menlo park|
|   Andy|santa cruz|
| Justin|  portland|
+-------+----------+

// 2 explodes in one query
scala> 
spark.sql("
  select 
    name, city, bio  
  from 
    students 
      lateral view explode(cities) ex_cities as city 
      lateral view explode(schools.grades.bio) ex_bio as bio
").show()
+-------+----------+------+
|   name|      city|   bio|
+-------+----------+------+
|Michael| palo alto|  null|
|Michael| palo alto|  null|
|Michael|menlo park|  null|
|Michael|menlo park|  null|
|   Andy|santa cruz|  null|
| Justin|  portland|[1, 3]|
+-------+----------+------+

scala> spark.sql("
  select 
    name, city, bio
  from 
    students 
      lateral view explode(cities) ex_cities as city 
      lateral view explode(schools.grades.bio) ex_bio as bio 
  where 
    bio[0]=1L
").show()
+------+--------+------+
|  name|    city|   bio|
+------+--------+------+
|Justin|portland|[1, 3]|
+------+--------+------+                                                     ^

scala> spark.sql("select name, city, bio  from students lateral view explode(cities) ex_cities as city lateral view explode(schools.grades.bio) ex_bio as bio where bio[0]=1L AND city='palo alto'").show()
+----+----+---+
|name|city|bio|
+----+----+---+
+----+----+---+

// triple """ escape inner "
scala> spark.sql("""select name, city, bio  from students lateral view explode(cities) ex_cities as city lateral view explode(schools.grades.bio) ex_bio as bio where bio[0]=1L AND city="palo alto"""").show()
+----+----+---+
|name|city|bio|
+----+----+---+
+----+----+---+

// and now for something totally unrelated: truncate=false
scala> spark.sql("select name, schools from students").show(truncate=false)
+-------+------------------------------------------------------------------------------------------+
|name   |schools                                                                                   |
+-------+------------------------------------------------------------------------------------------+
|Michael|[[null,stanford,WrappedArray(2010, 2011, 2013)], [null,berkeley,WrappedArray(2012)]]      |    ]]
|Andy   |[[null,ucsb,WrappedArray(2011, 2012)]]                                                    |    ]]
|Justin |[[[WrappedArray(1, 3),WrappedArray(1),WrappedArray(2)],berkeley,WrappedArray(2014, 2015)]]|    ]]
+-------+------------------------------------------------------------------------------------------+


scala> spark.sql("select name, schools.years from studentDS").show()
+-------+--------------------+
|   name|               years|
+-------+--------------------+
|Michael|[WrappedArray(201...|
|   Andy|[WrappedArray(201...|
| Justin|[WrappedArray(201...|
+-------+--------------------+


scala> spark.sql("select name, schools.years from students").show()
+-------+--------------------+
|   name|               years|
+-------+--------------------+
|Michael|[WrappedArray(201...|
|   Andy|[WrappedArray(201...|
| Justin|[WrappedArray(201...|
+-------+--------------------+


scala> spark.sql("select name, schools.years from students").show(truncate=false)
+-------+----------------------------------------------------+
|name   |years                                               |
+-------+----------------------------------------------------+
|Michael|[WrappedArray(2010, 2011, 2013), WrappedArray(2012)]|
|Andy   |[WrappedArray(2011, 2012)]                          |
|Justin |[WrappedArray(2014, 2015)]                          |
+-------+----------------------------------------------------+


scala> spark.sql("select name, schools.years[0] from students").show(truncate=false)
+-------+---------------------------+
|name   |schools.years AS `years`[0]|
+-------+---------------------------+
|Michael|[2010, 2011, 2013]         |
|Andy   |[2011, 2012]               |
|Justin |[2014, 2015]               |
+-------+---------------------------+


scala> spark.sql("select name, schools.years[0][0] from students").show(truncate=false)
+-------+------------------------------+
|name   |schools.years AS `years`[0][0]|
+-------+------------------------------+
|Michael|2010                          |
|Andy   |2011                          |
|Justin |2014                          |
+-------+------------------------------+
{"name":"Michael", "cities":["palo alto", "menlo park"], "schools":[ {"sname":"stanford", "years":[2010, 2011, 2013]}, {"sname":"berkeley", "years":[2012]} ] }
{"name":"Andy", "cities":["santa cruz"], "schools":[{"sname":"ucsb", "years":[2011, 2012]}]}
{"name":"Justin", "cities":["portland"], "schools":[ {"sname":"berkeley", "years":[2014, 2015], "grades": { "math": [2], "bio": [1,3], "lang": [1]}} ] }

    
    scala> val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    scala> import sqlContext.implicits._
    scala> val people = sqlContext.read.json("/Users/t/people.json")
    
    
    scala> people.show()
    +--------------------+-------+--------------------+
    |              cities|   name|             schools|
    +--------------------+-------+--------------------+
    |[palo alto, menlo...|Michael|[[null,stanford,W...|
    |        [santa cruz]|   Andy|[[null,ucsb,Wrapp...|
    |          [portland]| Justin|[[[WrappedArray(1...|
    +--------------------+-------+--------------------+
    
    scala> people.printSchema()
    root
     |-- cities: array (nullable = true)
     |    |-- element: string (containsNull = true)
     |-- name: string (nullable = true)
     |-- schools: array (nullable = true)
     |    |-- element: struct (containsNull = true)
     |    |    |-- grades: struct (nullable = true)
     |    |    |    |-- bio: array (nullable = true)
     |    |    |    |    |-- element: long (containsNull = true)
     |    |    |    |-- lang: array (nullable = true)
     |    |    |    |    |-- element: long (containsNull = true)
     |    |    |    |-- math: array (nullable = true)
     |    |    |    |    |-- element: long (containsNull = true)
     |    |    |-- sname: string (nullable = true)
     |    |    |-- years: array (nullable = true)
     |    |    |    |-- element: long (containsNull = true)
    
    scala> people.select($"name",explode($"schools.grades")).show()
    +-------+--------------------+
    |   name|                 col|
    +-------+--------------------+
    |Michael|                null|
    |Michael|                null|
    |   Andy|                null|
    | Justin|[WrappedArray(1, ...|
    +-------+--------------------+
    
    scala> people.select($"name",explode($"schools.grades.bio")).show()
    +-------+------+
    |   name|   col|
    +-------+------+
    |Michael|  null|
    |Michael|  null|
    |   Andy|  null|
    | Justin|[1, 3]|
    +-------+------+
    
    scala> people.select($"name",explode($"schools.years")).show()
    +-------+------------------+
    |   name|               col|
    +-------+------------------+
    |Michael|[2010, 2011, 2013]|
    |Michael|            [2012]|
    |   Andy|      [2011, 2012]|
    | Justin|      [2014, 2015]|
    +-------+------------------+
    
    scala> people.select($"name",explode($"schools"), explode($"schools.grades.bio")).show()
    org.apache.spark.sql.AnalysisException: Only one generator allowed per select clause but found 2: explode(schools), explode(schools.grades.bio AS `bio`);
    
    scala> people.select($"name",explode($"schools.years[0]")).show()
    org.apache.spark.sql.AnalysisException: No such struct field years[0] in grades, sname, years;
    
    
    scala> people.select($"schools").show()
    +--------------------+
    |             schools|
    +--------------------+
    |[[null,stanford,W...|
    |[[null,ucsb,Wrapp...|
    |[[[WrappedArray(1...|
    +--------------------+
    
    scala> people.select($"name",explode($"schools"), explode($"schools.grades.bio")).show()
    org.apache.spark.sql.AnalysisException: Only one generator allowed per select clause but found 2: explode(schools), explode(schools.grades.bio AS `bio`);
    

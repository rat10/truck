    {
      "descriptor_type": "network-status-consensus-3 1.0",
      "src_date": 1472792701000,
      "network_status_version": 3,
      "vote_status": "consensus",
      "consensus_method": 20,
      "consensus_flavor": null,
      "valid_after": 1472792400000,
      "fresh_until": 1472796000000,
      "valid_until": 1472803200000,
      "voting_delay": {
        "vote_seconds": 300,
        "dist_seconds": 300
      },
      "client_versions": [
        "0.2.4.26",
        "0.2.4.27",
        "0.2.5.11",
        "0.2.5.12",
        "0.2.6.5-rc",
        "0.2.6.6",
        "0.2.6.7",
        "0.2.6.8",
        "0.2.6.9",
        "0.2.6.10",
        "0.2.7.1-alpha",
        "0.2.7.2-alpha",
        "0.2.7.3-rc",
        "0.2.7.4-rc",
        "0.2.7.5",
        "0.2.7.6",
        "0.2.8.1-alpha",
        "0.2.8.2-alpha",
        "0.2.8.3-alpha",
        "0.2.8.4-rc",
        "0.2.8.5-rc",
        "0.2.8.6",
        "0.2.8.7",
        "0.2.9.1-alpha",
        "0.2.9.2-alpha"
      ],
      "server_versions": [
        "0.2.4.26",
        "0.2.4.27",
        "0.2.5.11",
        "0.2.5.12",
        "0.2.6.5-rc",
        "0.2.6.6",
        "0.2.6.7",
        "0.2.6.8",
        "0.2.6.9",
        "0.2.6.10",
        "0.2.7.1-alpha",
        "0.2.7.2-alpha",
        "0.2.7.3-rc",
        "0.2.7.4-rc",
        "0.2.7.5",
        "0.2.7.6",
        "0.2.8.1-alpha",
        "0.2.8.2-alpha",
        "0.2.8.3-alpha",
        "0.2.8.4-rc",
        "0.2.8.5-rc",
        "0.2.8.6",
        "0.2.8.7",
        "0.2.9.1-alpha",
        "0.2.9.2-alpha"
      ],
      "package": null,
      "known_flags": [
        "Authority",
        "BadExit",
        "Exit",
        "Fast",
        "Guard",
        "HSDir",
        "Running",
        "Stable",
        "V2Dir",
        "Valid"
      ],
      "params": {
        "CircuitPriorityHalflifeMsec": 30000,
        "NumDirectoryGuards": 3,
        "NumEntryGuards": 1,
        "NumNTorsPerTAP": 100,
        "Support022HiddenServices": 0,
        "UseNTorHandshake": 1,
        "UseOptimisticData": 1,
        "bwauthpid": 1,
        "cbttestfreq": 60,
        "pb_disablepct": 0,
        "usecreatefast": 0
      },
      "authorities": {
        "0232AF901C31A04EE9848595AF9BB7620D4C5B2E": {
          "dir_source": {
            "nickname": "dannenberg",
            "identity": "0232AF901C31A04EE9848595AF9BB7620D4C5B2E",
            "hostname": "dannenberg.torauth.de",
            "address": "193.23.244.244",
            "dir_port": 80,
            "or_port": 443,
            "is_legacy": false
          },
          "contact": "Andreas Lehner",
          "vote_digest": "DBFE1E54E671206BBFB53A1D90BD9C1CAE511742"
        },
        "14C131DFC5C6F93646BE72FA1401C02A8DF2E8B4": {
          "dir_source": {
            "nickname": "tor26",
            "identity": "14C131DFC5C6F93646BE72FA1401C02A8DF2E8B4",
            "hostname": "86.59.21.38",
            "address": "86.59.21.38",
            "dir_port": 80,
            "or_port": 443,
            "is_legacy": false
          },
          "contact": "Peter Palfrader",
          "vote_digest": "3F16C989D89B3A7CB83391D0FD4C7BB723A04027"
        },
        "E8A9C45EDE6D711294FADF8E7951F4DE6CA56B58": {
          "dir_source": {
            "nickname": "dizum",
            "identity": "E8A9C45EDE6D711294FADF8E7951F4DE6CA56B58",
            "hostname": "194.109.206.212",
            "address": "194.109.206.212",
            "dir_port": 80,
            "or_port": 443,
            "is_legacy": false
          },
          "contact": "1024R/8D56913D Alex de Joode <adejoode@sabotage.org>",
          "vote_digest": "3637DC17A1E6BF5E4934AB79D0945A934C8B6EFF"
        },
        "EFCBE720AB3A82B99F9E953CD5BF50F7EEFC7B97": {
          "dir_source": {
            "nickname": "Faravahar",
            "identity": "EFCBE720AB3A82B99F9E953CD5BF50F7EEFC7B97",
            "hostname": "154.35.175.225",
            "address": "154.35.175.225",
            "dir_port": 80,
            "or_port": 443,
            "is_legacy": false
          },
          "contact": "0x0B47D56D Sina Rabbani (inf0) <sina redteam net>",
          "vote_digest": "CE898BED57B39BD8C922F431EFCE5F63680C56EE"
        },
        "23D15D965BC35114467363C165C4F724B64B4F66": {
          "dir_source": {
            "nickname": "longclaw",
            "identity": "23D15D965BC35114467363C165C4F724B64B4F66",
            "hostname": "199.254.238.53",
            "address": "199.254.238.53",
            "dir_port": 80,
            "or_port": 443,
            "is_legacy": false
          },
          "contact": "Riseup Networks <collective at riseup dot net> - 1nNzekuHGGzBYRzyjfjFEfeisNvxkn4RT",
          "vote_digest": "6C7277F10473CE6832358C2CE3EA2A1460307CF9"
        },
        "49015F787433103580E3B66A1707A00E60F2D15B": {
          "dir_source": {
            "nickname": "maatuska",
            "identity": "49015F787433103580E3B66A1707A00E60F2D15B",
            "hostname": "171.25.193.9",
            "address": "171.25.193.9",
            "dir_port": 443,
            "or_port": 80,
            "is_legacy": false
          },
          "contact": "4096R/23291265 Linus Nordberg <linus@nordberg.se>",
          "vote_digest": "9181BACB11F82731463EE6810DA6D24E9AB9F48C"
        },
        "ED03BB616EB2F60BEC80151114BB25CEF515B226": {
          "dir_source": {
            "nickname": "gabelmoo",
            "identity": "ED03BB616EB2F60BEC80151114BB25CEF515B226",
            "hostname": "131.188.40.189",
            "address": "131.188.40.189",
            "dir_port": 80,
            "or_port": 443,
            "is_legacy": false
          },
          "contact": "4096R/261C5FBE77285F88FB0C343266C8C2D7C5AA446D Sebastian Hahn <tor@sebastianhahn.net> - 12NbRAjAG5U3LLWETSF7fSTcdaz32Mu5CN",
          "vote_digest": "F7813D610B46F3038967C74C86DD20B5CA942C5E"
        },
        "D586D18309DED4CD6D57C18FDB97EFA96D330566": {
          "dir_source": {
            "nickname": "moria1",
            "identity": "D586D18309DED4CD6D57C18FDB97EFA96D330566",
            "hostname": "128.31.0.34",
            "address": "128.31.0.34",
            "dir_port": 9131,
            "or_port": 9101,
            "is_legacy": false
          },
          "contact": "1024D/28988BF5 arma mit edu",
          "vote_digest": "54CAA8EA3F688756100CF98655C1EB7D79BDEE55"
        }
      },
      "status": {
        "4ED9DE16C89ED66982D09E15A9269D6C3E5BC448": {
          "r": {
            "nickname": "herzausgold",
            "identity": "4ED9DE16C89ED66982D09E15A9269D6C3E5BC448",
            "digest": "2A159D31F459D9DA9278F056A8E855C8C4884241",
            "publication": 1472750441000,
            "ip": "176.9.140.108",
            "or_port": 9001,
            "dir_port": 0
          },
          "a": null,
          "s": [
            "Fast",
            "Guard",
            "Running",
            "Stable",
            "Valid"
          ],
          "v": "Tor 0.2.8.6",
          "w": {
            "bandwidth": 3870,
            "measured": null,
            "unmeasured": false
          },
          "p": {
            "default_policy": "accept",
            "port_summary": ""
          }
        },
        "629FE436746E0AACDFC93D2DA70B5E269F7ADC79": {
          "r": {
            "nickname": "BungeeNet1",
            "identity": "629FE436746E0AACDFC93D2DA70B5E269F7ADC79",
            "digest": "AF1524CC22029639BBAE56B1A7EA231B01A2DB1C",
            "publication": 1472770930000,
            "ip": "159.203.11.12",
            "or_port": 443,
            "dir_port": 80
          },
          "a": null,
          "s": [
            "Exit",
            "Fast",
            "Guard",
            "HSDir",
            "Running",
            "Stable",
            "V2Dir",
            "Valid"
          ],
          "v": "Tor 0.2.8.6",
          "w": {
            "bandwidth": 5050,
            "measured": null,
            "unmeasured": false
          },
          "p": {
            "default_policy": "accept",
            "port_summary": "20-23,43,53,79-81,88,110,143,194,220,389,443,464-465,531,543-544,554,563,587,636,706,749,873,902-904,981,989-995,1194,1220,1293,1500,1533,1677,1723,1755,1863,2082-2083,2086-2087,2095-2096,2102-2104,3128,3389,3690,4321,4643,5050,5190,5222-5223,5228,5900,6660-6669,6679,6697,8000,8008,8074,8080,8082,8087-8088,8332-8333,8443,8888,9418,9999-10000,11371,19294,19638,50002,64738"
          }
        },
    

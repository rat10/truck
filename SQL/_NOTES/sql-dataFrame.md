{"name":"Michael", "cities":["palo alto", "menlo park"], "schools":[ {"sname":"stanford", "years":[2010, 2011, 2013]}, {"sname":"berkeley", "years":[2012]} ] }
{"name":"Andy", "cities":["santa cruz"], "schools":[{"sname":"ucsb", "years":[2011, 2012]}]}
{"name":"Justin", "cities":["portland"], "schools":[ {"sname":"berkeley", "years":[2014, 2015], "grades": { "math": [2], "bio": [1,3], "lang": [1]}} ] }


scala> import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.SparkSession


// For implicit conversions like converting RDDs to DataFrames
// This import is also needed to use the $-notation
scala> import spark.implicits._
import spark.implicits._

// SparkSession in Spark 2.0 provides builtin support for Hive features including 
// the ability to write queries using HiveQL, access to Hive UDFs. To use these 
// features, you do not need to have an existing Hive setup.
scala> val spark = SparkSession.builder().appName("da biste platt").getOrCreate()
16/12/12 16:08:07 WARN SparkSession$Builder: Use an existing SparkSession, some configuration may not take effect.
spark: org.apache.spark.sql.SparkSession = org.apache.spark.sql.SparkSession@29db3f23

// With a SparkSession, applications can create DataFrames from Spark data sources.
// DataFrames are just Dataset of Rows in Scala, also referred to as 
// "untyped transformations" in contrast to "typed transformations" that come with 
// strongly typed Scala/Java Datasets.
scala> val df = spark.read.json("/Users/t/people.json")
df: org.apache.spark.sql.DataFrame = [cities: array<string>, name: string ... 1 more field]

scala> df.show()
+--------------------+-------+--------------------+
|              cities|   name|             schools|
+--------------------+-------+--------------------+
|[palo alto, menlo...|Michael|[[null,stanford,W...|
|        [santa cruz]|   Andy|[[null,ucsb,Wrapp...|
|          [portland]| Justin|[[[WrappedArray(1...|
+--------------------+-------+--------------------+


scala> df.printSchema()
root
 |-- cities: array (nullable = true)
 |    |-- element: string (containsNull = true)
 |-- name: string (nullable = true)
 |-- schools: array (nullable = true)
 |    |-- element: struct (containsNull = true)
 |    |    |-- grades: struct (nullable = true)
 |    |    |    |-- bio: array (nullable = true)
 |    |    |    |    |-- element: long (containsNull = true)
 |    |    |    |-- lang: array (nullable = true)
 |    |    |    |    |-- element: long (containsNull = true)
 |    |    |    |-- math: array (nullable = true)
 |    |    |    |    |-- element: long (containsNull = true)
 |    |    |-- sname: string (nullable = true)
 |    |    |-- years: array (nullable = true)
 |    |    |    |-- element: long (containsNull = true)


scala> df.select($"name",explode($"schools.grades")).show()
+-------+--------------------+
|   name|                 col|
+-------+--------------------+
|Michael|                null|
|Michael|                null|
|   Andy|                null|
| Justin|[WrappedArray(1, ...|
+-------+--------------------+

scala> df.select($"name",explode($"schools.grades.bio")).show()
+-------+------+
|   name|   col|
+-------+------+
|Michael|  null|
|Michael|  null|
|   Andy|  null|
| Justin|[1, 3]|
+-------+------+

scala> df.select($"name",explode($"schools.years")).show()
+-------+------------------+
|   name|               col|
+-------+------------------+
|Michael|[2010, 2011, 2013]|
|Michael|            [2012]|
|   Andy|      [2011, 2012]|
| Justin|      [2014, 2015]|
+-------+------------------+

scala> df.select($"name",explode($"schools"), explode($"schools.grades.bio")).show()
org.apache.spark.sql.AnalysisException: Only one generator allowed per select clause but found 2: explode(schools), explode(schools.grades.bio AS `bio`);

scala> df.select($"name",explode($"schools.years[0]")).show()
org.apache.spark.sql.AnalysisException: No such struct field years[0] in grades, sname, years;

scala> df.select($"schools").show()
+--------------------+
|             schools|
+--------------------+
|[[null,stanford,W...|
|[[null,ucsb,Wrapp...|
|[[[WrappedArray(1...|
+--------------------+

scala> df.select($"name",explode($"schools"), explode($"schools.grades.bio")).show()
org.apache.spark.sql.AnalysisException: Only one generator allowed per select clause but found 2: explode(schools), explode(schools.grades.bio AS `bio`);





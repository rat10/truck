DEFINING A DataSet THROUGH CASE CLASSES

// Note: Case classes in Scala 2.10 can support only up to 22 fields. To work around this limit,
// you can use custom classes that implement the Product interface
// see https://stackoverflow.com/questions/28689186

// the schema

    scala> df.printSchema()
    root
     |-- cities: array (nullable = true)
     |    |-- element: string (containsNull = true)
     |-- name: string (nullable = true)
     |-- schools: array (nullable = true)
     |    |-- element: struct (containsNull = true)
     |    |    |-- grades: struct (nullable = true)
     |    |    |    |-- bio: array (nullable = true)
     |    |    |    |    |-- element: long (containsNull = true)
     |    |    |    |-- lang: array (nullable = true)
     |    |    |    |    |-- element: long (containsNull = true)
     |    |    |    |-- math: array (nullable = true)
     |    |    |    |    |-- element: long (containsNull = true)
     |    |    |-- sname: string (nullable = true)
     |    |    |-- years: array (nullable = true)
     |    |    |    |-- element: long (containsNull = true)

// define case classe 

    scala> case class Grades(bio: Array[Long], lang: Array[Long], math: Array[Long])
    scala> case class School(grades: Grades, sname: String, years: Array[Long])
    scala> case class Student(cities: Array[String], name: String, schools: Array[School])

// load and encode data as DataSet

    scala> val studentsDS = spark.read.json("/Users/t/people.json").as[Student]
  
// triumph!

    scala> studentsDS.select($"name",explode($"schools.years")).show()
    +-------+------------------+
    |   name|               col|
    +-------+------------------+
    |Michael|[2010, 2011, 2013]|
    |Michael|            [2012]|
    |   Andy|      [2011, 2012]|
    | Justin|      [2014, 2015]|
    +-------+------------------+

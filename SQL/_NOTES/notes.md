# BOOLEAN

    tomlurge
    Is it correct that the row store doesn't support data type BOOLEAN'?
    jagsr123
    I believe you can use CHAR and use the CAST function to operate as Boolean.  
    Must be covered in the docs presumably.
    I am hoping this will be supported as we consolidate the type system with 
    Spark before we go GA.
    
    


# identity - how to establish identity on converted descriptors
    
    relay           type + published + fingerprint   (could as well be digest)
    bridgeExtra     type + published + fingerprint   (could as well be digest)
    relayExtra      type + published + fingerprint   (could as well be digest) 
    bridge          type + published + fingerprint   (could as well be digest)
    relayConsensus  type + valid-after 
    bridgeStatus    type + valid-after  ???
    relayVote       type + valid-after + identity
    tordnsel        type + downloaded 
    torperf         type + start + source + filesize
    
    
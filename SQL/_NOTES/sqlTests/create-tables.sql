drop table if exists sub2;
drop table if exists sub1;
drop table if exists test;
drop table if exists stage;


create table test (
  id                   integer  NOT NULL  generated always as identity,
  downloaded           bigint,
  testchar             varchar(10),
  CONSTRAINT Pk_test   PRIMARY KEY ( id )
) USING row OPTIONS();

create table sub1 (
  id                   integer  NOT NULL  generated always as identity,
  test_id              integer  NOT NULL  ,
  fingerprint          varchar(40),
  CONSTRAINT Pk_sub1   PRIMARY KEY ( id ),
  CONSTRAINT fk_test   FOREIGN KEY ( test_id ) REFERENCES test( id )
) USING row OPTIONS();

create table sub2 (
  id                   integer  NOT NULL  generated always as identity,
  sub1_id              integer  NOT NULL  ,
  adress               varchar(20)  NOT NULL  ,
  millis               bigint  NOT NULL  ,
  CONSTRAINT Pk_sub2   PRIMARY KEY ( id ),
  CONSTRAINT fk_sub1   FOREIGN KEY ( sub1_id ) REFERENCES sub1( id )
) USING row OPTIONS();

CREATE EXTERNAL TABLE stage USING PARQUET OPTIONS (path '/Users/t/torData/snappy/RelayConsensus_2016-09.parquet.snappy');


insert into test(testchar) values('hi');
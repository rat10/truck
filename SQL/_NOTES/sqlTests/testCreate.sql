drop table if exists test.eins;

create schema test;

create table test.eins (
  id       integer  NOT NULL  generated always as identity,
  nummy    integer,
  bla      varchar(256)
)

scala> parquetFile.printSchema()

    root
    |-- downloaded: long (nullable = true)
    |-- exit_nodes: array (nullable = true)
    |    |-- element: struct (containsNull = true)
    |    |    |-- fingerprint: string (nullable = true)
    |    |    |-- exit_adresses: map (nullable = true)
    |    |    |    |-- key: string
    |    |    |    |-- value: long (valueContainsNull = true)



-- todo jdbc connection to snappy localhost:1527

drop table if exists test;
drop table if exists sub1;
drop table if exists sub2;


create table test (
  id                   integer  NOT NULL  generated always as identity,
  downloaded           bigint
) USING row OPTIONS();

create table sub1 (
  id                   integer  NOT NULL  generated always as identity,
  test_id              integer  NOT NULL  ,
  fingerprint          varchar(40),
  CONSTRAINT Pk_sub1 PRIMARY KEY ( id ),
  CONSTRAINT fk_test FOREIGN KEY ( test_id ) REFERENCES test( id )
) USING row OPTIONS();

create table sub2 (
  id                   integer  NOT NULL  generated always as identity,
  sub1_id              integer  NOT NULL  ,
  adress               varchar(20)  NOT NULL  ,
  millis               bigint  NOT NULL  ,
  CONSTRAINT Pk_sub2 PRIMARY KEY ( id ),
  CONSTRAINT fk_sub1 FOREIGN KEY ( sub1_id ) REFERENCES sub1( id )
) USING row OPTIONS();

CREATE /*EXTERNAL*/ TABLE stage
  USING PARQUET
  OPTIONS (path '/Users/t/snappy/test/tordnsel.parquet.snappy');

-- todo   get _this_ running and check how staging_tordnsel actually looks like



AUTOCOMMIT off;


insert into test (downloaded)  -- variant: 'id' here and 'DEFAULT' in 'values'
    select stage.downloaded
;


insert into sub1 (test_id)
--  values (DEFAULT, test.id, fingerprint)
--  values (test.id, select fingerprint from stage)
    select test.id;

insert into sub1 (fingerprint)
--  values (DEFAULT, test.id, fingerprint)
--  values (test.id, select fingerprint from stage)
--  select stage.fingerprint;
    values (
      (select test.id),
      (select stage.fingerprint)
    )
;

insert into sub2 (sub1_id, adress, millis)
  -- values (DEFAULT, sub1.id, adress, millis)
  select
    sub1.id,
    stage.exit_nodes.element.exit_adresses.key,
    stage.exit_nodes.element.exit_adresses.value
;


COMMIT;



-- show results

select * from test where id = 1;
select * from sub1 where test_id = 1;
select * from sub2 where sub1_id = (select id from sub1 where test_id = 1);
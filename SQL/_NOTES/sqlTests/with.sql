WITH ins (description, type) AS
( VALUES
( 'more testing',   'blue') ,
( 'yet another row', 'green' )
)
INSERT INTO bar
(description, foo_id)
SELECT
  ins.description, foo.id
FROM
  foo JOIN ins
    ON ins.type = foo.type ;
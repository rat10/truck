    
    scala> @sh:~/spark$ ./bin/spark-shell --master local[2]
    Using Spark's default log4j profile: org/apache/spark/log4j-defaults.properties
    Setting default log level to "WARN".
    To adjust logging level use sc.setLogLevel(newLevel).
    16/12/01 18:17:43 WARN NativeCodeLoader: Unable to load native-hadoop library for your platform... using builtin-java classes where applicable
    16/12/01 18:17:43 WARN Utils: Your hostname, unknownA0999B1729E5 resolves to a loopback address: 127.0.0.1; using 192.168.1.4 instead (on interface en0)
    16/12/01 18:17:43 WARN Utils: Set SPARK_LOCAL_IP if you need to bind to another address
    16/12/01 18:17:43 WARN SparkContext: Use an existing SparkContext, some configuration may not take effect.
    Spark context Web UI available at http://192.168.1.4:4040
    Spark context available as 'sc' (master = local[2], app id = local-1480612663832).
    Spark session available as 'spark'.
    Welcome to
          ____              __
         / __/__  ___ _____/ /__
        _\ \/ _ \/ _ `/ __/  '_/
       /___/ .__/\_,_/_/ /_/\_\   version 2.0.2
          /_/
             
    Using Scala version 2.11.8 (Java HotSpot(TM) 64-Bit Server VM, Java 1.8.0_91)
    Type in expressions to have them evaluated.
    Type :help for more information.
    
    
    scala> import org.apache.spark.sql.SparkSession
    scala> val spark = SparkSession.builder().appName("SparkTest").getOrCreate()
    scala> import spark.implicits._
    scala> val df = spark.read.parquet("/Users/t/test/rc.parquet.snappy")
    
    scala> df.show(2)
    16/12/10 16:00:12 WARN ParquetRecordReader: Can not initialize counter due to context is not a instance of TaskInputOutputContext, but is org.apache.hadoop.mapreduce.task.TaskAttemptContextImpl
    +--------------------+-------------+----------------------+-----------+----------------+----------------+-------------+-------------+-------------+------------+--------------------+--------------------+-------+--------------------+--------------------+--------------------+--------------------+--------------------+
    |     descriptor_type|     src_date|network_status_version|vote_status|consensus_method|consensus_flavor|  valid_after|  fresh_until|  valid_until|voting_delay|     client_versions|     server_versions|package|         known_flags|              params|         authorities|              status|    directory_footer|
    +--------------------+-------------+----------------------+-----------+----------------+----------------+-------------+-------------+-------------+------------+--------------------+--------------------+-------+--------------------+--------------------+--------------------+--------------------+--------------------+
    |network-status-co...|1472792701000|                     3|  consensus|              20|            null|1472792400000|1472796000000|1472803200000|   [300,300]|[0.2.4.26, 0.2.4....|[0.2.4.26, 0.2.4....|   null|[Authority, BadEx...|Map(usecreatefast...|Map(E8A9C45EDE6D7...|Map(E75E3AA88E3F5...|[Map(Wgd -> 0, We...|
    |network-status-co...|1472828701000|                     3|  consensus|              20|            null|1472828400000|1472832000000|1472839200000|   [300,300]|[0.2.4.26, 0.2.4....|[0.2.4.26, 0.2.4....|   null|[Authority, BadEx...|Map(usecreatefast...|Map(E8A9C45EDE6D7...|Map(E75E3AA88E3F5...|[Map(Wgd -> 0, We...|
    |network-status-co...|1472814301000|                     3|  consensus|              20|            null|1472814000000|1472817600000|1472824800000|   [300,300]|[0.2.4.26, 0.2.4....|[0.2.4.26, 0.2.4....|   null|[Authority, BadEx...|Map(usecreatefast...|Map(E8A9C45EDE6D7...|Map(E75E3AA88E3F5...|[Map(Wgd -> 0, We...|
    +--------------------+-------------+----------------------+-----------+----------------+----------------+-------------+-------------+-------------+------------+--------------------+--------------------+-------+--------------------+--------------------+--------------------+--------------------+--------------------+
    
    // field
    scala> df.select("network_status_version").show(2)
    +----------------------+
    |network_status_version|
    +----------------------+
    |                     3|
    |                     3|
    +----------------------+
    
    // struct
    scala> df.select("voting_delay").show(2)
    +------------+
    |voting_delay|
    +------------+
    |   [300,300]|
    |   [300,300]|
    +------------+
    
    // struct > field
    scala> df.select("voting_delay.vote_seconds").show(2)
    +------------+
    |vote_seconds|
    +------------+
    |         300|
    |         300|
    +------------+
    
    // struct
    scala> df.select(df("directory_footer")).show(2)
    +--------------------+
    |    directory_footer|
    +--------------------+
    |[Map(Wgd -> 0, We...|
    |[Map(Wgd -> 0, We...|
    +--------------------+
    
    // struct > field
    scala> df.select(df("directory_footer.consensus_digest")).show(2)
    +--------------------+
    |    consensus_digest|
    +--------------------+
    |59fc9471747c5e5b3...|
    |72abfcaba1f3e2ccd...|
    +--------------------+
    
    // struct > array
    scala> df.select(df("directory_footer.directory_signature")).show(2)
    +--------------------+
    | directory_signature|
    +--------------------+
    |[[sha1,0232AF901C...|
    |[[sha1,0232AF901C...|
    +--------------------+
    
    // struct > map
    scala> df.select(df("directory_footer.bandwidth_weights")).show(2)
    +--------------------+
    |   bandwidth_weights|
    +--------------------+
    |Map(Wgd -> 0, Wee...|
    |Map(Wgd -> 0, Wee...|
    +--------------------+
    
    // array
    scala> df.select(explode(df("known_flags"))).show(2)
    +---------+
    |      col|
    +---------+
    |Authority|
    |  BadExit|
    +---------+
    
    // array > first entry
    
    ???
    
    
    // map
    scala> df.select(explode(df("params"))).show(2)
    +--------------------+-----+
    |                 key|value|
    +--------------------+-----+
    |CircuitPriorityHa...|30000|
    |  NumDirectoryGuards|    3|
    +--------------------+-----+
    
    // map > key or value
    
    ???
    
    
    // map
    scala> df.select(explode(df("authorities"))).show(2)
    +--------------------+--------------------+
    |                 key|               value|
    +--------------------+--------------------+
    |0232AF901C31A04EE...|[[dannenberg,0232...|
    |14C131DFC5C6F9364...|[[tor26,14C131DFC...|
    +--------------------+--------------------+
    

#truck

**truck** is the storage component of the [**trice**](https://gitlab.com/rat10/trice/) analytics suite for Tor network metrics data. It provides two alternative solutions to store and aggregate Parquet data converted in **trans** in a database backed analytics tool:

###[truckFull](../truckFull/readme.md)
[**SnappyData**](http://www.snappydata.io/) is a database build around Spark. It is actually two databases: a transactional row store for raw data and a column store for analytics. If access to the whole Spark ecosystem of analytic tools is important, SnappyData is the way to go. It is still in beta and has some rough edges but definitely looks very promising.
+ unecumbered Spark integration
+ integration into the whole Spark ecosystem
+ SQL, R, Scala and Java interfaces
+ much faster then Spark itself
- sometimes it shows that the Spark SQL is not "real" SQL
- still in beta
- transactional store not on par with Greenplum
- column store is appendable, but still lacks updates and deletes (beta...)


###[truckBase](../truckBase/readme.md)
[**Greenplum**](http://greenplum.org/) is a PostgreSQL 8.2 fork tuned for analytics. If SQL and R is all you need and knowing that there exists a machine learning extension is just a welcome bonus then Greenplum is for you. Its SQL support is rock solid, it scales way beyond what Tor metrics can provide and PostgreSQL users will feel right at home.  
+ native, rock solid SQL
+ familiar PostgreSQL interface
- less analytics options then Spark
- packages for RedHat and MacOS, but not for Debian (but there are means...)
- maybe not as fast as SnappyData (but probably sufficient for Tor workloads)


Both packages shall provide 
- instructions for setting up
- instructions for scaling out to EC2 or similar
- SQL scripts for initializing the database
- SQL scripts for loading the data from Parquet files or 
- Java tools for loading data directly from the raw CollecTor data via JDBC
- script for periodic ingestion of new or updated data
- appropriate re-runs of aggregation scripts when data gets added or updated


##backgrounds

###when to use a database

When metrics data got converted through **trans** to Parquet or JSON serializations it is already fit for consumption in analytics tools. While JSON serves the needs of web applications and end user "business intelligence"/BI tools the less ubiquitious Parquet serialization is a column storage serialization optimized for heavy analytic workloads. Big Data tools that don't need row level acces and updating capabilities regularily use Parquet files stored on Hadoop file systems (HDFS) instead of a database. This makes Parquet the ideal storage solution when the goal is to analyze a given timeframe: convert the raw data with **trans** to a Parquet file and aggregate on this very file in Apache [Spark](https://spark.apache.org/).   

The downside of Parquet however is that it can't be appended, let alone updated. Updates in the raw data unfortunately are not uncommon, not only but of course especially if the period of interest is not historic. Some analytic tools like Apache Drill (which speaks only SQL) can work over directories of data but the increasingly dominant Apache Spark can't. Getting an update on the raw data therefor probably requires a new conversion run and a new run of aggregations. That may be okay and if it is it spares the effort of loading the data into a database, but if not...

###transactional vs big data vs columnar databases

The natural solution to the updates problem is a database - but which one should it be? A traditional transactional RDBMS? A Big Data NoSQL database? Something else? The size of all available raw Tor metrics data is about 1 TB uncompressed. This is somehow on the edge: it's not negligable but little by Big Data standards and it's a lot but certaimnly not unfeasable for a traditional RDBMS (at least for a mature product like eg PostgreSQL).
Traditional RDBMS are optimized for transactional workloads and many Big Data NoSQL databases like HBase, Cassandra or Accumulo are too (they just sacrifice some integrity constraints for performance). Transactional databases are optimized for row/record level read/write access where a single record can be updated performantly and data is accessed record wise. In an analytics setting OTOH writes are mostly bulk writes of big chunks of data and updates are not the norm. Analytics reads concentrate on columns: they usually have to access a specific field from a great number of rows. Fetching all those rows entirely only to throw away all but one field in the next step like a transactional, row-oriented database would do is unefficient. For these analytic workloads columnar storage plays out much better: data locality is organized around columns, not rows. Fetching a specific field in a lot of rows becomes cheap and storage space can be effectively reduced through compression. Updates OTOH become expensive because a lot of columns have to be touched to update one row.

###integration with analytics tools

The other important factor to consider is how well the database integrates with available analytics tools. While MapReduce was all the craze in the Big Data world not long ago development has shifted back to SQL more recently. Apache [Spark](https://spark.apache.org/) is now the central hub through which the storage layer is accessed and into which all sorts of analytics packages plug in - not only SQL but graph processing, machine learning, fulltext search, visualization tools etc -, not to mention the range of supported languages: Java, Scala, Python and R. So good integration with Spark seems definitely desirable.

###all-in-one packages

After some initial research into the [Big Data database world](doc/db-research-2015.md) HBase for storage and Spark and Drill for analytics seemed like reasonable choices.  
In automn 2016 the [topic was revisited](doc/db-research-2016.md). One reason was a better understanding of the problem space aquired in the meantime and consequently a stronger awareness of the benefits of column stores. But also the focus of the project had shifted from setting up a server for users to log in and run their aggregations on to a suite of tools that users can install on there own premises - and asking users to set up a Hadoop installation with HDFS, HBase, Spark and maybe even some streaming library etc (even if it's not a real cluster but just a local surrogate) looked very much like a non-starter. So an integrated solution, including a column store, was needed. When this project started in automn 2015 there where very few open source column stores available (MonetDB, Virtuosos) and they where rather academic and/or not integrated into the Big Data ecosystem at all but it turned out that the open source column store landscape had indeed changed during the following year. Some rather highend solutions had been publsihed, real Big Data databases that replace indices with partitioning, but also two options that fit into **truck**s requirements quite nicely, although only partially: [SnappyData](http://www.snappydata.io/) and [Greenplum](http://greenplum.org/). Both databases are optimzed for analytic workloads through 'columnar' storage, but target different audiences. SnappyData is build around [Spark](https://spark.apache.org/), incorporating the whoel Spark ecosystem of analytic tools, and extends it with both a CRUD columnar store and a transactional backend, plus easy scale out to the cloud and some other goodies. Greenplum OTOH is a fork of PostgreSQL 8.2 (therefor without JSON support), enhanced through an advanced query optimizer, configurable columnar storage along side row storage and support for clustered setups, thereby providing Big Data performance with a familiar PostgreSQL interface.
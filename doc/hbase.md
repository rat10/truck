# HBase   

HBase for looking up single documents. You can do map reduce on HBase too but other storage formats like Parquet are better suited to 
that task. The HBase schema will be optimized for access to records of individual servers. Since HBase has to substitute SQL joins with data denormalization and uses some special techniques to that end this schema will look very different to Collector/JSON.

## principals of HBase schema design

HBase is a semi-structured DBMS whereas the wellknown RDBMS are structured 
DBMS. This provides certain freedoms that can be used to make up for its 
deficiences compared to RDBMS particularily that HBase doesn't offer joins 
and only limited indices.

- put any "critical" information - that is: anything that you'll query for - in
  the key (on retrieval filter for the stuff you want to know)  
- substitute joins by data duplication in nested columns  
- do not fear de-normalization since a) our data is write-once and 
  inconsistencies can't arise later and b) columns can be compressed (and we 
  don't need space for indices (since we dont have them)  
- use column families to group related columns     
  eg from the same input sources  
- do not fear wide rows. thousands of columns are totally okay  
- do not fear sparsly populated rows. NULL values use up zero space  



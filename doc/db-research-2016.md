[actually this whole evaluation is quite frustrating because all these fine DBs
have a tendency to lack very fundamental mechanisms (like e.g. indexing or 
some CRUD operations) without being very outspoken about it upfront ...]

what we need is a database to store incoming records and a database for 
aggregated views.  
the incoming db must be able to update records and has 3 purposes: 
- deduping, when data comes twice eg because of updates
- feeding the aggregations
- record-level access when searching for particular servers or occurrences
it would be very beneficial if the incoming database provided complex 
modelling capabilities so that it is able to capture the full complexity of
the incoming raw data (which unfortunately is quite complex, with lists of 
sets of maps etc). otherwise we would still need the original raw data around
just in case some very special information need comes up.
the aggregations database holds the aggregated views. it's sole purpose is to 
provide interactive access to the data. vulgo: it should provide damn fast reads
to e.g. power an interactive visualization dashboard like zeppelin or tableau.
it should provide at least the same speed as Parquet files and as a plus be 
updateable - otherwise it wouldn't provide much benefit over parquet. 
it probably has to be a columnar store to achieve the necessary speed. if it 
provides APPEND then we wouldn't need to aggregate each view anew when new data
comes in (e.g. daily our even hourly) as we would have to with Parquet files. 
with full CRUD we could even handle the occassional updates in raw data
(corrected versions, Snafus, etc) without having to aggregate everything from 
start again. that should be a nice plus.
it would be nice to have a system that combines 
- incoming raw data DB
- aggregations DB
- aggregations engine (Apache Spark, to be precise)
- ingestion management
to minimize setup and administration complexity.
 
 
aggregations DB 
                              filodb  kudu    snappy  splice  memsql    greenplum
    row storage               -       -       +       +       +         +
      dbms                    -       -       GemFire HBase   InMemory  PostgreSQL
      sql                     -       -       +       +                 +
      nested complex          -       -                                 (+ as XML) 
    columnar storage          +       +       +       +       +         +
      full CRUD               -       +       -               +         +
      no CRUD, mostly append  +       -       +               -  
      index on primary key    +       +       -               +         +
      complex primary key     +       +       -               +         
      secondary index         -       -       -               -    
      complex types                   -       +               (json)    XML       
      nested complex types            -       ????            -     
      strongly typed                  +       +                     
    documentation             --      +       +       -                 +
    spark well integrated     +       -       +               +         -
    spark 2.0                 -       +       +       +                 n/a
    interface from sql                                        +         +
    interface from o/jdbc                                     +         +
    interface from scala                                      +         -
    interface from python                                     +         -
    interface elastic search                                  +         -
    ec2 ready to go           -       -       +       -       +         -
    zeppelin ready to go      -       -       +       -       -         +
    kafka                     +                               +         

these are the databases that we investigated in detail. if you're missing HBase 
and Cassandra: there read speed is far slower than that of Parquet files. they
couldn't drive an interactive visualization but they would be contenders for the
in incoming raw data DB although they would both struggle with the complexity of
the raw data. but they provide fast row access and with some tricks and 
workarounds it might be possible to stuff all data in.


memsql
    full sql, mysql-interface
    column store is the main structure, row store is ony in memory 
    super fast through query code geeration
    docker container
    imports from csv, but not from parquet 8-(
    very good docs
    QUESTIONS
      can i use spark libs like spark ml?
      no stored procedures, triggers etc??
      ingest parquet?
    BUT
      NOT OPEN SOURCE  8::(


kudu 
                                    scala sql   spark zepp hbase 2ind  ec2
                                    -     o      1.6   -    +     -     .
    batch performance like parquet for OLAP
    but also fine grained access for OLTP
    based on HBase
    CRUD through impala as main interface (why not Spark SQL? because impala is clouderas baby?)
    limitations in schema design (https://kudu.apache.org/docs/schema_design.html#known-limitations)
    spark-shell --jars kudu-spark-0.9.0.jar (https://kudu.apache.org/docs/developing.html)
    no scala api, not clear how complete support for spark is
    ingest streams, trigger view aggregates (http://blog.cloudera.com/blog/2016/05/how-to-build-a-prediction-engine-using-spark-kudu-and-impala/)
    fine grained compression tuning possible per column
    COULD BE THE RIGHT THING
      because it seems to solve the main problem with the least effort
      have fast OLAP as the general usecase
      with some OLTP for the occassional need 
    BUT
      only spark 1.6
      no secondary indices
      no nested collections (not a priority, https://issues.apache.org/jira/browse/KUDU-1261)
      row sizes max 10kb, columns max 50
    BY cloudera
    
    
snappyData
                                    scala sql   spark zepp hbase 2ind  ec2
                                    +     +      2.0   +    -     +     +
    GemFire DB for row (OLTP) and column (OLAP) storage
    has "row tables" (GemFire) and "column tables" (spark)
      GemFire is an  In Memory Data Grid
        data model is non-relational and is object-based
        data is stored entirely in RAM of multiple, distributed servers
        a distributed object store
    column tables have no indexes or primary keys
    row tables support a big range of SQL
    complete spark integration
      everything is accessible through spark api
      all spark extensions work on all the data
    comes with EC2 and zeppelin integration
    streaming support
    Approximate Query processing (AQP)
    trigger stored procedures with streams
    GOOD
      the amount of problems that seem to be solved in one piece of software
      seems fast (http://www.snappydata.io/blog/snappydata-memsql-cassandra-a-performance-benchmark)
      complex types (but nesting? probably the same as in Spark -> TODO check spark for nesting)
    BAD
      it does APPENDS on column tables but not updates 
        (is that really a serious problem?)
        (they are working on it)
      no indices on column tables (aggregations)
    DESIGN
    - The storage layer is primarily in-memory and manages data in either row or column formats. 
      The column format is derived from Spark’s RDD caching implementation and allows for compression. 
      Row oriented tables can be indexed on keys or secondary columns, supporting fast reads and writes on index keys. 
    - a SQL database that uses Spark API as its language for stored procedures
    - stream processing is primarily through Spark Streaming, but it is integrated and runs in-situ 
    - OLAP scheduler and job server coordinate all OLAP and Spark jobs 
    - to prevent running out of memory, tables can be configured to evict or overflow to disk 
      using an LRU strategy. For instance, an application may ingest all data into HDFS 
      while preserving the last day’s worth of data in memory.
    - tables are automatically registered to a built-in persistent catalog.
      Data in tables is primarily managed in-memory but can also be reliably managed on disk. 
    QUESTIONS
      if column tables have no primary keys and no indexes, how are they different from Parquet files?
      what types are supported in the column store? does it support "complex" types like lists, sets, maps?
    ANSWERS in slack
      while conceptually there is similarity to Parquet, Parquet is a storage format designed for disk/SSDs. Each file includes meta data and headers for each file, etc. And, afaik, Parquet has no provisions to alter the file once created. SnappyData column tables do support APPENDs and the data storage design in memory is quite different - compression is based on dictionary encoding, run-length-encoding and delta encoding.
      yes, the type system is the same as Spark - you can store nested types you list. the depth of nesting is something I don't know off-hand. If it works in spark, it will likely work in Snappydata too.
      You can store JSON and you can also manage nested objects using User Defined Types in the Row store. We will consolidate all this soon.
      The User defined types though are opague and merely treated as blobs within the system. JSON support on the other hand is more first class. Do some searches in the docs to get to the relevant content.
       column tables will also get indexes and update support soon in a future release.
      Column tables are memory optimized for highest performance and their performance profile with default compression schemes is significantly better than compressed/uncompressed parquet and even if latter is cached in memory (or in OS buffers).
      Also in consideration is inbuilt support for persisting column table data to parquet files in an efficient way (right now has to be done manually using Spark APIs). Somewhat challenging when column tables add update support though.
  
  
spliceMachine
                                    scala sql   spark zepp hbase 2ind  ec2
                                    -     +      2.0   .    +     +     +
    scale out SQL with odbc, jdbc as main interfaces
    derby db sql logic on top of hbase OLTP + spark OLAP
    2 types of tables (like snappy), but transparently / under the hood
    automates RDD creation for olap queries
    MapReduce Input/Output API to run custom, batch-oriented analyses by Hadoop tools 
    BAD
      to work with spark libraries first has to convert table to RDD 
      docs
      tedious to evaluate - may be cool, maybe not
      no benchmarks on how fast the sql-on-hbase really is
      uncool: registration
  
  
phoenix
    a thin sql layer on top of hbase
    scale out OLTP but not for OLAP


filoDB
    GOOD
      based on cassandra, but 10x to 100x faster for analytical workloads
      written in scala
      good spark integration
      supports versioning (but that is not documented)
      basically not OLTP suited, just a better Parquet
    BAD
      but only Spark 1.6, so no DataSets
      rudimentary indices
      very rudimentary CRUD
      no complex types?
      no "move this to EC2"-script
      very sparse documentation
      future slightly unclear
      in-memory store does not spill to disk (https://groups.google.com/forum/#!topic/filodb-discuss/uQ1RDVO7sgE)
      for OLTP better use pure cassandra (https://groups.google.com/forum/#!topic/filodb-discuss/uQ1RDVO7sgE)
      updates are actually implemented as appends (versioning), sometimes compaction
    FOLLOW UP QUESTIONS
      what happens when memstore is full? spill to disk already implemented?
      are deletes supported
      can you elaborate on versioning?
      strong typing?
    TODO
      understand predicate pushdown (https://github.com/filodb/FiloDB#overview)
    
cassandra vs hbase
    cassandra seems to be more actively developed and faster
    cassandra has secondary indices
      but they are not very efficient (https://www.oreilly.com/ideas/apache-cassandra-for-analytics-a-performance-and-storage-analysis)
    hbase is more OLAP optimized in the way it organizes its cluster
    so: for our single node cluster cassandra would be better
    but for our mainly OLAP use cases hbase would be better in scale out

KEYING
    primary key
      datasource              raw/aggregate
      type                    relay/bridge/authority
      timestampCreation       (rather which time is the data about)
      raw-timestampIngestion  (rather timestamp of the collector source file)
      raw-identifier
      aggregate-span          hour/day/week/month/year
    filo
      partition key           hour (but what about daily aggregations?)
      segment key             type (e.g. rawBridge, rawTordnsel, ..., aggregate)
      row id                  ... (either which aggregate or the identity of a raw descriptor)

cassandra
    WHY
      OLAP is not a usecase rdbms are made for
      it requires denormalization and profits from column storage
      C* with one table per query provides fast reads without joins (extreme denormalization)
      C* also provides good write speed, so extreme denormalization is relatively cheap
    
    HOW
      partition raw table by month, aggregated tables not at all or by year
  
  
SPARK CENTERED STRATEGY
    source
      parquet files are the single source of truth
    ingestion
      new parquet files trigger (kafka or spark streaming) appropriate aggregations
    spark does all agregations
      aggregate views from parquet to either other parquet files or an updatefriendly store
    store
      update aggregations when new sources arrive
      indexed
    
    aggragations are mainly what we (expect to) want to know
    each aggregated value/row is accompanied by a list of source/raw entries it was generated from
    the raw/source entries are aggregated into a source table to look them up
    this source table is less detailed than the actual source because it can't be nested
    but it's indexed and therefor allows fast lookups
 
  
  
???
  would it be possible to store with each aggregated value the keys of the raw entries it was aggregated from? 
    (yes: as a list)
    would it make sense?
    
    
    
    
A LETTER TO FILO

tl;dr: 
  Can FiloDB store complex types or even nested complex types? 
  (When) will it support Spark 2 and Cassandra 3? 
  Is there an API reference somewhere? 
  What are the supported Spark SQL commands? 
  Are row keys indexed?


THE PROJECT

Right away: sorry that this is such a long mail! Let me describe the situation:
I've got raw data stored in Parquet files. Every hour I get new data files.
The data schema is not exactly flat - there are lots of complex fields (lists, 
sets, maps and even lists of maps nested inside lists).
Processing involves two components: 
- OLTP ingesting and updating raw data into an indexed store
- OLAP aggregating and analyzing views (Spark + plugins), visualizing (Zeppelin)

Also, because sometimes I get updates to older, already processed raw data, 
aggregates need to be updated. Ideally I'd like to be able to update them 
partially so that I don't have to run all aggregations on the whole raw 
dataset again. 

The easiest solution would be to read the raw Parquet data from Spark, write
aggregates to new Parquet files and run analytics on them and that's indeed my 
baseline. This does get tedious though when updates come into play or when I
need to backtrace analytic findings to their source in the raw data. I would
therefor prefer to have all data in in a database, indexed, updateable - and
of course lightning fast ;-)

The size of raw data is modest: about 1 TB uncompressed until now, 200 GB more 
each year. Still it's a lot for conventional RDBMSs like Postgres and I assume 
the OLAP performance of a big data solution (not to mention the libraries and 
tools that are available for Spark) makes the effort worthwile and enables a
nice and fluid analytic experience. OTOH I don't expect to need a cluster but 
get by with a single machine (still it's nice to know that scale out would not 
be an issue).


THE SOFTWARES

After checking Kudu, SnappyData, SpliceMachine, FiloDB and others it looks
like FiloDB would be my best bet since it offers
- very fast read, but also occassional writes
- flexible enough indexing
- good integration with Spark (and Scala :)
SnappyData is runner up as it provides better support for OLTP workload and 
includes support for Zeppelin and EC2 out of the box but has a less convincing
OLAP implementation (especially only very limited CRUD (yet)).

But I do see problems with FiloDB too:
- It seems that complex types like sets, lists and maps are not supported. 
  Is that correct?
- Supported libraries are aging (well, Spark 1.6 is not really old, but 
  Spark 2 is newer ;) and Cassandra 2.x will soon reach EOL)
- Documentation is really sparse and seems to require prior knowledge and I'm 
  not even sure where to aquire that - read C* docs?
  (is there a API reference? where can I look up supported database commands?)
  (Or is this a dumb question as I would I see the available commands if I just 
  installed the darn thing and started an IDE or plugged in Tableau?)
[0] says it uses C* 3.5 - so it's possible?


THE KEYS

Regarding primary keys: I'm having trouble figuring out what partition and
segment keys I should use. The following paragraph seems to be important but
I do not fully understand it: 
"Partition keys are the most efficient way to filter data. Remember that, 
unlike Cassandra, FiloDB is able to efficiently filter any column in a 
partition key -- even string contains, IN on only one column. It can do this 
because FiloDB pre-scans a much smaller table ahead of scanning the main 
columnar chunk table. This flexibility means that there is no need to populate 
different tables with different orders of partition keys just to optimize for 
different queries."

A thread on filodb-discuss [0] seemed more helpful. From it I gather that:
- only partition keys and segment keys are indexed, but not row keys
- no parallelization without partitions (that's the benefit)
  but for each partition key a Spark process is started (that's the cost)
  and partitions shouldn't get bigger than 2 GB (no problem for me)
- segment keys are indexed (which gives speed)   
  but more segments mean less space efficiency (the cost)
  so segment keys with low cardinality are a plus
I hope that I got that right.

Some more questions:
- Does the size of key names or column names matter? Does it make a difference 
  if I call a key "agg" or "aggregation"?
- Does adding more columns to the row key have any disadvantages besides, 
  obviously, index size?
- How does a combined segment key of "Column1 + Column2" affect the performance 
  of searches on only Column1 or only Column2?
- Timestamp is the single most important aspect in my data. I could split it 
  into 
    partition key: year/month
    segment key: day/hour
    row key: identifier
  Does this look plausible or is there e.g. a rule to never split timestamps?
  

Thanks,
Thomas


[0] https://groups.google.com/forum/#!topic/filodb-discuss/oBIzquZ2ulk